# EatT

Make sure
⁨- /system⁩/spotneat⁩/session folder exists with write permission

Import the eatt.sql file and

Configure the database here
- /system/spotneat/config/database.php

Provide write permissions for
- assets/images

Error reporting
- /system/spotneat/core/SpotnEat.php