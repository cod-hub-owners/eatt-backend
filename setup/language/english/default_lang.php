<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'EatT - Setup - %s';
$lang['text_license_heading'] 		    = 'License agreement';
$lang['text_requirement_heading'] 		= 'Server Requirements';
$lang['text_database_heading'] 		    = 'Database Settings';
$lang['text_settings_heading'] 		    = 'Site Settings';
$lang['text_success_heading'] 		    = 'Installation Successful';

$lang['text_license_sub_heading']       = 'Please read the EatT Licence agreement.';
$lang['text_requirement_sub_heading']   = 'Check below to make sure all server requirements are met.';
$lang['text_database_sub_heading']      = 'Enter your database connection details.';
$lang['text_settings_sub_heading']      = 'Enter your restaurant and administrator details.';
$lang['text_success_sub_heading']       = 'Start receiving orders!';

$lang['text_php_version'] 				= 'is version %s supported?';
$lang['text_php_version'] 				= 'is version %s supported?';
$lang['text_mysqli_installed'] 			= 'is MySQLi installed?';
$lang['text_curl_installed'] 			= 'is cURL installed?';
$lang['text_gd_installed'] 				= 'is GD/GD2 library installed?';
$lang['text_register_globals_enabled'] 	= 'is Register Globals turned off?';
$lang['text_magic_quotes_enabled'] 	    = 'is Magic Quotes GPC turned off?';
$lang['text_file_uploads_enabled'] 		= 'is File Uploads turned on?';
$lang['text_is_file_writable'] 		    = 'is file writable?';

$lang['text_license_agreed'] 		    = 'By proceeding further, you will be deemed to have accepted the License agreement.';
$lang['text_login_to_admin'] 		    = 'Login to your Admin Dashboard';
$lang['text_goto_storefront'] 		    = 'Go to your Online Restaurant';
$lang['text_next_step'] 		        = 'Next Step:';
$lang['text_restaurant_details'] 	    = 'Restaurant settings';
$lang['text_admin_details'] 		    = 'Administration settings';
$lang['text_installation_success'] 	    = 'EatT has been installed successfully.';
$lang['text_join_community'] 	        = '<h3>Join the community</h3><span>Receive future updates, recent news and free support.</span>';

$lang['text_spotneat_home'] 	    = 'EatT Homepage';
$lang['text_documentation'] 		    = 'Documentation';
$lang['text_community_forums'] 		    = 'Community Forums';
$lang['text_feature_request'] 			= 'Feature Requests';

$lang['label_php_version'] 				= 'PHP Version (required %s+):';
$lang['label_register_globals'] 	    = 'Register Globals:';
$lang['label_magic_quotes'] 		    = 'Magic Quotes GPC:';
$lang['label_file_uploads'] 		    = 'File Uploads:';
$lang['label_mysqli'] 				    = 'MySQL:';
$lang['label_curl'] 				    = 'cURL:';
$lang['label_gd'] 				        = 'GD/GD2:';

$lang['label_database'] 			    = 'Database Name:';
$lang['label_hostname'] 			    = 'Hostname:';
$lang['label_username'] 			    = 'Username:';
$lang['label_password'] 			    = 'Password:';
$lang['label_prefix'] 				    = 'Table Prefix:';

$lang['label_site_name'] 			    = 'Restaurant name:';
$lang['label_site_email'] 			    = 'Restaurant email:';
$lang['label_staff_name'] 			    = 'Administrator\'s name:';
$lang['label_admin_username'] 	        = 'Administrator\'s username:';
$lang['label_admin_password'] 	        = 'Administrator\'s password:';
$lang['label_confirm_password'] 	    = 'Confirm password:';
$lang['label_demo_data'] 			    = 'Include demo data:';

$lang['help_database'] 				    = 'Enter the name of the database you want to use.';
$lang['help_hostname'] 				    = 'Enter the database host name.';
$lang['help_username'] 				    = 'Enter the database username.';
$lang['help_password'] 				    = 'Enter the database password. Make sure to use a strong password.';
$lang['help_dbprefix'] 				    = 'The database table prefix. Do not change unless you have to.';

$lang['button_continue'] 				= 'Continue';
$lang['button_back'] 				    = 'Back';

$lang['alert_license_error'] 		    = 'Please read the SpotnEat license before proceeding.';
$lang['alert_requirement_error'] 		= 'Error checking server requirements, please make sure all lights are green.';
$lang['alert_database_error'] 		    = 'Database connection was unsuccessful, please make sure the database server, username and password is correct.';
$lang['alert_settings_error'] 		    = 'Error installing user and site settings.';

$lang['text_license_terms']             = "<h3>LICENSE</h3>
<pre>

What is Lorem Ipsum?

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry'/s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
Why do we use it?

It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).

Where does it come from?

Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', comes from a line in section 1.10.32.

The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from 'de Finibus Bonorum et Malorum' by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
Where can I get some?

There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
</pre>";

/* End of file english_lang.php */
/* Location: ./setup/language/english/english_lang.php */