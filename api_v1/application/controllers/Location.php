<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// Rest Controller library Loaded
use Restserver\Libraries\REST_Controller; 
require APPPATH . 'libraries/REST_Controller.php';

class location extends REST_Controller {

    public function __construct() { 
        parent::__construct();
        $this->load->model(array('Member','Customers_model','Locations_model'));
        $this->load->library(array('form_validation','Customer'));
        $this->load->helper('security');
    }

    
    public function view_get() {
         
         $location_data = array();

         if($this->get('search_data')){

          $search_data = $this->get('search_data');            

         }else{

          $search_data = '';  

         }


         $location_data = $this->Locations_model->getLocation_New($search_data);

         if($location_data==0){

         $error_data = array('code'  => 401 ,
                             'error' => 'Location Not Found.');               
         $output = array('message'  => $error_data);
         echo json_encode($output);

         }else{

         $output = array('result'  => $location_data, 
                         'message' => 'Location Search');
         echo json_encode($output);
         
         }
    
    }
}
 ?>