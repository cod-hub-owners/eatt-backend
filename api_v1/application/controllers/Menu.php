<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// Rest Controller library Loaded
use Restserver\Libraries\REST_Controller; 
require APPPATH . 'libraries/REST_Controller.php';

class menu extends REST_Controller {

    public function __construct() { 
        parent::__construct();
        $this->load->model(array('Member','Customers_model','Locations_model','Menus_model'));
        $this->load->library(array('form_validation','Customer','Currency'));
        $this->load->helper('security');
    }

    // Login Section

    public function view_get() {

         if($this->get('search_data')){

          $search_data = $this->get('search_data');            

         }else{

          $search_data = '';  

         }

         $results = $this->Menus_model->getListNew($search_data);

         if($results=='0'){

         $error_data = array('code'  => 401 ,
                             'error' => 'Menu Not Found.');               
         $output = array('message'  => $error_data);
         echo json_encode($output);
            
         }else{
           
           $output = array('result'  => $results, 
                         'message' => 'Location Search');
           echo json_encode($output);

         }
    
    }
}
 ?>