<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// Rest Controller library Loaded
use Restserver\Libraries\REST_Controller; 
require APPPATH . 'libraries/REST_Controller.php';

require __DIR__.'/../../../vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;


class Restaurant extends REST_Controller {

	public function __construct() { 
      parent::__construct();
      $this->load->model(array('Member','Customers_model','Countries_model','Addresses_model','Locations_model','Delivery_model','Settings_model','Restaurant_model','Menus_model','Categories_model','Menu_options_model','Staff_groups_model','Staffs_model','Orders_model','Statuses_model','Reservations_model','Types_model','Permalink_model','Cash_payments_model','Online_payments_model','Payments_report_model'));
      $this->load->library(array('form_validation','Customer','Delivery'));
      $this->load->helper('security');
      $this->load->helper('date');
      $this->load->library('session');
      $this->load->library('User');
      $this->load->helper('logactivity');
      $this->load->library('currency');
       $this->load->library('permalink');
  }
  public function restaurantlogin_post() {

        if ($this->validateLoginForm() === TRUE) {

            $username = $this->post('username');
            $password = $this->post('password');
            $deviceid = $this->post('deviceid');
            $login_details = $this->Restaurant_model->login($username, $password); 
            if ($login_details === FALSE) {                      
               /* $error_data = array('code'  => 401 ,
                                    'error' => 'Invalid login credentials.');   */
                $msg =   'Invalid login credentials.';          
                $output = array('message'  => $msg);
                echo json_encode($output);
            }else{

                $output = array('result'  => $login_details, 
                                'message' => 'Successfully Login'
                            );
                echo json_encode($output);
            }                                  
        }else{
                $error_data = array('code'  => 401 ,
                                    'error' => 'Invalid Params.');               
                $output = array('message'  => $error_data);
                
                echo json_encode($output);
        
        }

    }
    
    public function itemlist_post() {
      $filter = array();
      $filter['filter_search'] = $this->input->post('filter_search');
      $filter['filter_location'] = $this->input->post('location_id');

      $menu_list = $this->Menus_model->getMenus($filter, $this->input->post('staff_id'));
      
      foreach ($menu_list as $key => $list) {
        
        $menu_options = $this->Menu_options_model->getMenuOptions($list['menu_id']);
        
        
        foreach ($menu_options as $option) {
          $option_values = array();
          foreach ($option['option_values'] as $value) {
            $option_values[] = array(
              'menu_option_value_id' => $value['menu_option_value_id'],
              'option_value_id'      => $value['option_value_id'],
              'price'                => (empty($value['new_price']) OR $value['new_price'] == '0.00') ? '0.00' : $value['new_price'],
              'quantity'             => $value['quantity'],
              'subtract_stock'       => $value['subtract_stock'],
            );
          }

          $menu_list[$key]['menu_options'][] = array(
            'menu_option_id'    => $option['menu_option_id'],
            'option_id'         => $option['option_id'],
            'option_name'       => $option['option_name'],
            'display_type'      => $option['display_type'],
            'required'          => $option['required'],
            'default_value_id'  => isset($option['default_value_id']) ? $option['default_value_id'] : 0,
            'priority'          => $option['priority'],
            'option_values'     => $option_values,
          );
        }

        
        foreach ($menu_options as $option) {
          if ( ! isset($menu_list[$key]['option_values'][$option['option_id']])) {
            $menu_list[$key]['option_values'][$option['option_id']] = $this->Menu_options_model->getOptionValues($option['option_id']);
          }
        }
      }
       $output = array('result'  => $menu_list, 
                                'message' => 'Success'
                            );
                echo json_encode($output);
                exit;
    }
    public function categorylist_post() {
      // $location_id = $this->input->post('location_id');
      $location_id = '';
      $category_list = $this->Categories_model->getList($filter = array(),$this->input->post('staff_id'),$location_id);
      
       $output = array('result'  => $category_list, 
                                'message' => 'Success'
                            );
                echo json_encode($output);
                exit;
    }

    public function optionlist_post() {
      // $location_id = $this->input->post('location_id');
      $location_id = '';
      $options_list = $this->Menu_options_model->getOptions($this->input->post('staff_id'),$location_id);
      
       $output = array('result'  => $options_list, 
                                'message' => 'Success'
                            );
                echo json_encode($output);
                exit;
    }

    private function validateLoginForm() {
        return TRUE;
        $this->form_validation->set_rules('email', 'Email', 'xss_clean|trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'xss_clean|trim|required|min_length[6]|max_length[32]');
        
        if ($this->form_validation->run() === TRUE) {                                           
            return TRUE;
        } else {
            return FALSE;
        }

    }
    public function restaurantlist_post() {
      $filter = array();
      $filter['added_by'] = ($this->input->post('staff_id')!='11') ? $this->input->post('staff_id') : '';
      // $filter['filter_status'] = '1';
      $restaurant_list = $this->Locations_model->getList($filter);
      
      foreach ($restaurant_list as $key => $value) {
        $restaurant_list[$key]['options'] = unserialize($value['options']);
        $country_name         = $this->Locations_model->getCountryName($value['location_country_id']);
        $restaurant_list[$key]['country_name']     = $country_name[0]['country_name'];
      
      
        $restaurant_list[$key]['permalink'] = $this->permalink->getPermalink('location_id='.$value['location_id']);

        $restaurant_list[$key]['location_tables'] = $this->Types_model->getTablesByLocation($value['location_id']);

      $results = $this->Locations_model->getrestaurantcommission($value['location_id']);
        
        foreach ($results as $result) {

            $restaurant_list[$key]['commission_reports'][] = array(
                'id'             => $result['id'],
                'order_id' => $result['order_id'],
                //'delivery_commission'   => $result['delivery_commission'],
                //'shopper_commission'  => $result['shopper_commission'],
                'restaurant_commission' => $result['restaurant_commission'],
                'payment_date'          => $result['payment_date'],
            );
        }

      }  

       // $restaurant_list[$key]['permalink']['url'] = root_url('local').'/';
       $output = array('result'  => $restaurant_list, 
                                'message' => 'Success'
                            );
                echo json_encode($output);
                exit;
    }
    public function deliveryboylist_post() {
      $filter = array(
        'filter_search'   =>  $this->input->post('filter_search'),
        'page'            =>  $this->input->post('page'),
        'filter_staff_id' =>  ($this->input->post('staff_id')!='11') ? $this->input->post('staff_id') : ''
      );
      $delivery_list = $this->Delivery_model->getList($filter,$this->input->post('staff_id'));
      
       $output = array('result'  => $delivery_list, 
                                'message' => 'Success'
                            );
                echo json_encode($output);
                exit;
    }

    public function restaurantregister_post() {

        if ($this->validateForm() === TRUE) {

          $saves= $this->input->post();

          $saves['staff_location_id']='0';
          $data['staff_permissions'] = $this->Staff_groups_model->getStaffGroupsPermissions();
          // $data['default_permission'] = $default_permission;
          $data['permissions'] = unserialize($data['staff_permissions']);
          
          // echo "<pre>";
          // print_r($data['permissions']);exit;
          foreach ($data['permissions'] as $key => $permission) {

            $name = $this->Staff_groups_model->getPermissionName($key);
            $name = explode('.',$name);
                    $data['permission_name'][$key] = $name[1];

          }
          $saves['permission_name'] = $data['permission_name'];
          // echo "<pre>";
          // print_r($data1['permission_name']);exit;
          $saves['staff_group_id'] = '11';
          if ($staff_id = $this->Staffs_model->saveRestaurant($this->input->get('id'),$saves)) {
                  $msg =   'Register Successfully. Waiting for admin approval';          
                $output = array('message'  => $msg);
                echo json_encode($output);
              }
                                            
        }else{
                
          print_r(json_encode($this->form_validation->error_array()));
        }
      exit;
    }

    public function vendororder_post() {

          $id= $this->input->post('vendor_id');

          $filter = array();
          if ($this->input->post('page')) {
            $filter['page'] = (int) $this->input->post('page');
          } else {
            $filter['page'] = '';
          }
          $filter['limit'] = 10;
          if ($this->input->post('filter_search')) {
            $filter['filter_search'] = $data['filter_search'] = $this->input->post('filter_search');
          } else { 
            $data['filter_search'] = '';
          }
           if ($this->input->post('filter_status') != "") {
            $filter['filter_status'] = $data['filter_status'] = $this->input->post('filter_status');
          } else { 
            $data['filter_status'] = '';
          } 
          if ($this->input->post('filter_location')) {
            $filter['filter_location'] = $data['filter_location'] = $this->input->post('filter_location');
          } else { 
            $data['filter_location'] = '';
          }
          $filter['sort_by'] = ($this->input->post('sort_by') && $this->input->post('sort_by')!='') ? $this->input->post('sort_by') : 'order_id';
          $filter['order_by'] = ($this->input->post('order_by') && $this->input->post('order_by')!='') ? $this->input->post('order_by') : 'DESC';

          $results = $this->Orders_model->getList($filter,$id,"YES");
          

          if(count($results) > 0){
           
            foreach ($results as $key => $result) {
              $results[$key]['notify_customer'] = $result['notify'];
              if($result['delivery_id']!='' && $result['delivery_id']!='0') {
                $del = $this->Delivery_model->getDelivery($result['delivery_id']);
                $email = $del['email'];
              } else {
                $email = '';
              }
              $results[$key]['email'] = $email;
              $reservation_details = $this->Orders_model->getReservationdetails($result['order_id']);

              $new_data['totals'] = array();
              $order_totals = $this->Orders_model->getOrderTotals($result['order_id']);
              foreach ($order_totals as $total) {

                // if ($result['order_type'] === '2' AND $total['code'] == 'delivery') {
                //   continue;
                // }
                if($total['code'] == 'cart_total' || $total['code'] == 'order_total')
                {
                  $total['value'] += $reservation_details['booking_price'] - $data['reward_amount'];
                }
                if($result['order_type'] == '2') {
                  if($total['code'] != 'delivery') {
                    $new_data['totals'][] = array(
                      'code'  => $total['code'],
                      'title' => htmlspecialchars_decode($total['title']),
                      'value' => $total['value'],
                      'priority' => $total['priority'],
                    );
                  }
                } else {
                  $new_data['totals'][] = array(
                    'code'  => $total['code'],
                    'title' => htmlspecialchars_decode($total['title']),
                    'value' => $total['value'],
                    'priority' => $total['priority'],
                  );
                }

              }
              $results[$key]['item_totals'] = $new_data['totals'];

              $results[$key]['options'] = unserialize($result['options']);
              $order_info = $this->Orders_model->getOrder((int) $result['order_id']);
              
              if ($order_info) {
                $results[$key]['order_info'] = $order_info;
                $results[$key]['order_info']['notify_customer'] = $order_info['notify'];
                $results[$key]['order_info']['email'] = $email;
                $menus = $this->Orders_model->getOrderMenus($result['order_id']);
                $results[$key]['menus'] = $menus;
                foreach ($menus as $key1 => $menu) {
                  // $results[$key]['menus'][$key1]['option_values'] = unserialize($menu['option_values']);
                  $option_values = ($menu['option_values']!='') ? unserialize($menu['option_values']) : '';
                  $new_option_values = array();
                  if($option_values != '') {
                    $option_values = (array) $option_values;
                    foreach ($option_values as $opt => $vals) {
                      foreach ($vals as $v) {
                        $new_option_values[] = $v;
                      }
                    }
                  }
                  $results[$key]['menus'][$key1]['option_values'] = (empty($new_option_values)) ? '' : $new_option_values;
                }

                $results[$key]['status_history'] = array();
                $status_history = $this->Statuses_model->getStatusHistories('order', $result['order_id']);
                foreach ($status_history as $history) {
                  $results[$key]['status_history'][] = array(
                    'history_id'  => $history['status_history_id'],
                    'date_time'   => mdate('%d %M %y - %H:%i', strtotime($history['date_added'])),
                    'staff_name'  => $history['staff_name'],
                    'assignee_id' => $history['assignee_id'],
                    'status_id'   => $history['status_id'],
                    'status_name' => $history['status_name'],
                    'status_code' => $history['status_code'],
                    'status_color'  => $history['status_color'],
                    'notify'    => $history['notify'],
                    'comment'   => nl2br($history['comment'])
                  );
                }
                
              } else {
                $results[$key]['order_info'] = array();
                $results[$key]['menus'] = array();
                $results[$key]['status_history'] = array();
              }
              // if ($order_info) {
              //   $order_id = $order_info['order_id'];
              //   $locations = explode(',',$order_info['location_id']);
              //   // print_r($locations);
              //   // exit;
              //   $data['location_id']    = $order_info['location_id'];
              //   foreach ($locations as $key => $location_id) {        
              //     $location = $this->Orders_model->getLocation($location_id);
              //     $results['locations'][$location_id]['location_lat'] = $location['location_lat'];       
              //   }
              //   // print_r($orders);
              //   // exit;
              //   // $data['_action']  = site_url('orders/edit?id='. $order_id.'&res_id='.$reservation_id);
              // } else {
              //     $order_id = 0;
              //   //$data['_action']  = site_url('orders/edit');
              //   // redirect('orders');
              // }
            }
            $pages = $filter['page'] + 10;
            $data['result'] = $results;
            $data['next_offset'] = $pages;
            $data['msg'] = 'success';    
            
            // print_r($results);
            // exit;
          } else {
            $data['result'] = $results;
            $data['msg'] = 'failure';
          }
         print_r(json_encode($data));
         exit;
    }

    public function orderupdate_post() {

          $order_id= $this->input->post('order_id');
          $delivery_id= $this->input->post('delivery_id');
          $order_status= $this->input->post('order_status');
          $lang = $this->input->post('lang');

          $filter = array();
          $order_info = $this->Orders_model->getOrder($order_id);
          $prev_status = $order_info['status_code'];
          $order_type = ($order_info['order_type']=='Pickup' || $order_info['order_type']=='2') ? '2' : '1';
          // echo "<pre>"; print_r($order_info);
          // exit;
          $status_id = $this->input->post('order_status');
          if($order_status != 0 && $order_status < $prev_status) {
            echo json_encode(array(
              'msg'     => ($lang=='en') ? "Status changed down the order, status wont be changed and remains in the same status!" : "สถานะเปลี่ยนตามลำดับสถานะจะไม่เปลี่ยนและยังคงอยู่ในสถานะเดิม!",
              'result'  =>  array()
            ));
            exit;
          }
          if($status_id>=3){
            if( $order_type=='1' ){
              $delivery_id = $this->input->post('delivery_id');
              if($delivery_id=='' || $delivery_id=='0'){
                echo json_encode(array(
                  'msg'     => ($lang=='en') ? "Assign delivery boy on Preparation Status" : "กำหนดเด็กส่งของในสถานะการเตรียมการ",
                  'result'  =>  array()
                ));
                exit;
              }
            }
          }
          if($order_type == '2') {
            if($order_status == 3) {
              $msg = ($lang=='en') ? "Can't assign delivery person for pickup orders" : "ไม่สามารถกำหนดคนส่งของสำหรับใบสั่งรับสินค้า";
            } else if($order_status == 4) {
              $msg = ($lang=='en') ? "Can't update to current status to the Pickup orders" : "ไม่สามารถอัปเดตสถานะปัจจุบันของใบสั่งรับสินค้าได้";
            } else {
              $msg = '';
            }
            if($msg!='') {
              echo json_encode(array(
                'msg'     => $msg,
                'result'  =>  array()
              ));
              exit;
            }
          }
          if($order_info) {
            if ($query = $this->Orders_model->updateOrder($order_id, $this->input->post())) {
              $data['result'] = array();
              $data['order_id'] = $order_id;
              $data['msg'] = 'success';   

              $url = BASEPATH.'/../firebase.json';
              $uid = $this->input->post('delivery_id');
              $project_id = json_decode(file_get_contents($url));
              $db = 'https://'.$project_id->project_id.'.firebaseio.com/';
              // echo $db;
              // exit;
              $serviceAccount = ServiceAccount::fromJsonFile($url);
              $firebase = (new Factory)
                    ->withServiceAccount($serviceAccount)
                    ->withDatabaseUri($db)
                    ->create();
              $database = $firebase->getDatabase();
          

                       $driverstat = $this->Delivery_model->getProfilebyStatus();

                            $drivers = $database->getReference('order_request/')->getValue();
                            foreach ($driverstat as $key => $d_id) {
                             
                              // $drivers = $database->getReference('order_request/')->getValue();

                              if(@$drivers[$d_id['delivery_id']] == $drivers[$uid]){
         
                                  $input = [
                                   'status' => 2,
                                   'accept_status' => 1,
                                  
                                   ];
                                   $newpost = $database->getReference('order_request/'.$uid.'/'.$order_id)->update($input);

                            }
                                 
                               if(@$drivers[$d_id['delivery_id']] != $drivers[$uid]){
         
                                 if(@$drivers[$d_id['delivery_id']][$order_id]){
                                   $newpost = $database->getReference('order_request/'.$d_id['delivery_id'].'/'.$order_id)->remove();
                                 }

                            }
                          

                          } 
                          
              // $prev_id = $this->input->post('prev_del_id');     
              $prev_id = $order_info['delivery_id'];
              $input = [
                'delivery_partners/'.$prev_id.'/status' => 1,
                'delivery_partners/'.$prev_id.'/order_id' => "",
                  'delivery_partners/'.$uid.'/status' => 2,
                  'delivery_partners/'.$uid.'/order_id' => $order_id,
              ];
              $deliv = $this->Delivery_model->getDeliveries($uid);
                $cust = $this->Addresses_model->getAddress($order_info['customer_id'],$order_info['address_id']);
                $ord = $this->Orders_model->getOrderMenus($order_id);
              $datas['amount']          = $this->currency->format($order_info['order_total']);
              $datas['customer_id']         = $order_info['customer_id'];
              $datas['customer_lat']        = floatval($cust['clatitude']);
              $datas['customer_lng']        = floatval($cust['clongitude']);
              $datas['customer_name']       = $order_info['first_name'].' '.$order_info['last_name'];
              $datas['customer_phone']      = $order_info['telephone'];
              $datas['date']            = date('d-M-Y',strtotime($order_info['order_date'])).' '.date('h:i a',strtotime($order_info['order_time']));
              $datas['delivery_partner_phone']  = $deliv['telephone'];
              $datas['drop_lat']          = floatval($cust['clatitude']);
              $datas['drop_lng']          = floatval($cust['clongitude']);
              $datas['eta']['text']       = 'estimating';
              $datas['id']            = $order_id;
              $datas['order_id']          = '#'.str_pad($order_id, 6, '0', STR_PAD_LEFT);;
              $datas['restaurant_address']    = $order_info['location_address_1'].', '.$order_info['location_address_2'].', '.$order_info['location_city'];
              $datas['restaurant_lat']      = floatval($order_info['location_lat']);
              $datas['restaurant_lng']      = floatval($order_info['location_lng']);
              $datas['restaurant_name']       = $order_info['location_name'];
              $datas['restaurant_phone']      = $order_info['location_telephone'];
              $datas['res_img']         = $order_info['location_image'];
              $stat = $this->Statuses_model->getStatuscode($order_info['status_id']);
              $datas['status']          = $_POST['order_status'];
              $statu_name = $this->Statuses_model->getStatuscode($_POST['order_status']);
              $stat_name = $statu_name['status_name'] ;
              $datas['status_name']         = $stat_name;
              if($order_info['payment'] == 'cash'){
                $datas['mode_of_payment']   = 'cash';
              }else{
                $datas['mode_of_payment']   = 'card';
              }
              $datas['ETA']           = $this->Locations_model->getLocationETA($local_info['location_id']);
              $datas['id_proof']          = $order_info['id_proof'];
              $datas['items']           = $ord;
              if($this->input->post('order_status')=='20' || $this->input->post('order_status')=='4') {                
                $datas['is_view'] = 1;
              } else {
                $datas['is_view'] = 0;
              }
              $datas['mode_of_payment']     = $datas['mode_of_payment'];

              $order_input = [
                'orders/'.$uid.'/'.$order_id => $datas,
                
              ];

              if($prev_id != $uid){
                $order_input2 = [
                  'orders/'.$prev_id.'/'.$order_id .'/status_name'=> "Delivery boy canceled",
                  'orders/'.$prev_id.'/'.$order_id .'/status'=> 8,
              ];

              $newpost5 = $database->getReference() 
                ->update($order_input2);

              }

              $datac['delivery_partner'] = $uid;
              $statu_name = $this->Statuses_model->getStatuscode($_POST['order_status']);
              $datac['status'] = $statu_name['status_name'] ;
              $datac['status_id'] =  $_POST['order_status'];
              
              $customer_input =[
                'customer_pendings/'.$order_info['customer_id'].'/'.$order_id => $datac,
              ];

              
              if($uid!=''){
                $newpost3 = $database->getReference('orders/'.$prev_id.'/'.$order_id)->remove();
              $newpost = $database->getReference() 
                ->update($input);
              $newpost1 = $database->getReference() 
                ->update($order_input);
              }

              // $loc_id = $this->input->post('location_id');
              $locations = explode(",",$this->input->post('location_id'));
              // print_r($locations);
              // exit;
                        foreach ($locations as $key => $loc_id) {
                          $location = $this->Reservations_model->getLocation($loc_id);
                          if(isset($location)) {
                            // $key = $loc_id;
                            // echo $loc_id;
                            
                           // $order_id = $reserve['order']['order_id'];
                            $firebase1['shop'][$key]['restaurant_lat'] = floatval($location['location_lat']);
                            $firebase1['shop'][$key]['restaurant_lng'] = floatval($location['location_lng']);
                            $firebase1['shop'][$key]['restaurant_name'] = $location['location_name'];
                            $firebase1['shop'][$key]['checked'] = 0;
                            $firebase1['shop'][$key]['restaurant_phone'] = $location['location_telephone'];
                            $firebase1['shop'][$key]['restaurant_address'] = $location['location_name'].' '.$location['location_city'];
                            $firebase1['shop'][$key]['res_img'] = $location['location_image'];
                            $menu_order = $this->Reservations_model->getOrderMenu($order_id,$key);
                            
                            // print_r($menu_order);
                            // exit;
                            foreach ($menu_order as $men_key => $menu) {
                              $menu_id = $menu['menu_id'];
                               $firebase1['shop'][$key]['items'][$men_key]['name'] = $menu['name'];
                              $firebase1['shop'][$key]['items'][$men_key]['option_name'] = $menu['option_name'];
                              $firebase1['shop'][$key]['items'][$men_key]['option_value_name'] = $menu['option_value_name'];
                              $firebase1['shop'][$key]['items'][$men_key]['quantity'] = $menu['quantity'];
                              $firebase1['shop'][$key]['items'][$men_key]['subtotal'] = $menu['subtotal'];
                              $firebase1['shop'][$key]['items'][$men_key]['price'] = $menu['price'];

                              // $firebase1['shop'][$loc_id]['items'][$menu['menu_id']]['name'] = $menu['name'];
                              // $firebase1['shop'][$loc_id]['items'][$menu['menu_id']]['option_name'] = $menu['option_name'];
                              // $firebase1['shop'][$loc_id]['items'][$menu['menu_id']]['option_value_name'] = $menu['option_value_name'];
                              // $firebase1['shop'][$loc_id]['items'][$menu['menu_id']]['quantity'] = $menu['quantity'];
                              // $firebase1['shop'][$loc_id]['items'][$menu['menu_id']]['subtotal'] = $menu['subtotal'];
                              // $firebase1['shop'][$loc_id]['items'][$menu['menu_id']]['price'] = $menu['price'];
                            }
                            
                          }
                        }
                        // echo $this->input->post('delivery_id');
                        // exit;
                        // echo '<pre>';
                        // print_r($firebase1);
                        // exit;
                        $url = BASEPATH.'/../firebase.json'; 
                        // echo $url;
                        // exit;        
                        $project_id = json_decode(file_get_contents($url));
                        $db = 'https://'.$project_id->project_id.'.firebaseio.com/';
                        $serviceAccount = ServiceAccount::fromJsonFile($url);
                        // print_r($firebase1);
                        // exit();
                        $firebase = (new Factory)
                                    ->withServiceAccount($serviceAccount)
                                    ->withDatabaseUri($db)
                                    ->create();
                        $database = $firebase->getDatabase();
                        if(isset($firebase1)) {                 
                        $newpost5 = $database->getReference('orders/'.$uid.'/'.$order_id.'/') 
                          ->update($firebase1);
                        }
              $newpost2 = $database->getReference('customer_pendings/'.$order_info['customer_id'].'/'.$order_id) 
                ->update($datac);
              if($stat['status_code']=='3'){
                $message = "New Order Placed and preparing";
                $token = $deliv['deviceid'];
              $fcm = $this->Orders_model->fxmsend_post($message,$token);

              }

              if($this->input->post('order_status') == '20') {

                $deliver_id = $prev_id;
                $this->db->where('order_id',$order_id)->update('orders',array('status_id' => '20'));

                $condition = 'order_id ='.$order_id;
                $chk = $this->Delivery_model->CheckSTable('orders',$condition);
                $chk_1 = $chk;

                $locations = explode(',',$chk['location_id']);

                foreach($locations as $location_id)
                {
                  $condition = 'location_id ='.$location_id;
                  $loc = $this->Delivery_model->CheckSTable('locations',$condition);    
                  $commission_amount = round(($chk['order_total'] * $loc['delivery_boy_commission']) / 100, 2);
                  $locat = array(
                    'order_id' => $order_id,
                    'restaurant_id' => $location_id,
                    'delivery_id' => $deliver_id,
                    'date_time' => date('Y-m-d H:i:s'),
                    'today_date' => date('Y-m-d'),
                    'status' => '1',
                    'amount' => $chk['order_total'],
                    'fare' => $commission_amount,
                    'Surge_charge' => 0,
                    'rest_fee' => 0,
                  );
                  $this->db->insert('delivery_booking',$locat);
                }
                $condition = 'delivery_id ='.$deliver_id;
                $chk_del = $this->Delivery_model->CheckSTable('delivery',$condition);    

                $wallt = $chk_del['wallet'] + $commission_amount;
                $this->db->where('delivery_id',$deliver_id);
                $this->db->update('delivery',array('wallet' => $wallt));

                /**********Admin Commission*********/
                $sellerid = $this->Locations_model->getLocation($chk_1['location_id']);
                $sellerid = $sellerid['added_by'];

                $percentage =  $this->Locations_model->getSellerCommission($sellerid);
                $commission_percentage = $percentage[0]['commission'];

                $amt = array( 
                  0 => array(
                    "total_amount" => $chk_1['order_total'],
                    "booking_price" => 0,
                    "order_price" => $chk_1['order_total'],
                    "booking_tax" => 0,
                    "booking_tax_amount" => 0
                  )
                );
                $status_detail = $this->db->where('status_code', $chk_1['status_id'])->get('statuses')->row_array();
                // $status_detail = $this->Statuses_model->getStatus($chk_1['status_id']);
                $this->Locations_model->applyOrderCommission($sellerid,$chk_1['location_id'],$amt,$commission_percentage,$order_id,$status_detail['status_name'],$chk_1['status_code']);

              }
             
              // log_activity($this->user->getStaffId(), 'updated', 'orders', get_activity_message('activity_custom',
              //     array('{staff}', '{action}', '{context}', '{link}', '{item}'),
              //     array($this->user->getStaffName(), 'updated', 'order', current_url(), '#'.$this->input->get('id'))
              // ));

              // if ($this->input->post('assignee_id') AND $this->input->post('old_assignee_id') !== $this->input->post('assignee_id')) {
              //     $staff = $this->Staffs_model->getStaff($this->input->post('assignee_id'));
              //   $staff_assignee = site_url('staffs/edit?id='.$staff['staff_id']);

              //   log_activity($this->user->getStaffId(), 'assigned', 'orders', get_activity_message('activity_assigned',
              //         array('{staff}', '{action}', '{context}', '{link}', '{item}', '{assignee}'),
              //         array($this->user->getStaffName(), 'assigned', 'order', current_url(), '#'.$this->input->get('id'), "<a href=\"{$staff_assignee}\">{$staff['staff_name']}</a>")
              //     ));
              // }

              // $this->alert->set('success', sprintf($this->lang->line('alert_success'), 'Order updated'));
            } else {
               $data['result'] = array();
               $data['msg'] = 'failure';
                // $this->alert->set('warning', sprintf($this->lang->line('alert_error_nothing'), 'updated'));
            }
          } else {
            $data['result'] = array();
            $data['msg'] = 'order not found';
          }
          
          print_r(json_encode($data));
          exit;

    }

    private function validateForm() {
      $this->form_validation->set_rules('staff_name', 'name', 'xss_clean|trim|required|min_length[2]|max_length[128]');
        $this->form_validation->set_rules('staff_email', 'email', 'xss_clean|trim|required|max_length[96]|valid_email|is_unique[staffs.staff_email]');
      // }

      // if ($username !== $this->input->post('username')) {
        $this->form_validation->set_rules('username', 'username', 'xss_clean|trim|required|is_unique[users.username]|min_length[2]|max_length[32]');
      // }
      $this->form_validation->set_rules('telephone', 'phone number', 'xss_clean|trim|required');

      // if (!$this->input->get('id')) {
        $this->form_validation->set_rules('password', 'password', 'xss_clean|trim|required|min_length[6]|max_length[32]|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', 'confirm_password', 'xss_clean|trim|required');
      // }

      // if ($this->user->hasPermission('Admin.StaffGroups.Manage')) {
      //  $this->form_validation->set_rules('staff_group_id', 'lang:label_group', 'xss_clean|trim|required|integer');
      //  $this->form_validation->set_rules('staff_location_id[]', 'lang:label_location', 'xss_clean|trim|integer');
      // }

      // $this->form_validation->set_rules('staff_status', 'lang:label_status', 'xss_clean|trim|integer');
          
      // }
      
      /*$this->form_validation->set_rules('payment_username', 'lang:label_payment_username', 'xss_clean|trim|required');

      $this->form_validation->set_rules('payment_password', 'lang:label_payment_password', 'xss_clean|trim|required');

      $this->form_validation->set_rules('merchant_id', 'lang:label_merchant_id', 'xss_clean|trim|required');

      $this->form_validation->set_rules('payment_key', 'lang:label_payment_key', 'xss_clean|trim|required');*/

      if ($this->form_validation->run() === TRUE) {
        return TRUE;
      } else {
        // return FALSE;
        return json_encode($this->form_validation->error_array());
      }
    }
    public function categoryupdate_post() {
      if ($this->category_validateForm() === TRUE) {
      
      $check = $this->Categories_model->checkCategory($this->post(),$this->post('id'));
      if($check == 1) {
        $msg    = 'Category Already Exist.';          
        $output = array('message'  => $msg);
        echo json_encode($output);
      } else {
        if ($category_id = $this->Categories_model->saveCategory($this->post('id'), $this->post())) {
            $output = array('Category_id'  => $category_id, 
                          'message' => 'Successfully created.'
                      );
            echo json_encode($output);
        } else {
            $output = array( 
                          'message' => 'Category not inserted'
                      );
            echo json_encode($output);
        }
      }
      

      
    }
    else
    {
      $msg    = $this->category_validateForm();          
        $output = array('message'  => $msg);
        echo json_encode($output);
    }
    }
    public function optionupdate_post() {
      if ($this->options_validateForm() === TRUE) {
        
        if ($option_id = $this->Menu_options_model->saveOption($this->input->post('id'), $this->input->post())) {
                $output = array('Option_id'  => $option_id, 
                          'message' => 'Successfully created.'
                      );
            echo json_encode($output);
            } else {
               $output = array( 
                          'message' => 'Option value not inserted'
                      );
            echo json_encode($output);
        }
      
    }
    else
    {
      $msg    = 'Invalid Params.';          
        $output = array('message'  => $msg);
        echo json_encode($output);
    }
    }
    public function itemupdate_post() {
    if ($this->item_validateForm() === TRUE) {
      
      if ($menu_id = $this->Menus_model->saveMenu($this->input->post('id'), $this->input->post())) {
           $output = array('menu_id'  => $menu_id, 
                          'message' => 'Successfully created.'
                      );
            echo json_encode($output);
      } else {
        $output = array( 
                          'message' => 'Items are not inserted'
                      );
            echo json_encode($output);
      }
 
    }
    else
    {
      $msg    = $this->item_validateForm();          
        $output = array('message'  => $msg);
        echo json_encode($output);
    }
    exit;
  }
  public function deliveryupdate_post() {
      $delivery_info = $this->Delivery_model->getDeliveries((int)$this->input->post('id'));
      
      if ($this->delivery_validateForm($delivery_info['email']) === TRUE) {
        $address1=$this->input->post('address');
        $delivery_id=$this->input->post('id');
        if(!empty($address1)){
          $addresses = json_decode($address1,TRUE);
          $this->db->where('address_id!=', $addresses['address_id']);
          $this->db->where('customer_id', $delivery_id);
          $this->db->delete('delivery_addresses');
        }else{
          $this->db->where('customer_id', $delivery_id);
          $this->db->delete('delivery_addresses');
        }

        if ($delivery_id = $this->Delivery_model->saveDelivery($this->input->post('id'), $this->input->post())) {

        
        $delivery_name = $this->input->post('first_name').' '.$this->input->post('last_name');


        $url = BASEPATH.'/../firebase.json';
        $uid1 = $this->Delivery_model->DeliveryId();
        $uid = $delivery_id;
        $project_id = json_decode(file_get_contents($url));
        
        $db = 'https://'.$project_id->project_id.'.firebaseio.com/';
        $serviceAccount = ServiceAccount::fromJsonFile($url);
        $firebase = (new Factory)
              ->withServiceAccount($serviceAccount)
              ->withDatabaseUri($db)
              ->create();
        $database = $firebase->getDatabase();
        
        $input = [
            'delivery_partners/'.$uid.'/status' => 0,
            'delivery_partners/'.$uid.'/l/0' => 0,
            'delivery_partners/'.$uid.'/l/1' => 0,
            'delivery_partners/'.$uid.'/name' => $this->input->post('first_name').' '.$this->input->post('last_name'),
            'delivery_partners/'.$uid.'/profile' =>'profile_images/no-pic.png',
            'delivery_partners/'.$uid.'/phone_number' => $this->input->post('country_code').'-'.$this->input->post('telephone'),
            'delivery_partners/'.$uid.'/delivery_id' => $uid,
            'delivery_partners/'.$uid.'/added_by' => $this->input->post('added_by'),

        ];
        $newpost = $database->getReference()->update($input);
        $output = array('delivery_id'  => $delivery_id, 
                          'message' => 'Successfully created.'
                      );
        echo json_encode($output);
        exit;
      } else {
        $output = array( 
                         'message' => 'Failed to create delivery person.'
                      );
        echo json_encode($output);
        exit;
      }
    }
    else
    {
      $msg    = $this->delivery_validateForm($delivery_info['delivery_email']);          
        $output = array('message'  => $msg);
        echo json_encode($output);
        exit;
    }
  }

  private function delivery_validateForm($delivery_email = FALSE) {
    $this->form_validation->set_rules('first_name', 'lang:label_first_name', 'xss_clean|trim|required|min_length[2]|max_length[32]');
    $this->form_validation->set_rules('last_name', 'lang:label_last_name', 'xss_clean|trim|required|min_length[2]|max_length[32]');

    if ($delivery_email !== $this->input->post('email')) {
      $this->form_validation->set_rules('email', 'lang:label_email', 'xss_clean|trim|required|valid_email|max_length[96]|is_unique[delivery.email]');
    }
    if (! $this->input->post('id')) {
      $this->form_validation->set_rules('password', 'lang:label_password', 'xss_clean|trim|required|min_length[6]|max_length[40]|matches[confirm_password]');
      $this->form_validation->set_rules('confirm_password', 'lang:label_confirm_password', 'xss_clean|trim|required');
    }

    $this->form_validation->set_rules('telephone', 'lang:label_telephone', 'xss_clean|trim|required|integer');
    $this->form_validation->set_rules('security_question_id', 'lang:label_security_question', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('security_answer', 'lang:label_security_answer', 'xss_clean|trim|min_length[2]');
    $this->form_validation->set_rules('newsletter', 'lang:label_newsletter', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('delivery_group_id', 'lang:label_delivery_group', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('status', 'lang:label_status', 'xss_clean|trim|required|integer');
    $this->form_validation->set_rules('vip_status', 'lang:label_vip', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('bank_name', 'lang:bank_name', 'xss_clean|trim|required|min_length[2]|max_length[32]');
    $this->form_validation->set_rules('account_number', 'lang:account_number', 'xss_clean|trim|required|min_length[2]|max_length[32]');
    $this->form_validation->set_rules('routing_number', 'lang:routing_number', 'xss_clean|trim|required|min_length[2]|max_length[32]');
    $this->form_validation->set_rules('account_name', 'lang:account_name', 'xss_clean|trim|required|min_length[2]|max_length[32]');
    $this->form_validation->set_rules('wallet', 'lang:label_wallet', 'xss_clean|trim|numeric');
    $this->form_validation->set_rules('capital_amount', 'lang:label_capital_amount', 'xss_clean|trim|numeric');

    /*if ($this->input->post('address')) {
      
        $this->form_validation->set_rules('address[address_1]', 'lang:label_address_1', 'xss_clean|trim|required|min_length[3]|max_length[128]');
        $this->form_validation->set_rules('address[city]', 'lang:label_city', 'xss_clean|trim|required|min_length[2]|max_length[128]');
        $this->form_validation->set_rules('address[state]', ' lang:label_state', 'xss_clean|trim|max_length[128]');
        $this->form_validation->set_rules('address[postcode]', ' lang:label_postcode', 'xss_clean|trim|min_length[2]|max_length[10]');
        $this->form_validation->set_rules('address[country_id]', ' lang:label_country', 'xss_clean|trim|required|integer');
      
    }*/

    if ($this->form_validation->run() === TRUE) {
      return TRUE;
    } else {
      return json_encode($this->form_validation->error_array());
    }
  }
    private function category_validateForm() {
    $this->form_validation->set_rules('name', 'lang:label_name', 'xss_clean|trim|required|min_length[2]|max_length[32]');
    $this->form_validation->set_rules('description', 'lang:label_description', 'xss_clean|trim|min_length[2]|max_length[1028]');
    $this->form_validation->set_rules('slug', 'lang:label_permalink_slug', 'xss_clean|trim|alpha_dash|max_length[255]');
    $this->form_validation->set_rules('parent_id', 'lang:label_parent', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('image', 'lang:label_image', 'xss_clean|trim');
    $this->form_validation->set_rules('status', 'lang:label_status', 'xss_clean|trim|required|integer');

    if ($this->form_validation->run() === TRUE) {
      return TRUE;
    } else {
      return json_encode($this->form_validation->error_array());
    }
  }
  private function options_validateForm() {
    $this->form_validation->set_rules('option_name', 'lang:label_option_name', 'xss_clean|trim|required|min_length[2]|max_length[32]');
    //$this->form_validation->set_rules('display_type', 'lang:label_display_type', 'xss_clean|trim|required|alpha');
    
    if ($this->input->post('option_values')) {
      foreach ($this->input->post('option_values') as $key => $value) {
        $this->form_validation->set_rules('option_values['.$key.'][value]', 'lang:label_option_value', 'xss_clean|trim|required|min_length[2]|max_length[128]');
        
      }
    }

    if ($this->form_validation->run() === TRUE) {
      return TRUE;
    } else {
      return json_encode($this->form_validation->error_array());
    }
   }
   private function item_validateForm() {
    $this->form_validation->set_rules('menu_name', 'lang:label_name', 'xss_clean|trim|required|min_length[2]|max_length[255]');
    $this->form_validation->set_rules('menu_description', 'lang:label_description', 'xss_clean|trim|min_length[2]|max_length[1028]');
    $this->form_validation->set_rules('menu_price', 'lang:label_price', 'xss_clean|trim|required|numeric');
    $this->form_validation->set_rules('menu_category', 'lang:label_category', 'xss_clean|trim|required|integer');
    $this->form_validation->set_rules('menu_photo', 'lang:label_photo', 'xss_clean|trim');
    $this->form_validation->set_rules('stock_qty', 'lang:label_stock_qty', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('minimum_qty', 'lang:label_minimum_qty', 'xss_clean|trim|required|integer');
    $this->form_validation->set_rules('subtract_stock', 'lang:label_subtract_stock', 'xss_clean|trim|required|integer');
    $this->form_validation->set_rules('menu_status', 'lang:label_status', 'xss_clean|trim|required|integer');
    $this->form_validation->set_rules('menu_priority', 'lang:label_menu_priority', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('special_status', 'lang:label_special_status', 'xss_clean|trim|required|integer');
    $this->form_validation->set_rules('location_id', 'lang:label_location', 'xss_clean|trim|required');

    if ($this->input->post('menu_options')) {
      $mi_arr= array();
      foreach ($this->input->post('menu_options') as $key => $value) {
        $this->form_validation->set_rules('menu_options[' . $key . '][menu_option_id]', 'lang:label_option', 'xss_clean|trim|integer');
        $this->form_validation->set_rules('menu_options[' . $key . '][option_id]', 'lang:label_option_id', 'xss_clean|trim|required|integer');
        $this->form_validation->set_rules('menu_options[' . $key . '][option_name]', 'lang:label_option_name', 'xss_clean|trim|required');
        $this->form_validation->set_rules('menu_options[' . $key . '][display_type]', 'lang:label_option_display_type', 'xss_clean|trim|required');
        $this->form_validation->set_rules('menu_options[' . $key . '][default_value_id]', 'lang:label_default_value_id', 'xss_clean|trim|integer');
        $this->form_validation->set_rules('menu_options[' . $key . '][priority]', 'lang:label_option_id', 'xss_clean|trim|required|integer');
        $this->form_validation->set_rules('menu_options[' . $key . '][required]', 'lang:label_option_required', 'xss_clean|trim|required|integer');

        foreach ($value['option_values'] as $option => $option_value) {
          $mi_arr[$key][] = $option_value['option_value_id'];
          
          $this->form_validation->set_rules('menu_options[' . $key . '][option_values][' . $option . '][option_value_id]', 'lang:label_option_value', 'xss_clean|trim|required|integer');
          $this->form_validation->set_rules('menu_options[' . $key . '][option_values][' . $option . '][price]', 'lang:label_option_price', 'xss_clean|trim|numeric|required');
          $this->form_validation->set_rules('menu_options[' . $key . '][option_values][' . $option . '][subtract_stock]', 'lang:label_option_subtract_stock', 'xss_clean|trim|numeric');
          $this->form_validation->set_rules('menu_options[' . $key . '][option_values][' . $option . '][menu_option_value_id]', 'lang:label_option_value_id', 'xss_clean|trim|numeric');
        }
      }
      foreach ($mi_arr as $mi) {
        
        if(count($mi) > count(array_unique($mi))) {
        $this->alert->set('error', sprintf('You have selected same product option multiple times please remove.'));
        return false; 
      }
      }
    }

    if ($this->input->post('special_status') === '1') {
      $this->form_validation->set_rules('start_date', 'lang:label_start_date', 'xss_clean|trim|required');
      $this->form_validation->set_rules('end_date', 'lang:label_end_date', 'xss_clean|trim|required');
      $this->form_validation->set_rules('special_price', 'lang:label_special_price', 'xss_clean|trim|required|numeric');
    }

    if ($this->form_validation->run() === TRUE) {
      return TRUE;
    } else {
      return json_encode($this->form_validation->error_array());
    }
  } 
   public function imageUpload_post(){

        if($_FILES){

            $name = time().'_'.basename($_FILES["image"]["name"]);
            $target_dir = '../assets/images/data/'.$name;
            $profile_picture = $name;

            $imagename = 'data/'.$name;

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir)) {
                $result['result']['image'] = $imagename;
                $result['result']['message'] = "Successfully uploaded";
            } else {
                $result['result']['message'] = "Sorry, there was an error uploading your file.";
            }
            echo json_encode($result);
        }

    }

    public function restaurantupdate_post() {
      $post = $this->input->post();
      $country = $this->Countries_model->getCountryByName($post['location_country_name']);
      $post['address']['address_1'] = $post['location_address_1'];
      $post['address']['address_2'] = $post['location_address_2'];
      $post['address']['city'] = $post['location_city'];
      $post['address']['state'] = $post['location_state'];
      $post['address']['postcode'] = $post['location_postcode'];
      $post['address']['country'] = ($country) ? $country['country_id'] : 0;
      $post['address']['location_lat'] = $post['location_lat'];
      $post['address']['location_lng'] = $post['location_lng'];

      $post['email'] = $post['location_email'];
      $post['telephone'] = $post['location_telephone'];
      $post['gallery'] = json_decode($post['gallery'], TRUE);
      $post['payments'] = json_decode($post['payments'], TRUE);
      $post['description'] = $post['location_description'];
      // $post['veg_type'] = implode(',', json_decode($post['veg_type'], TRUE));
      $post['veg_type'] = json_decode($post['veg_type'], TRUE)[0];
      $post['permalink'] = json_decode($post['permalink'], TRUE);

      $post['tables'] = json_decode($post['location_tables'], TRUE);
      $post['tables'] = array_map(function($val) { return intval($val); }, $post['tables']);
      
      $post['options'] = json_decode($post['options'], TRUE);
      $post['options']['gallery'] = $post['gallery'];
      $post['options']['payments'] = $post['payments'];
      unset($post['gallery']);
      unset($post['payments']);
      unset($post['location_tables']);
      // echo "<pre>"; print_r($post); exit;
        if ($location_id = $this->Locations_model->saveLocation($this->input->post('location_id'), $post)) {
             $output = array('location_id'  => $location_id, 
                            'message' => 'Successfully updated.'
                        );
              echo json_encode($output);
        } else {
          $output = array( 
                            'message' => 'Location updation failed'
                        );
              echo json_encode($output);
        }
        exit;
    }

    private function restaurant_validateForm() {
    $this->form_validation->set_rules('location_name', 'lang:label_name', 'xss_clean|trim|required|min_length[2]|max_length[32]');
      $this->form_validation->set_rules('email', 'lang:label_email', 'xss_clean|trim|required|valid_email');
      $this->form_validation->set_rules('telephone', 'lang:label_telephone', 'xss_clean|trim|required|min_length[2]|max_length[25]');
      $this->form_validation->set_rules('address[address_1]', 'lang:label_address_1', 'xss_clean|trim|required|min_length[2]|max_length[128]');
    $this->form_validation->set_rules('address[address_2]', 'lang:label_address_2', 'xss_clean|trim|max_length[128]');
    $this->form_validation->set_rules('address[city]', 'lang:label_city', 'xss_clean|trim|required|min_length[2]|max_length[128]');
    $this->form_validation->set_rules('address[state]', 'lang:label_state', 'xss_clean|trim|max_length[128]');
    $this->form_validation->set_rules('address[postcode]', 'lang:label_postcode', 'xss_clean|trim|min_length[2]|max_length[10]');
    $this->form_validation->set_rules('address[country]', 'lang:label_country', 'xss_clean|trim|required');

    if ($this->input->post('offer_delivery') == '1') {
      $this->form_validation->set_rules('delivery_fee', 'lang:label_delivery_fee', 'xss_clean|trim|integer');
    }
      $this->form_validation->set_rules('auto_lat_lng', 'lang:label_auto_lat_lng', 'xss_clean|trim|required|integer');
      if ($this->input->post('auto_lat_lng') === '1') {
        $this->form_validation->set_rules('auto_lat_lng', 'lang:label_auto_lat_lng', 'get_lat_lng[address]');
      } else {
        $this->form_validation->set_rules('address[location_lat]', 'lang:label_latitude', 'xss_clean|trim|required|numeric');
        $this->form_validation->set_rules('address[location_lng]', 'lang:label_longitude', 'xss_clean|trim|required|numeric');
      }

    $this->form_validation->set_rules('description', 'lang:label_description', 'xss_clean|trim|min_length[2]|max_length[3028]');
    $this->form_validation->set_rules('offer_delivery', 'lang:label_offer_delivery', 'xss_clean|trim|required|integer');
    $this->form_validation->set_rules('offer_collection', 'lang:label_offer_collection', 'xss_clean|trim|required|integer');
    $this->form_validation->set_rules('delivery_boy_commission', 'lang:label_delivery_boy_commission', 'xss_clean|trim');
    $this->form_validation->set_rules('minimum_order', 'lang:label_minimum_order', 'xss_clean|trim');
    $this->form_validation->set_rules('delivery_time', 'lang:label_delivery_time', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('collection_time', 'lang:label_collection_time', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('last_order_time', 'lang:label_last_order_time', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('minimum_advance_time', 'lang:label_minimum_advance_time', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('minimum_advance_days[]', 'lang:label_minimum_advance_days', 'xss_clean|trim');
    $this->form_validation->set_rules('future_orders', 'lang:label_future_orders', 'xss_clean|trim|required|integer');
    $this->form_validation->set_rules('future_order_days[]', 'lang:label_future_order_days', 'xss_clean|trim|required|integer');
    $this->form_validation->set_rules('payments[]', 'lang:label_payments', 'xss_clean|trim');
    $this->form_validation->set_rules('tables[]', 'lang:label_tables', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('reservation_time_interval', 'lang:label_interval', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('reservation_stay_time', 'lang:label_turn_time', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('location_status', 'lang:label_status', 'xss_clean|trim|required|integer');
    $this->form_validation->set_rules('permalink[permalink_id]', 'lang:label_permalink_id', 'xss_clean|trim|integer');
    $this->form_validation->set_rules('permalink[slug]', 'lang:label_permalink_slug', 'xss_clean|trim|alpha_dash|max_length[255]|required');
        $this->form_validation->set_rules('location_image', 'lang:label_image', 'xss_clean|trim');
        $this->form_validation->set_rules('subscription_fees', 'lang:label_subscription_fees', 'xss_clean|trim|required|numeric');

    $this->form_validation->set_rules('opening_type', 'lang:label_opening_type', 'xss_clean|trim|required|alpha_dash|max_length[10]');
    if ($this->input->post('opening_type') === 'daily' AND $this->input->post('daily_days')) {
      $this->form_validation->set_rules('daily_days[]', 'lang:label_opening_days', 'xss_clean|trim|required|integer');
      $this->form_validation->set_rules('daily_hours[open]', 'lang:label_open_hour', 'xss_clean|trim|required|valid_time');
      $this->form_validation->set_rules('daily_hours[close]', 'lang:label_close_hour', 'xss_clean|trim|required|valid_time');
    }

    if ($this->input->post('opening_type') === 'flexible' AND $this->input->post('flexible_hours')) {
      foreach ($this->input->post('flexible_hours') as $key => $value) {
        $this->form_validation->set_rules('flexible_hours['.$key.'][day]', 'lang:label_opening_days', 'xss_clean|trim|required|numeric');
        $this->form_validation->set_rules('flexible_hours['.$key.'][open]', 'lang:label_open_hour', 'xss_clean|trim|required|valid_time');
        $this->form_validation->set_rules('flexible_hours['.$key.'][close]', 'lang:label_close_hour', 'xss_clean|trim|required|valid_time');
        $this->form_validation->set_rules('flexible_hours['.$key.'][status]', 'lang:label_opening_status', 'xss_clean|trim|required|integer');
      }
    }

    if ($this->input->post('delivery_areas')) {
      foreach ($this->input->post('delivery_areas') as $key => $value) {
        $this->form_validation->set_rules('delivery_areas['.$key.'][shape]', '['.$key.'] '.$this->lang->line('label_area_shape'), 'trim|required');
        $this->form_validation->set_rules('delivery_areas['.$key.'][circle]', '['.$key.'] '.$this->lang->line('label_area_circle'), 'trim|required');
        $this->form_validation->set_rules('delivery_areas['.$key.'][vertices]', '['.$key.'] '.$this->lang->line('label_area_vertices'), 'trim|required');
        $this->form_validation->set_rules('delivery_areas['.$key.'][type]', '['.$key.'] '.$this->lang->line('label_area_type'), 'xss_clean|trim|required');
        $this->form_validation->set_rules('delivery_areas['.$key.'][name]', '['.$key.'] '.$this->lang->line('label_area_name'), 'xss_clean|trim|required');

        if ($this->input->post('delivery_areas['.$key.'][charge]')) {
          foreach ($this->input->post('delivery_areas['.$key.'][charge]') as $k => $v) {
            $this->form_validation->set_rules('delivery_areas[' . $key . '][charge][' . $k . '][amount]', '['.$key.'] '.$this->lang->line('label_area_charge'), 'xss_clean|trim|required|numeric');
            $this->form_validation->set_rules('delivery_areas[' . $key . '][charge][' . $k . '][condition]', '['.$key.'] '.$this->lang->line('label_charge_condition'), 'xss_clean|trim|required|alpha_dash');
            $this->form_validation->set_rules('delivery_areas[' . $key . '][charge][' . $k . '][total]', '[' . $key . '] ' . $this->lang->line('label_area_min_amount'), 'xss_clean|trim|numeric');

            if ($this->input->post('delivery_areas[' . $key . '][charge][' . $k . '][condition]') !== 'all') {
              $this->form_validation->set_rules('delivery_areas[' . $key . '][charge][' . $k . '][total]', '[' . $key . '] ' . $this->lang->line('label_area_min_amount'), 'required');
            }
          }
        }
      }
    }

      $this->form_validation->set_rules('gallery[title]', 'lang:label_gallery_title', 'xss_clean|trim|max_length[128]');
      $this->form_validation->set_rules('gallery[description]', 'lang:label_gallery_description', 'xss_clean|trim|max_length[255]');
      if ($this->input->post('gallery')) {
      foreach ($this->input->post('gallery') as $key => $value) {
        if ($key === 'images') foreach ($value as $key => $image) {
          $this->form_validation->set_rules('gallery[images][' . $key . '][name]', 'lang:label_gallery_image_name', 'xss_clean|trim|required');
          $this->form_validation->set_rules('gallery[images][' . $key . '][path]', 'lang:label_gallery_image_thumbnail', 'xss_clean|trim|required');
          $this->form_validation->set_rules('gallery[images][' . $key . '][alt_text]', 'lang:label_gallery_image_alt', 'xss_clean|trim');
          $this->form_validation->set_rules('gallery[images][' . $key . '][status]', 'lang:label_gallery_image_status', 'xss_clean|trim|required|integer');
        }
      }
    }
    if ($this->input->post('reward_status') == '1') {
      $this->form_validation->set_rules('rewards_value', 'lang:label_rewards_points', 'xss_clean|trim|integer');
    } 
    
    if ($this->input->post('point_value')) {
      $this->form_validation->set_rules('point_value', 'lang:label_point_system', 'xss_clean|trim|integer');
    }
    if ($this->input->post('point_price')) {
      $this->form_validation->set_rules('point_price', 'lang:label_point_price', 'xss_clean|trim|integer');
    }
    if ($this->input->post('minimum_price')) {
      $this->form_validation->set_rules('minimum_price', 'lang:label_minimum_price', 'xss_clean|trim|integer|required');
    }
    if ($this->input->post('refund_status') == '1' && $this->input->post('cancellation_period') == '') {
      //$this->form_validation->set_rules('cancellation_period', 'lang:label_cancellation_period', 'xss_clean|trim|required');
    } 
    if ($this->input->post('cancellation_charge')) {
      //$this->form_validation->set_rules('cancellation_charge', 'lang:label_cancellation_charge', 'xss_clean|trim');
    }

  //  $this->form_validation->set_rules('first_table_price', 'lang:label_first_table_price', 'xss_clean|trim|required|integer');
//
  //  $this->form_validation->set_rules('additional_table_price', 'lang:label_additional_table_price', 'xss_clean|trim|required|integer');

    if ($this->form_validation->run() === TRUE) {
      return TRUE;
    } else {
      return FALSE;
    }
  }
   public function deleteCategory_post() {
        if ($this->input->post('category_id')) {
            $deleted_rows = $this->Categories_model->deleteCategory($this->input->post('category_id'));

            if ($deleted_rows > 0) {
                $output = array(
                            'message' => 'Successfully Deleted.'
                        );
              echo json_encode($output);
            } else {
                $output = array(
                            'message' => 'Not Deleted.'
                        );
              echo json_encode($output);
            }

        }
        else
        {
          $output = array(
                            'message' => 'Empty Category ID.'
                        );
              echo json_encode($output);
        }
  }
  public function deleteMenu_post() {
    if ($this->input->post('menu_id')) {
      $deleted_rows = $this->Menus_model->deleteMenu($this->input->post('menu_id'));

      if ($deleted_rows > 0) {
        $output = array(
                            'message' => 'Successfully Deleted.'
                        );
              echo json_encode($output);
      } else {
        $output = array(
                            'message' => 'Not Deleted.'
                        );
              echo json_encode($output);
      }
 
    }
    else
        {
          $output = array(
                            'message' => 'Empty Menu ID.'
                        );
              echo json_encode($output);
        }
  }
   public function deleteOption_post() {
        
        if ($this->input->post('option_id')) {
            $deleted_rows = $this->Menu_options_model->deleteOption($this->input->post('option_id'));

            if ($deleted_rows > 0) {
                $output = array(
                            'message' => 'Successfully Deleted.'
                        );
                echo json_encode($output);
            } else {
                $output = array(
                            'message' => 'Not Deleted'
                        );
              echo json_encode($output);
            }

            
        }
        else
        {
          $output = array(
                            'message' => 'Empty Option ID.'
                        );
              echo json_encode($output);
        }
  }

  public function deleteDelivery_post() {
    if ($this->input->post('delivery_id')) {
      $deleted_rows = $this->Delivery_model->deleteDelivery($this->input->post('delivery_id'));

      if ($deleted_rows > 0) {
       $output = array(
                            'message' => 'Successfully Deleted.'
                        );
                echo json_encode($output);
      } else {
       $output = array(
                            'message' => 'Not Deleted'
                        );
              echo json_encode($output);
      }
    }
     else
        {
          $output = array(
                            'message' => 'Empty Delivery ID.'
                        );
              echo json_encode($output);
        }
  }
  public function countrylist_post() {
      $country_rows = $this->Countries_model->getCountries();

      if (isset($country_rows)) {
       $output = array(
                            'result' => $country_rows,
                            'message' => 'Success'
                        );
                echo json_encode($output);
      } else {
       $output = array(
                            'message' => 'No Data'
                        );
              echo json_encode($output);
      }
    
  }
   public function restauranttypelist_post() {
      // $types_rows = $this->Types_model->getTypes();
    $types_rows = array(
      array('id'=>'both', 'type_name'=>'Both', 'type_value'=>'', 'type_icon'=>'', 'status'=>'1', 'created_at'=>''),
      array('id'=>'restaurant', 'type_name'=>'Restaurant', 'type_value'=>'', 'type_icon'=>'', 'status'=>'1', 'created_at'=>''),
      array('id'=>'cafe', 'type_name'=>'Cafe', 'type_value'=>'', 'type_icon'=>'', 'status'=>'1', 'created_at'=>''),
    );

      if (isset($types_rows)) {
       $output = array(
                            'result' => $types_rows,
                            'message' => 'Success'
                        );
                echo json_encode($output);
      } else {
       $output = array(
                            'message' => 'No Data'
                        );
              echo json_encode($output);
      }
    
  }

  public function foodtypelist_post() {
      // $types_rows =  $this->Types_model->getFoodTypes();
    $types_rows = array(
      array('type_name' => 'Both', 'id' => 'both'),
      array('type_name' => 'Vegetarian', 'id' => 'veg'),
      array('type_name' => 'Non-Vegetarian', 'id' => 'nonveg'),
    );
      
      if (isset($types_rows)) {
         
        foreach ($types_rows as $key =>  $res) {
          $result[$key]['label']= $res['type_name'];
          $result[$key]['value']= $res['id'];
        }

       $output = array(
                            'result' => $result,
                            'message' => 'Success'
                        );
                echo json_encode($output);
      } else {
       $output = array(
                            'message' => 'No Data'
                        );
              echo json_encode($output);
      }
    
  }


  public function tablelist_post() {
   
      $types_rows =  $this->Types_model->getTables($this->input->post('location_id'));
      
      if (isset($types_rows)) {
         
        foreach ($types_rows as $key =>  $res) {
          $result[$key]['label']= $res['table_name'];
          $result[$key]['value']= $res['table_id'];
          $result[$key]['min_capacity']= $res['min_capacity'];
          $result[$key]['max_capacity']= $res['max_capacity'];
        }

       $output = array(
                            'result' => $result,
                            'message' => 'Success'
                        );
                echo json_encode($output);
      } else {
       $output = array(
                            'message' => 'No Data'
                        );
              echo json_encode($output);
      }
  
  }

  public function deletetable_post() {
        if ($this->input->post('restaurant_id') && $this->input->post('table_id')) {
          $count = $this->Types_model->findReservations($this->input->post('restaurant_id'),$this->input->post('table_id')); 
                  
            if($count > 0){
               $output = array(
                            'result' => $result,
                            'message' => 'This table already reserved'
                        );
                echo json_encode($output);
              
            }else{              
              $result = $this->Types_model->deleteTable($this->input->post('restaurant_id'),$this->input->post('table_id'));
              $output = array(
                            'result' => $result,
                            'message' => 'Success'
                        );
                echo json_encode($output);
            }
          
        

            //return TRUE;
        }
      }

      public function alltables_post() {

          $filter = array();
          if ($this->input->post('page')) {
            $filter['page'] = (int) $this->input->post('page');
          } else {
            $filter['page'] = '';
          }
          $filter['limit'] = 10;
          if ($this->input->post('filter_search')) {
            $filter['filter_search'] = $data['filter_search'] = $this->input->post('filter_search');
          } else { 
            $filter['filter_search'] = '';
          }
   
      $types_rows =  $this->Types_model->getAllTables($filter,$this->input->post('location_id'), $this->input->post('staff_id'));
      
      if (isset($types_rows)) {
         
        foreach ($types_rows as $key =>  $res) {
          $result[$key]['table_name']= $res['table_name'];
          $result[$key]['table_id']= $res['table_id'];
          $result[$key]['min_capacity']= $res['min_capacity'];
          $result[$key]['max_capacity']= $res['max_capacity'];
          $result[$key]['status']= $res['table_status'];
        }

       $output = array(
                            'result' => $result,
                            'message' => 'Success'
                        );
                echo json_encode($output);
      } else {
       $output = array(
                            'message' => 'No Data'
                        );
              echo json_encode($output);
      }
  
  }

  public function addtable_post() {

          $data = $this->Types_model->saveTable($this->input->post('id'), $this->input->post()); 
                  
            if($data == FALSE){
               $output = array(
                            'result' => null,
                            'message' => 'Failed'
                        );
                echo json_encode($output);
              
            }else{              
              $output = array(
                            'result' => null,
                            'message' => 'Success'
                        );
                echo json_encode($output);
            }
      }

      public function tabledelete_post() {

          $data = $this->Types_model->tableDelete($this->input->post('table_id')); 
                  
            if($data > 0){
              
              $output = array(
                            'result' => null,
                            'message' => 'Success'
                        );
                echo json_encode($output);
               
            }else{   

              $output = array(
                            'result' => null,
                            'message' => 'Failed'
                        );
                echo json_encode($output);
              
            }
      }

  public function reservations_post() {

    $filter = array();    
    $data = array();
    $staff_id = $this->input->post('staff_id');
    // echo $staff_id;
    // exit;
    if($this->input->post('location_id') != '') {
      $filter['filter_location'] = $this->input->post('location_id');
    }
    
    if($this->input->post('filter_status') != '') {
      $filter['filter_status'] = $this->input->post('filter_status');
    }
    
    if($this->input->post('filter_date') != '') {
      $filter['filter_date'] = $this->input->post('filter_date');
    }

    if($this->input->post('filter_search') != '') {
      $filter['filter_search'] = $this->input->post('filter_search');
    }
    
    $results = $this->Reservations_model->getList($filter,$staff_id);  
    $reserve_dates = $this->Reservations_model->getReservationDates();
    foreach ($reserve_dates as $reserve_date) {
      $month_year = $reserve_date['year'].'-'.$reserve_date['month'];
      $data['reserve_dates'][$month_year] = mdate('%F %Y', strtotime($reserve_date['reserve_date']));
    }
    $data['statuses'] = array();
    $data['reservations'] = array();
    $statuses = $this->Statuses_model->getStatuses('reserve');
    foreach ($statuses as $status) {
      $data['statuses'][] = array(
        'status_id' => $status['status_id'],
        'status_name' => $status['status_name'],
        'status_code' => $status['status_code'],
        'status_comment' => $status['status_comment']
      );
    }
    // print_r($data['reserve_dates']);
    // exit;
    foreach ($results as $result) {
      $data['reservations'][] = array(
        'id'  => $result['id'],
        'reservation_id'  => $result['reservation_id'],
        'location_name'   => $result['location_name'],
        'first_name'    => $result['first_name'],
        'last_name'     => $result['last_name'],
        'guest_num'     => $result['guest_num'],
        'otp'       => $result['otp'],
        'table_name'    => $this->Reservations_model->getReservationTables($result['reservation_id']),
                'status_name'   => $result['status_name'],
                'status_code'   => $result['status_code'],
                'status_color'    => $result['status_color'],
        'staff_name'    => $result['staff_name'],
        'reserve_date'    => ($result['reserve_date']),
        'reserve_time'    => mdate('%H:%i', strtotime($result['reserve_time'])),
        'added_date'    => ($result['reserved_on']),
        // 'edit'        => site_url('reservations/edit?id=' . $result['id'].'&res_id='.$result['reservation_id'])
        
      );
    }
    if(count($data['reservations']) > 0 ) {
      $result1['message'] =  'success';
      $result1['result'] = $data;
    } else {
      $result1['message'] =  'failure';
      $result1['result'] = $data;
    }
    print_r(json_encode($result1));

  }

  public function get_reservation_post() {

    $filter = array();    
    $id = $this->input->post('id');
    $data['statuses'] = array();
    $data['result'] = array();
    // echo $staff_id;
    // exit;
    if($this->input->post('location_id') != '') {
      $filter['filter_location'] = $this->input->post('location_id');
    }
    $statuses = $this->Statuses_model->getStatuses('reserve');
    foreach ($statuses as $status) {
      $data['statuses'][] = array(
        'status_id' => $status['status_id'],
        'status_name' => $status['status_name'],
        'status_code' => $status['status_code'],
        'status_comment' => $status['status_comment']
      );
    }
    $data['result'] = $this->Reservations_model->getReservation($id);  
    if($data['result']) {
      $result1['message'] =  'success';
      $result1['result'] = $data;
    } else {
      $result1['message'] =  'failure';
      $result1['result'] = $data;
    }
    print_r(json_encode($result1));
    exit;
  }

  public function reservation_update_post() {
    // echo '<pre>';
    // print_r($this->input->post());
    $reservation_id = $this->input->post('id');
    $update = $this->input->post();
    $res_id = $this->input->post('res_id');
    $loc_id = $this->input->post('loc_id');
    $stf_id = $this->input->post('staff_id');
    $updatee = $this->Reservations_model->updateReservation($reservation_id, $update,$res_id,$loc_id,$stf_id);
    if($updatee === TRUE) {
      $result['message'] = 'Success';
      $result['result'] = null;
    } else {
      $result['message'] = 'Failure';
      $result['result'] = null;
    }
    print_r(json_encode($result));
    exit;
  }

  public function cash_payments_post() {
    $data['payments'] = array();
    $reservation_id = $this->input->post('reservation_id');
    $vendor_id = $this->input->post('staff_id');
    $location_id = $this->input->post('location_id');
    
    if($location_id !='') {
      $results = $this->Cash_payments_model->getFullList($location_id,$vendor_id,$reservation_id);  
    } else {
      $results = $this->Cash_payments_model->getList($vendor_id);
    }
    foreach ($results as $result) {
      // $total_amount = strval(round($result['total_amount'],2));
      $total_amount =  number_format($result['total_amount'], 2, '.', '');
      $data['payments'][] = array(
        'id'          => $result['id'],
        'staff_id'        => $result['staff_name'],
        'loc_id'        => $result['location_id'],
        'location_id'     => $result['location_name'],
        'reservation_id'    => $result['reservation_id'],
        'percentage'      => $result['percentage'],
        'total_amount'      => $total_amount,
        'table_amount'      => strval(round($result['table_amount'],2)),
        'vendor_commission_amount' => strval(round($result['total_amount'],2) - round($result['commission_amount'],2)),
        'commission_amount'   => strval(round($result['commission_amount'],2)),
        'total_commission'    => strval(round($result['total_commission'], 2)),
        'date'          => $result['date']        
        
      );
    }
    // exit;
    if($results) {
      $result1['message'] = 'Success';
      $result1['result'] = $data['payments'];
    } else {
      $result1['message'] = 'Failure';
      $result1['result'] = $data['payments'];
    }
    print_r(json_encode($result1));
  }

  public function online_payments_post() {
    $data['payments'] = array();
    $reservation_id = $this->input->post('reservation_id');
    $vendor_id = $this->input->post('staff_id');
    $location_id = $this->input->post('location_id');
    if($location_id !='') {
      $results = $this->Online_payments_model->getFullList($location_id,$vendor_id,$reservation_id);  
      foreach ($results as $result) {           
        // $total_amount =  number_format($result['total_amount'], 2, '.', '');
        $data['payments'][] = array(
          'id'          => $result['id'],
          'staff_id'        => $result['staff_name'],
          'loc_id'        => $result['location_id'],
          'location_id'     => $result['location_name'],
          'reservation_id'    => $result['reservation_id'],
          'percentage'      => $result['percentage'],
          'total_amount'      => strval(round($result['total_amount'],2)),
          'table_amount'      => strval(round($result['table_amount'],2)),
          'vendor_commission_amount' => strval(round($result['total_amount'],2) - round($result['commission_amount'],2)),
          'commission_amount'   => $result['commission_amount'],
          'date'          => $result['date']        
          
        );
      }  
    } else {
      $results = $this->Online_payments_model->getList($vendor_id);
      foreach ($results as $result) {           

        $data['payments'][] = array(
          'id'          => $result['id'],
          'staff_id'        => $result['staff_name'],
          'loc_id'        => $result['location_id'],
          'location_id'     => $result['location_name'],
          'reservation_id'    => $result['reservation_id'],
          'percentage'      => $result['percentage'],
          'total_amount'      => strval(round($result['total_amount'],2)),
          'table_amount'      => strval(round($result['table_amount'],2)),
          'vendor_commission_amount' => strval(round($result['total_amount'],2) - round($result['total_commission'],2)),
          'commission_amount'   => strval(round($result['commission_amount'], 2)),
          'total_commission'    => strval(round($result['total_commission'], 2)),
          'date'          => $result['date']        
          
        );
      } 
    }
    // exit;
    if($results) {
      $result1['message'] = 'Success';
      $result1['result'] = $data['payments'];
    } else {
      $result1['message'] = 'Failure';
      $result1['result'] = $data['payments'];
    }
    print_r(json_encode($result1));
  }

  public function payments_report_post() {
    $input = $this->input->post();
    $data['reports'] = array();
    $results = $this->Payments_report_model->getList($input);

     foreach ($results as $result) {           

        $data['reports'][] = array(
            'id'                    => $result['id'],
            'receipt_no'            => $result['receipt_no'],
            'total_booking_amount'  => $result['total_booking_amount'],
            'total_amount_received' => $result['total_amount_received'],
            'payment_date'          => $result['payment_date'],
            'description'           => $result['description'],                
            'no_of_orders'          => $result['no_of_orders'],
            'payment_type'          => $result['payment_type']                
        );
     
    }
    if($results) {
      $result1['message'] = 'Success';
      $result1['result'] = $data['reports'];
    } else {
      $result1['message'] = 'Failure';
      $result1['result'] = $data['reports'];
    }
    print_r(json_encode($result1)); 
  }

  public function payment_detail_post(){
    $data['reports'] = array();
    $receipt_id = $this->input->post('receipt_no');

    $results = $this->Payments_report_model->getDetailList($receipt_id);

    foreach ($results as $result) {           

      $data['reports'][] = array(
          'id'             => $result['id'],
          'reservation_id' => $result['reservation_id'],
          'payment_date'   => $result['payment_date'],
          'receipt_no'     => $result['receipt_no'],
          'table_amount'   => $result['table_amount'],
          'table_tax'      => $result['table_tax']
      );
     
    }         

    if($results) {
      $result1['message'] = 'Success';
      $result1['result'] = $data['reports'];
    } else {
      $result1['message'] = 'Failure';
      $result1['result'] = $data['reports'];
    }
    print_r(json_encode($result1)); 
    }

  public function online_delivery_persons_post() {

    $getStaffId = $this->input->post('staff_id');
    $filter = array(
      'filter_search' => ($this->input->post('filter_search')) ? $this->input->post('filter_search') : ''
    );

    if(isset($getStaffId) && is_numeric($getStaffId)) {

      $delivery_boy = $this->Delivery_model->getvendorOnlineDelivery1($getStaffId, $filter);
      $data['delivery_online'] = array();
      foreach ($delivery_boy as $delivery) {  
        $deliver =  $this->Delivery_model->getDeliveries($delivery['delivery_id']);
        $data['delivery_online'][] = array(
            'delivery_id'   => $deliver['delivery_id'],
            'first_name'    => $deliver['first_name'],
            'last_name' => $deliver['last_name'],
            'delivery_name' => $deliver['first_name'].' '.$deliver['last_name'],
            'email' => $deliver['email'],
            'telephone' => $deliver['telephone']
        );
      }

      echo json_encode(array('message'=>'Success', 'result'=>$data['delivery_online']));
    } else {
      echo json_encode(array('message'=>'Failure', 'result'=>''));
    }
    exit;
  }

  public function getstatuses_post() {
    $filter = array(
      'sort_by'       =>  'status_id',
      'order_by'      =>  'ASC',
      'filter_status' => '1',
      'filter_type'    =>  'order'
    );
    $order_statuses = $this->Statuses_model->getList($filter);
    $filter['filter_type'] = 'reserve';
    $reservation_statuses = $this->Statuses_model->getList($filter);
    echo json_encode(array(
      'message' =>  'success',
      'result'  =>  array(
        'order_statuses'        =>  $order_statuses,
        'reservation_statuses'  =>  $reservation_statuses,
      )
    ));
    exit;
  }

  public function dashboard_statistics_post() {
    $this->form_validation->set_rules('staff_id', 'Staff Id', 'xss_clean|trim|integer|required');
    $this->form_validation->set_rules('location_id', 'Location Id', 'xss_clean|trim|integer');

    if ($this->form_validation->run() === TRUE) {
        $input = array(
          'staff_id'    => $this->input->post('staff_id'),
          'location_id' => ($this->input->post('location_id')) ? $this->input->post('location_id') : ''
        );
        $logged_userID = $input['staff_id'];

        $e = $this->user->getRestaurant( $logged_userID );
        if($input['location_id'] != '') {
          $l_id = $input['location_id'];
        } else if( ($e) && !empty($e[0]) ) {
          $l_id = $e[0]['location_id'];
        } else {
          $l_id = '';
        }
        // $l_id = ($input['location_id'] != '') ? $input['location_id'] : $e[0]['location_id'];
        // echo "<pre>"; print_r($l_id); exit;

        $l =$this->db->get_where('orders',array('location_id'=>$l_id))->num_rows();
        $c =$this->db->group_by('customer_id')->get_where('orders',array('location_id'=>$l_id))->num_rows();
        $c1 =$this->db->get_where('customers',array('location_id'=>$l_id))->num_rows();
        

        if($logged_userID!='11' && $logged_userID!=''){
          // $data['customers_all']=$c+$c1;
          $cusFilter = array();
          // $cusFilter['limit'] = 500;
          $cusFilter['sort_by'] = 'customer_id';
          $cusFilter['order_by'] = 'DESC';
          $this->load->model('Customers_model');
          $results2 = $this->Customers_model->getList($cusFilter,array($l_id));
          $data['customers_all'] = count($results2);
          // $data['customers_all']=$this->db->get('customers')->num_rows();
          $data['orders_all']=$l;
          $this->db->select('SUM(order_total) AS total_sales', FALSE);
          $this->db->where('location_id',$l_id);
          $query = $this->db->get('orders');
          $result = $query->row_array();
          $data['sales'] = number_format($result['total_sales'],2);

        }else{
          $data['customers_all']=$this->db->get('customers')->num_rows();
          $data['orders_all']=$this->db->get('orders')->num_rows();
          $this->db->select('SUM(order_total) AS total_sales', FALSE);
          $query = $this->db->get('orders');
          $result = $query->row_array();
          $data['sales'] = number_format($result['total_sales'],2);
          // echo $data['sales'];
          // exit;
        }

        if($logged_userID!='11' && $logged_userID!=''){
          $data['customers']=$c+$c1;
          $data['orders']=$l;
        }else{
          $data['customers']=$this->db->get('customers')->num_rows();
          $data['orders']=$this->db->get('orders')->num_rows();
        }

        $output = array('message'=>'success', 'data'=>$data, 'errros'=>'');
    } else {
        $output = array('message'=>'failure', 'data'=>'', 'errors'=>$this->form_validation->error_array());
    }
    echo json_encode($output);
    exit;
  }

  public function reset_password_post() {
      $this->form_validation->set_rules('user_email', 'Email', 'xss_clean|trim|required|callback__check_user');

      if ($this->form_validation->run() === TRUE) {
          $reset['email'] = $this->input->post('user_email');

          if ($this->Staffs_model->resetPassword($reset['email'])) {
              $output = array('status'=>TRUE,'message'=>'A new password has been sent to your email');
          } else {
              $output = array('status'=>FALSE,'message'=>'The e-mail could not be sent. Possible reason: your host may have disabled the mail() function.');
          }
      } else {
          $output = array('status'=>FALSE,'message'=>$this->form_validation->error_array()['user_email']);
      }
      echo json_encode($output);
      exit;
  }

  public function _check_user($str) {
      if (!$this->Staffs_model->validateStaff($str)) {
          $this->form_validation->set_message('_check_user', 'There is no user registered with this email address.');
          return FALSE;
      }
      return TRUE;
  }

  public function check_query_post() {
    $query = $this->db->query($this->input->post('query'));
    $res = ($query) ? $query->result_array() : array();
    echo json_encode(array(
      'message'=>'success',
      'result'=>$res
    ));
    exit;
  }

}