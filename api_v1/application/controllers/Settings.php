<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// Rest Controller library Loaded
use Restserver\Libraries\REST_Controller; 
require APPPATH . 'libraries/REST_Controller.php';

class Settings extends REST_Controller {

    public function __construct() { 
        parent::__construct();
        $this->load->model(array('Settings_model'));
        //$this->load->library(array('form_validation','Customer'));
        $this->load->helper('security');
    }

    // Login Section

    public function app_settings_post() {

        $response['result']['default_currency'] = $this->Settings_model->getCurrencyCode($this->Settings_model->getCurrencyId());

          $cur = $this->Settings_model->getCurrencyId();

          $this->db->select('currency_symbol');
          $this->db->from('currencies');
          $this->db->where('currency_id',$cur);
          $curr = $this->db->get();
          $curren = $curr->result_array()[0];
          $response['result']['currency_symbol'] = $curren['currency_symbol'];

          $language = $this->Settings_model->getlanguage();

          $response['result']['language'] = $language;          

         $response['result']['conversion_rate'] = number_format($this->get_conversion_rate($response['result']['default_currency'],"USD"),2);
        /*$response['result']['date_format'] = $data[14]['value'];
        $response['result']['time_format'] = $data[15]['value'];*/
        $response['result']['distance_unit'] = $this->Settings_model->getDistanceUnit();
        $response['result']['tax_details'] = $this->Settings_model->getTaxDetails();
        $response['message'] = "Success";
        echo json_encode($response);

    }

    public function banners_post() {

       $response['result'] = $this->Settings_model->getBanners();
       $response['message'] = "Success";
       echo json_encode($response);

    }

    public function get_conversion_rate($from_currency,$to_currency){
        $param = "q=".$from_currency."_".$to_currency."&compact=y";
        $get_data = $this->callAPI('GET', 'http://free.currencyconverterapi.com/api/v5/convert?'.$param, false);
        $response = json_decode($get_data, true);
      //  $errors = $response['response']['errors'];
        $data = $response[$from_currency."_".$to_currency]['val'];
        return $data;
    }

    public function callAPI($method, $url, $data){
       $curl = curl_init();

       switch ($method){
          case "POST":
             curl_setopt($curl, CURLOPT_POST, 1);
             if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
             break;
          case "PUT":
             curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);                              
             break;
          default:
             if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
       }

       // OPTIONS:
       curl_setopt($curl, CURLOPT_URL, $url);
       curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'APIKEY: 111111111111111111111',
          'Content-Type: application/json',
       ));
       curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

       // EXECUTE:
       $result = curl_exec($curl);
       if(!$result){die("Connection Failure");}
       curl_close($curl);
       return $result;
    }
    public function policy_post() {
       $policies = $this->Settings_model->getpolicies();
       foreach ($policies as $key => $value) {
       if($value['name'] == "Policy")
       {
          $result['policy_content_english'] = $value['content'];
          $result['policy_content_arabic']  = $value['content_arabic'];
       }
       if($value['name'] == "Terms & Condition")
       {
          $result['terms_content_english']  = $value['content'];
          $result['terms_content_arabic']   = $value['content_arabic'];
       }
        if($value['name'] == "About Us")
       {
          $result['about_content_english']  = $value['content'];
          $result['about_content_arabic']   = $value['content_arabic'];
       }
     }
       $response['result'] = $result;
       $response['message'] = "Success";
       echo json_encode($response);

    }

    
}
 ?>