<?php
/**
 * SpotnEat
 *
 * An open source online ordering, reservation and management system for restaurants.
 *
 * @package   SpotnEat
 * @author    SamPoyigi
 * @copyright SpotnEat
 * @link      http://spotneat.com
 * @license   http://opensource.org/licenses/GPL-3.0 The GNU GENERAL PUBLIC LICENSE
 * @since     File available since Release 1.0
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Locations Model Class
 *
 * @category       Models
 * @package        SpotnEat\Models\Locations_model.php
 * @link           http://docs.spotneat.com
 */
class Restaurant_model extends CI_Model {

	public function login($user, $password, $override_login = FALSE) {

		$this->db->from('users');
		$this->db->join('staffs', 'staffs.staff_id = users.staff_id', 'left');

		$this->db->where('username', strtolower($user));
		$this->db->where('password', 'SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1("' . $password . '")))))', FALSE);
		$this->db->where('staff_status', '1');

		$query = $this->db->get();

		//Login Successful
		if ($query->num_rows() === 1) {
			$row = $query->row_array();

			if($row['staff_id'] != '11') {
				$user_data = array(
					'username'     		=> $row['username'],
					'staff_id'			=> $row['staff_id'],
					'staff_name'		=> $row['staff_name'],
					'email'     		=> $row['staff_email'],
					'staff_group_id'    => $row['staff_group_id'],
					'user_id'  			=> $row['user_id'],
				);

				return $user_data;
			} else {
				return FALSE;
			}
		} else {
      		return FALSE;
		}
	}
	public function insertDeviceId($email,$deviceid) {
			$this->db->set('deviceid', $deviceid);
			$this->db->where('locations_email', $email);
			$query = $this->db->update('locations');
	}
  	/*public function logout() {

  		
		$this->CI->session->unset_userdata('cust_info');

		$this->delivery_id = '0';
		$this->firstname = '';
		$this->lastname = '';
		$this->email = '';
		$this->telephone = '';
		$this->address_id = '';
		$this->security_question_id = '';
		$this->security_answer = '';
		$this->delivery_group_id = '';

    }*/


}

/* End of file locations_model.php */
/* Location: ./system/spotneat/models/locations_model.php */