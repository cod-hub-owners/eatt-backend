<?php
/**
 * SpotnEat
 *
 * An open source online ordering, reservation and management system for restaurants.
 *
 * @package   SpotnEat
 * @author    SamPoyigi
 * @copyright SpotnEat
 * @link      http://spotneat.com
 * @license   http://opensource.org/licenses/GPL-3.0 The GNU GENERAL PUBLIC LICENSE
 * @since     File available since Release 1.0
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Languages Model Class
 *
 * @category       Models
 * @package        SpotnEat\Models\Languages_model.php
 * @link           http://docs.spotneat.com
 */
class Types_model extends CI_Model {

	public function getCount($filter = array()) {
		if ( ! empty($filter['filter_search'])) {
			$this->db->like('type_name', $filter['filter_search']);
			
		}

		if (isset($filter['filter_status']) AND is_numeric($filter['filter_status'])) {
			$this->db->where('status', $filter['filter_status']);
		}

		$this->db->from('store_types');

		return $this->db->count_all_results();
	}

	public function getList($filter = array()) {
		if ( ! empty($filter['page']) AND $filter['page'] !== 0) {
			$filter['page'] = ($filter['page'] - 1) * $filter['limit'];
		}

		if ($this->db->limit($filter['limit'], $filter['page'])) {
			$this->db->from('store_types');

			if ( ! empty($filter['sort_by']) AND ! empty($filter['order_by'])) {
				$this->db->order_by($filter['sort_by'], $filter['order_by']);
			}

			if ( ! empty($filter['filter_search'])) {
				$this->db->like('type_name', $filter['filter_search']);
			}

			if (isset($filter['filter_status']) AND is_numeric($filter['filter_status'])) {
				$this->db->where('status', $filter['filter_status']);
			}

			$query = $this->db->get();

			$result = array();

			if ($query->num_rows() > 0) {
				$result = $query->result_array();
			}

			return $result;
		}
	}

	public function getTypes() {
		$this->db->from('store_types');

		$this->db->where('status', '1');

		$query = $this->db->get();

		$result = array();

		if ($query->num_rows() > 0) {
			$result = $query->result_array();
		}

		return $result;
	}

	public function getType($type_id) {
		if ($type_id !== '') {
			$this->db->from('store_types');

			$this->db->where('id', $type_id);

			$query = $this->db->get();

			if ($query->num_rows() > 0) {
				return $query->row_array();
			}
		}
	}

	public function getFoodTypes() {
		$this->db->from('food_types');

		$this->db->where('status', '1');

		$query = $this->db->get();

		$result = array();

		if ($query->num_rows() > 0) {
			$result = $query->result_array();
		}

		return $result;
	}

	public function getTables($loc_id= '') {

			$this->db->from('tables');
			if($loc_id != ''){
			$this->db->where('table_status >', '0');
			$this->db->where('location_id', $loc_id);
		    }

			$query = $this->db->get();
			$result = array();

			if ($query->num_rows() > 0) {
				$result = $query->result_array();
			}

			return $result;
	}


		public function getAllTables($filter = array(),$location_id = '',$staff_id='') {
		     
		    if ( ! empty($filter['page']) AND $filter['page'] !== 0) {
			$filter['page'] = ($filter['page'] - 1) * $filter['limit'];
		     }
		    if ($this->db->limit($filter['limit'], $filter['page'])) {
			$this->db->from('tables');

			if ( ! empty($filter['filter_search'])) {
				$this->db->like('table_name', $filter['filter_search']);
			}
			$this->db->where('added_by',$staff_id);
			$this->db->where('location_id', $location_id);
			

			$query = $this->db->get();
			$result = array();

			if ($query->num_rows() > 0) {
				$result = $query->result_array();
			}

			return $result;
		}
		
	}

	public function getTablesByLocation($location_id = FALSE) {
		$this->db->from('location_tables');

		$this->db->where('location_id', $location_id);

		$query = $this->db->get();

		$location_tables = array();

		if ($query->num_rows() > 0) {

			foreach ($query->result_array() as $row) {
				$location_tables[] = $row['table_id'];
			}
		}

		return $location_tables;
	}

	public function findReservations($location_id,$table_id) {
		$this->db->from('reservations');
		$this->db->where('location_id', $location_id);
		$this->db->where('table_id', $table_id);
		$this->db->where('(status = 21 or status = 22)');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function deleteTable($location_id,$table_id) {
		$this->db->where('location_id', $location_id);
		$this->db->where('table_id', $table_id);
   		$this->db->delete('location_tables'); 
		return TRUE;
	}




	public function saveType($type_id, $save = array()) {
		if (empty($save)) return FALSE;

		if (isset($save['type_name'])) {
			$this->db->set('type_name', $save['type_name']);
		}

		if (isset($save['type_icon'])) {
			$this->db->set('type_icon', $save['type_icon']);
		}

		if (isset($save['status']) AND $save['status'] === '1') {
			$this->db->set('status', '1');
		} else {
			$this->db->set('status', '0');
		}

		if (is_numeric($type_id)) {
			$this->db->where('id', $type_id);
			$query = $this->db->update('store_types');
		} else {
			$query = $this->db->insert('store_types');
			$type_id = $this->db->insert_id();
		}

		return ($query === TRUE AND is_numeric($type_id)) ? $type_id : FALSE;
	}
	
	public function saveTable($table_id, $save = array()) {
		if (empty($save)) return FALSE;

		if (isset($save['table_name'])) {
			$this->db->set('table_name', $save['table_name']);
		}

		if (isset($save['min_capacity'])) {
			$this->db->set('min_capacity', $save['min_capacity']);
		}

		if (isset($save['max_capacity'])) {
			$this->db->set('max_capacity', $save['max_capacity']);
		}

		if (isset($save['additional_charge'])) {
			$this->db->set('additional_charge', $save['additional_charge']);
		}

		if (isset($save['total_price'])) {
			$this->db->set('total_price', $save['total_price']);
		}

		if (isset($save['location_id'])) {
			$this->db->set('location_id', $save['location_id']);
		}

		if ($save['added_by'] != "") {
			$this->db->set('added_by', $save['added_by']);
		}

		if (isset($save['table_status']) AND $save['table_status'] === '1') {
			$this->db->set('table_status', $save['table_status']);
		} else {
			$this->db->set('table_status', '0');
		}

		if (is_numeric($table_id)) {
			$this->db->where('table_id', $table_id);
			$query = $this->db->update('tables');
		} else {
			$query = $this->db->insert('tables');
			$table_id = $this->db->insert_id();
		}
		//echo $this->db->last_query();exit;
		return ($query === TRUE AND is_numeric($table_id)) ? $table_id : FALSE;
	}

		public function tableDelete($table_id) {
		if (is_numeric($table_id)) {
			$this->db->where('table_id', $table_id);
			$this->db->delete('tables');

			return $this->db->affected_rows();
		}
	}

	/*public function deleteLanguage($type_id) {
		if (is_numeric($type_id)) $type_id = array($type_id);

		if ( ! empty($type_id) AND ctype_digit(implode('', $type_id))) {
			$this->db->from('store_types');
			$this->db->where('can_delete', '0');
			$this->db->where_in('type_id', $type_id);
			$this->db->where('type_id !=', '11');
			$query = $this->db->get();

			if ($query->num_rows() > 0) {
				foreach ($query->result_array() as $row) {
					delete_language($row['idiom']);
				}
			}

			$this->db->where('can_delete', '0');
			$this->db->where_in('type_id', $type_id);
			$this->db->where('type_id !=', '11');
			$this->db->delete('languages');

			return $this->db->affected_rows();
		}
	}

	public function getCurrentLanguage() {
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('item', 'admin_type_id');
		$query = $this->db->get();

		return $query->row_array()['value'];
	}*/
}

/* End of file languages_model.php */
/* Location: ./system/spotneat/models/languages_model.php */