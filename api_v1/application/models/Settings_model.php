<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Settings_model extends CI_Model {


	public function __construct() {
        parent::__construct();
        
        //load database library
        $this->load->database();
    }

	public function getAll() {
		$this->db->from('settings');

		$query = $this->db->get();
		$result = array();

		if ($query->num_rows() > 0) {
			$result = $query->result_array();
		}

		return $result;
	}

	public function getCurrencyId() {
		
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('item','currency_id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array()[0]['value'];
		}else{
			return "";
		}
		
	
	}

	public function getDistanceUnit() {
		
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('item','distance_unit');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array()[0]['value'];
		}else{
			return "";
		}
		
	
	}

	public function updateSettings($sort, $update = array(), $flush = FALSE) {
		if ( ! empty($update) && ! empty($sort)) {
			if ($flush === TRUE) {
				$this->db->where('sort', $sort);
				$this->db->delete('settings');
			}

			foreach ($update as $item => $value) {
				if ( ! empty($item)) {
					if ($flush === FALSE) {
						$this->db->where('sort', $sort);
						$this->db->where('item', $item);
						$this->db->delete('settings');
					}

					if (isset($value)) {
						$serialized = '0';
						if (is_array($value)) {
							$value = serialize($value);
							$serialized = '1';
						}

						$this->db->set('sort', $sort);
						$this->db->set('item', $item);
						$this->db->set('value', $value);
						$this->db->set('serialized', $serialized);
						$this->db->insert('settings');
					}
				}
			}

			return TRUE;
		}
	}

	public function addSetting($sort, $item, $value, $serialized = '0') {
		$query = FALSE;

		if (isset($sort, $item, $value, $serialized)) {
			$this->db->where('sort', $sort);
			$this->db->where('item', $item);
			$this->db->delete('settings');

			$this->db->set('sort', $sort);
			$this->db->set('item', $item);

			if (is_array($value)) {
				$this->db->set('value', serialize($value));
			} else {
				$this->db->set('value', $value);
			}

			$this->db->set('serialized', $serialized);
			if ($this->db->insert('settings')) {
				$query = $this->db->insert_id();
			}
		}

		return $query;
	}

	public function deleteSettings($sort, $item) {
		if ( ! empty($sort) AND ! empty($item)) {
			$this->db->where('sort', $sort);
			$this->db->where('item', $item);
			$this->db->delete('settings');

			if ($this->db->affected_rows() > 0) {
				return TRUE;
			}
		}
	}

	public function getCurrencyCode($id) {
		$this->db->select('currency_code');
		$this->db->from('currencies');
		$this->db->where('currency_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array()[0]['currency_code'];
		}else{
			return "";
		}
		
	}

	public function getBanners() {
		$this->db->select('image_code');
		$this->db->from('banners');
		$this->db->where('banner_id', 1);
		$query = $this->db->get();
		$res = $query->row_array();
		$results = unserialize($res['image_code']);
		$count = count($results['paths']);
		$i = 0;
		for($i=0;$i<$count;$i++){
			$result[$i]['name'] =  '';
			$result[$i]['image_src'] =  $results['paths'][$i];
			$result[$i]['caption'] =  '';			
		}
		return $result;
		exit;
	}

	public function getTaxDetails() {
		$tax_details = array();

		$tax_details['tax_mode'] = $this->getTaxMode();
		if($tax_details['tax_mode'] == "Active"){
			$tax_details['tax_percentage'] = $this->getTaxPercentage();
			$tax_details['tax_percentage'] = $this->getTaxPercentage();
			$tax_details['tax_menu_price'] = $this->getTaxMenuPrice();
			$tax_details['tax_delivery_charge'] = $this->getTaxDeliveryCharge();
			
		}
		return $tax_details;
	}

	public function getTaxMode() {
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('item','tax_mode');
		$query = $this->db->get();
		$result = array();

		if ($query->num_rows() > 0) {
			$result = $query->result_array()[0]['value'];
		}

		if($result == 1){
			return "Active";
		}else{
			return "In-Active";
		}
	}

	public function getTaxPercentage() {
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('item','tax_percentage');
		$query = $this->db->get();
		$result = array();

		if ($query->num_rows() > 0) {
			$result = $query->result_array()[0]['value'];
		}
		
		return $result;
	}

	public function getTaxMenuPrice() {
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('item','tax_menu_price');
		$query = $this->db->get();
		$result = array();

		if ($query->num_rows() > 0) {
			$result = $query->result_array()[0]['value'];
		}

		if($result == 1){
			return "Apply tax on top of my menu price";
		}else{
			return "Menu price already include tax";
		}
	}

	public function getTaxDeliveryCharge() {
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('item','tax_delivery_charge');
		$query = $this->db->get();
		$result = array();

		if ($query->num_rows() > 0) {
			$result = $query->result_array()[0]['value'];
		}

		if($result == 1){
			return "YES";
		}else{
			return "NO";
		}
	}

	public function getlanguage() {
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('item','language_id');
		$query = $this->db->get();
		$result = array();

		if ($query->num_rows() > 0) {
			$result = $query->result_array()[0]['value'];
		}
		
		return $result;
	}


	public function getpolicies() {
		$this->db->select('*');
		$this->db->from('pages');
		$this->db->where('status',1);
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
		exit;
	}

	public function getMapKey() {
		
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('item','maps_api_key');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array()[0]['value'];
		}else{
			return "";
		}
	}

	public function getnexmokey() {
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('item','nexmo_key');
		$query = $this->db->get();
		$result = array();

		if ($query->num_rows() > 0) {
			$result = $query->result_array()[0]['value'];
		}
		
		return $result;
	}
	public function getnexmosecret() {
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('item','nexmo_secret');
		$query = $this->db->get();
		$result = array();

		if ($query->num_rows() > 0) {
			$result = $query->result_array()[0]['value'];
		}
		
		return $result;
	}
}

/* End of file settings_model.php */
