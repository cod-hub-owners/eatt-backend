<?php
/**
 * SpotnEat
 *
 * 
 *
 * @package   SpotnEat
 * @author    Sp
 * @copyright SpotnEat
 * @link      http://spotneat.com
 * @license   http://spotneat.com
 * @since     File available since Release 1.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Notification Class
 *
 * @category       Libraries
 * @package        SpotnEat\Libraries\Notification.php
 * @link           http://docs.spotneat.com
 */
class Notification {
}

// END Notification class

/* End of file Notification.php */
/* Location: ./system/spotneat/libraries/Notification.php */