<?php
/**
 * SpotnEat
 *
 * 
 *
 * @package   SpotnEat
 * @author    Sp
 * @copyright SpotnEat
 * @link      http://spotneat.com
 * @license   http://spotneat.com
 * @since     File available since Release 1.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['spotneat_system_name']       = 'EatT';
$lang['spotneat_system_powered']    = '';
$lang['spotneat_copyright']         = '';
$lang['spotneat_version']           = 'versión %s';

/* End of file spotneat_lang.php */
/* Location: ./system/spotneat/language/english/spotneat_lang.php */