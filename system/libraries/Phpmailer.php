<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Phpmailer
{
    public function load()
    {
        $objMail = new PHPMailer\PHPMailer\PHPMailer();
        return $objMail;
    }
}