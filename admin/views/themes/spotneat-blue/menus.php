<?php echo get_header(); ?>
<style>
@media (max-width: 500px) {
    .stock_qty_val  {
        width: 55% !important;
    }
}
</style>
    <div class="row content">
        <div class="col-md-12">
            <div class="alert alert-success alert-dismissable alert-collapsible" id="success_msg" style="display:none" role="alert">
                
                <button type="button" class="close top-35" data-dismiss="alert" aria-hidden="true">×</button>
                <ul class="list-group-alert">
                    Stock Quantity has been updated successfully
                    
                </ul>
            </div>
            <div class="alert alert-danger alert-dismissable alert-collapsible" id="error_msg" style="display:none" role="alert">
                
                <button type="button" class="close top-35" data-dismiss="alert" aria-hidden="true">×</button>
                <ul class="list-group-alert">
                    Stock Quantity has not been updated
                    
                </ul>
            </div>
            <div class="panel panel-default panel-table">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo lang('text_list'); ?></h3>
                    <div class="pull-center">
                          
                    </div>
                    <div class="pull-right">
                    
                        <button class="btn btn-filter btn-xs"><i class="fa fa-filter"></i></button>
                    </div>
                </div>
                <div class="panel-body panel-filter">
                    <form role="form" id="filter-form" accept-charset="utf-8" method="GET" action="<?php echo current_url(); ?>">
                        <div class="filter-bar">
                            <div class="form-inline">
                                <div class="row">

                                        <?php if($vendor_id != ""){ ?>
                                            <input type="hidden" name="id" value="<?php echo $vendor_id; ?>" />
                                        <?php } ?>

                                    <div class="col-md-3 pull-right text-right">
                                        <div class="form-group">
                                            <input type="text" name="filter_search" class="form-control input-sm" value="<?php echo set_value('filter_search', $filter_search); ?>" placeholder="<?php echo lang('text_filter_search'); ?>"/>&nbsp;&nbsp;&nbsp;
                                        </div>
                                        <a class="btn btn-grey" onclick="filterList();" title="<?php echo lang('text_search'); ?>"><i class="fa fa-search"></i></a>
                                    </div>
                                    <div class="col-md-8 pull-left">
                                        <div class="form-group">
                                            <select name="filter_category" class="form-control input-sm">
                                                <option value=""><?php echo lang('text_filter_category'); ?></option>
                                                <?php foreach ($categories as $category) { ?>
                                                    <?php if ($category['category_id'] == $category_id) { ?>
                                                        <option value="<?php echo $category['category_id']; ?>" <?php echo set_select('filter_category', $category['category_id'], TRUE); ?> ><?php echo $category['category_name']; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $category['category_id']; ?>" <?php echo set_select('filter_category', $category['category_id']); ?> ><?php echo $category['category_name']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>&nbsp;
                                        </div>
                                        <div class="form-group">
                                            <select name="filter_status" class="form-control input-sm">
                                                <option value=""><?php echo lang('text_filter_status'); ?></option>
                                                <?php if ($filter_status === '1') { ?>
                                                    <option value="1" <?php echo set_select('filter_status', '1', TRUE); ?> ><?php echo lang('text_enabled'); ?></option>
                                                    <option value="0" <?php echo set_select('filter_status', '0'); ?> ><?php echo lang('text_disabled'); ?></option>
                                                <?php } else if ($filter_status === '0') { ?>
                                                    <option value="1" <?php echo set_select('filter_status', '1'); ?> ><?php echo lang('text_enabled'); ?></option>
                                                    <option value="0" <?php echo set_select('filter_status', '0', TRUE); ?> ><?php echo lang('text_disabled'); ?></option>
                                                <?php } else { ?>
                                                    <option value="1" <?php echo set_select('filter_status', '1'); ?> ><?php echo lang('text_enabled'); ?></option>
                                                    <option value="0" <?php echo set_select('filter_status', '0'); ?> ><?php echo lang('text_disabled'); ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <a class="btn btn-grey" onclick="filterList();" title="<?php echo lang('text_filter'); ?>"><i class="fa fa-filter"></i></a>&nbsp;
                                        <a class="btn btn-grey" href="<?php echo page_url(); ?>" title="<?php echo lang('text_clear'); ?>"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <form role="form" id="list-form" accept-charset="utf-8" method="POST" action="<?php echo current_url(); ?>">
                    <div class="table-responsive">
                        <table class="table table-striped table-border">
                            <thead>
                            <tr>
                                <th class="action">
                                    <input type="checkbox" onclick="$('input[name*=\'delete\']').prop('checked', this.checked);">
                                </th>
                                <!--<th>Photo</th>-->
                                <th class="name">
                                    <a class="sort" href="<?php echo $sort_name; ?>"><?php echo lang('column_name'); ?>
                                        <i class="fa fa-sort-<?php echo ($sort_by == 'menu_name') ? $order_by_active : $order_by; ?>"></i></a>
                                </th>
                                <th>
                                    <a class="sort" href="<?php echo $sort_price; ?>"><?php echo lang('column_price'); ?>
                                        <i class="fa fa-sort-<?php echo ($sort_by == 'menu_price') ? $order_by_active : $order_by; ?>"></i></a>
                                </th>
                                <th><?php echo lang('column_category'); ?></th>
                                <th>
                                    <a class="sort" href="<?php echo $sort_stock; ?>"><?php echo lang('column_stock_qty'); ?>
                                        <i class="fa fa-sort-<?php echo ($sort_by == 'stock_qty') ? $order_by_active : $order_by; ?>"></i></a>
                                </th>
                                <th>
                                                     
                                     <?php echo lang('column_stock_qty_edit'); ?>
                                     <a href="javascript:void(0)" class="btn btn-info bulk_change_quantity" style="margin-left:2% !important"  data-url='<?php echo base_url();?>menus/qty/bulk'  >
                                        <i class="fa fa-check"></i> Update All
                                    </a>
                                     
                                </th>
                                <th class="text-center"><?php echo lang('column_status'); ?></th>
                                <th class="id">
                                    <a class="sort" href="<?php echo $sort_id; ?>"><?php echo lang('column_id'); ?>
                                        <i class="fa fa-sort-<?php echo ($sort_by == 'menus.menu_id') ? $order_by_active : $order_by; ?>"></i></a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($menus) { ?>
                                <?php foreach ($menus as $menu) { ?>
                                    <tr id="<?php echo $menu['menu_id']; ?>">
                                        <td class="action">
                                            <input type="checkbox" value="<?php echo $menu['menu_id']; ?>" name="delete[]"/>&nbsp;&nbsp;&nbsp;
                                            <a class="btn btn-edit" title="<?php echo lang('text_edit'); ?>" href="<?php echo $menu['edit']; ?>"><i class="fa fa-pencil"></i></a>
                                        </td>
                                        <!--<td class="left"><img src="<?php echo $menu['menu_photo']; ?>"></td>-->
                                        <td class="name"><?php echo $menu['menu_name']; ?></td>
                                        <td class="left"><?php echo $menu['menu_price']; ?>&nbsp;&nbsp;
                                            <?php if ($menu['special_status'] === '1' AND $menu['is_special'] === '1') { ?>
                                                <a title="<?php echo lang('text_special_enabled'); ?>"><i class="fa fa-star fa-star-special"></i></a>
                                            <?php } else if ($menu['special_status'] === '1' AND $menu['is_special'] !== '1') { ?>
                                                <a title="<?php echo lang('text_special_expired'); ?>"><i class="fa fa-star fa-star-special disabled"></i></a>
                                            <?php } else if ($menu['is_special'] === '1') { ?>
                                                <a title="<?php echo lang('text_special_disabled'); ?>"><i class="fa fa-star fa-star-special disabled"></i></a>
                                            <?php } ?>
                                        </td>
                                        <td class="left"><?php echo $menu['category_name']; ?></td>
                                        <td class="left stock_qty"><?php echo ($menu['stock_qty'] < 1) ? '<span class="red">' . $menu['stock_qty'] . '</span>' : $menu['stock_qty']; ?></td>
                                        <td class="left edit_stock_qty">
                                            <input class="stock_qty_val form-control"  style="width:38%;float:left" type="number" value="<?php echo $menu['stock_qty'];?>" data-key="<?php echo $menu['menu_id'];?>" min="0">
                                           
                                            <button class="btn btn-edit change_quantity" style="margin-top:1%;float:left;margin-left: 4%;" data-url='<?php echo base_url();?>menus/qty/view/<?php echo $menu['menu_id'];?>'  type="button" >
                                                <i class="fa fa-check"></i>
                                            </button>
                                        </td>
                                        <td class="text-center"><?php echo $menu['menu_status']; ?></td>
                                        <td class="id"><?php echo $menu['menu_id']; ?></td>
                                    </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="7" class="center"><?php echo lang('text_empty'); ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>

                <div class="pagination-bar clearfix">
                    <div class="links"><?php echo $pagination['links']; ?></div>
                    <div class="info"><?php echo $pagination['info']; ?></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"><!--
        function filterList() {
            $('#filter-form').submit();
        }
        //--></script>
       <script type="text/javascript">
            $(document).ready(function() {
                $(document).on('click','.change_quantity',function(){
                    
                    url = $(this).attr('data-url');
                    val = $(this).parent().find('.stock_qty_val').val();
                    thisss = $(this);
                    view.showLoader();
                    $.ajax({
                        url:url,
                        dataType:"json",
                        type:"post",
                        data:{val:val,action:'cahnge_qty'},
                        success:function(data){
                            console.log(data);
                            view.hideLoader();
                            if(data!==""){
                                thisss.parent().parent().find('.stock_qty').html(data);
                                $('#success_msg').show();
                                $('#success_msg').delay(1000).hide(1000); 
                            }else{
                                $('#error_msg').show();
                                $('#error_msg').delay(1000).hide(1000); 
                            }
                        }
                    });
                })

                $(document).on('click','.bulk_change_quantity',function(){
                    var val=[];
                    $.each($('table > tbody > tr'),function(i,v){
                        if($(v).find('.stock_qty_val').val() !==''){
                            val.push({
                                
                                val:$(v).find('.stock_qty_val').val(),
                                id:$(v).find('.stock_qty_val').attr('data-key')
                            });  
                        }else{
                            val.push({
                                
                                val:0,
                                id:$(v).find('.stock_qty_val').attr('data-key')
                            });
                        }
                       
                    });
                    console.log(val);
                   
                    url = $(this).attr('data-url');
                   
                    thisss = $(this);
                    view.showLoader();
                    $.ajax({
                        url:url,
                        dataType:"json",
                        type:"post",
                        data:{'data':val},
                        success:function(data){
                            console.log(data);
                            view.hideLoader();
                            if(data!==""){
                                thisss.parent().parent().find('.stock_qty').html(data);
                                
                                $('#success_msg').show();
                                $.each($('table > tbody > tr'),function(i,v){
                                    if($(v).find('.stock_qty_val').val() !==''){
                                        $(v).find('.stock_qty').html($(v).find('.stock_qty_val').val());
                                    }else{
                                        $(v).find('.stock_qty').html('<span class="red">0</span>');
                                        $(v).find('.stock_qty_val').val(0);
                                    }
                                    
                                    
                                              
                                });
                                $('#success_msg').delay(1000).hide(1000); 
                            }else{
                                $('#error_msg').show();
                                $('#error_msg').delay(1000).hide(1000); 
                            }
                        }
                    });
                })
            });
        var view={};    	
        view.hideLoader=function(){
            
                $(".loader").hide();
        }
        view.showLoader=function(){
            
                $(".loader").show();
        }
        </script> 
<?php echo get_footer(); ?>