<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Delivery Fee';
$lang['text_heading'] 		            = 'Delivery Fee';
$lang['text_delivery_fee'] 		        = 'Delivery Fee';
$lang['label_standard_fee'] 		    = 'Standard Delivery Fee';
$lang['label_premium_fee'] 		       	= 'Premium Delivery Fee';
$lang['standard_fee'] 		   			= 'Standard Delivery Fee';
$lang['premium_fee'] 		       		= 'Premium Delivery Fee';

$lang['label_first_name'] 		        = 'First Name';
$lang['label_user_name'] 		        = 'User Name';