<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Faq';
$lang['text_heading'] 		            = 'Faq';
$lang['text_list'] 		                = 'Faq List';
$lang['text_empty'] 		            = 'There are no security questions, please add.';

$lang['column_question_english'] 		= 'English';
$lang['column_question_arabic'] 		= 'Spanish';
$lang['column_id'] 		                = 'ID';
$lang['column_language'] 		        = 'Language';
$lang['text_english'] 		        	= 'English';
$lang['text_arabic'] 		        	= 'Spanish';
$lang['label_question'] 		        = 'Question Id';
$lang['label_answer'] 		            = 'Question Answer';

/* End of file security_questions_lang.php */
/* Location: ./admin/language/english/security_questions_lang.php */