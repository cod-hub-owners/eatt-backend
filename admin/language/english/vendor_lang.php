<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Clients';
$lang['text_heading'] 		            = '%s';
$lang['text_edit_heading'] 		        = 'Category: %s';
$lang['text_list'] 		                = 'Users List';
// $lang['completed_history'] 		        = 'Completed History';
// $lang['pending_history'] 		        = 'Pending Requests';
$lang['text_tab_general'] 		        = 'Category Details';
$lang['text_tab_payout'] 		        = 'Payout Details';
$lang['text_filter_search'] 		    = 'Search category name, description or status.';
$lang['text_filter_status'] 		    = 'View all status';
$lang['text_empty'] 		            = 'There are no categories available.';
$lang['text_payout'] 		            = 'There are no payouts available.';

$lang['column_name'] 		            = 'Name';
$lang['column_description'] 		    = 'Description';
$lang['column_parent'] 		            = 'Parent';
$lang['column_priority'] 		        = 'Priority';
$lang['column_status'] 		            = 'Status';
$lang['column_id'] 		                = 'ID';

$lang['label_name'] 		            = 'Name';
$lang['label_description'] 		        = 'Description';
$lang['label_permalink_id'] 		    = 'Permalink ID';
$lang['label_permalink_slug'] 		    = 'Permalink Slug';
$lang['label_parent'] 		            = 'Parent';
$lang['label_image'] 		            = 'Image';
$lang['label_priority'] 		        = 'Priority';
$lang['label_status'] 		            = 'Status';
$lang['label_amount'] 		            = 'Amount';

$lang['help_photo'] 		            = 'Select a file to update category image, otherwise leave blank.';
$lang['help_permalink'] 		        = 'Use ONLY alpha-numeric lowercase characters, underscores or dashes and make sure it is unique GLOBALLY.';

$lang['client_name'] 		            = 'User Name';
$lang['client_email'] 		            = 'User Email';
$lang['view_categories'] 		        = 'View Categories';
$lang['pay_to_bank'] 		        	= 'Pay To Bank';
$lang['actions'] 		        		= 'Action';
$lang['view_menus'] 		            = 'View Menus';
$lang['view_tables'] 		            = 'View Tables';
$lang['view_reservations'] 		        = 'View Reservations';
$lang['view_orders'] 		            = 'View Orders';
$lang['view_reviews'] 		            = 'View Reviews';
$lang['rest/cafe'] 		         	    = 'Restaurant Name';
$lang['credit'] 		         	    = 'Credit';
$lang['debit'] 		         	    	= 'Debit';
$lang['wallet_amount'] 	  	    		= 'Wallet Amount';
$lang['pending_amount'] 		        = 'Request Amount';
$lang['completed_amount'] 		        = 'Amount';
$lang['invoice_no'] 		        	= 'Invoice No';




/* End of file categories_lang.php */
/* Location: ./admin/language/english/categories_lang.php */