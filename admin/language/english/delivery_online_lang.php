<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Delivery Online';
$lang['text_heading'] 		            = 'Delivery Online';
$lang['text_all_heading'] 		        = 'delivery Online: All';
$lang['text_online'] 		            = 'Online';
$lang['text_all'] 		                = 'All';
$lang['text_filter_search'] 		    = 'Search ip, delivery or browser.';
$lang['text_filter_access'] 		    = 'View all access';
$lang['text_filter_date'] 		        = 'View all dates';
$lang['text_empty'] 		            = 'There are no delivery online.';
$lang['text_empty_report'] 		        = 'There are no delivery online report available.';
$lang['text_guest'] 		            = 'Guest';
$lang['text_private'] 		            = 'Private';
$lang['text_browser'] 		            = 'Browser';
$lang['text_mobile'] 		            = 'Mobile';
$lang['text_robot'] 		            = 'Robot';

$lang['column_name'] 		            = 'Name';
$lang['email']							= 'Email';
$lang['phone']							= 'Phone';
$lang['column_id'] 		                = 'ID';
$lang['column_ip'] 		                = 'IP';
$lang['column_delivery'] 		        = 'Delivery';
$lang['column_access'] 		            = 'Access';
$lang['column_browser'] 		        = 'Browser';
$lang['column_agent'] 		            = 'User Agent';
$lang['column_request_uri'] 		    = 'Last Request URL';
$lang['column_referrer_url'] 		    = 'Last Referrer URL';
$lang['column_last_activity'] 		    = 'Last Activity';

/* End of file delivery_online_lang.php */
/* Location: ./admin/language/english/delivery_online_lang.php */