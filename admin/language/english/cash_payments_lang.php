<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Cash Payments';
$lang['text_heading'] 		            = 'Cash Payments';
$lang['text_edit_heading'] 		        = 'Store: %s';
$lang['payments_to_spotneat']			= 'Cash Payments';
$lang['column_staff']					= 'Client Name';
$lang['column_location']				= 'Store';
$lang['column_percentage']				= 'Commission Percentage';
$lang['column_amount']					= 'Total Sales Amount';
$lang['column_Commission']				= 'Commission Amount';
$lang['column_total_Commission']		= 'Total Commission Amount';
$lang['column_admin_Commission']		= 'Table Booking Amount';
$lang['column_date']					= 'Date';
$lang['column_reservation']				= 'Order ID';
$lang['text_empty'] 			        = 'There is no data to display';
$lang['column_vendor_name']				= 'Vendor/Client Name';