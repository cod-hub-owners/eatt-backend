<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Payments to EatT';
$lang['text_heading'] 		            = 'Payments to EatT';
$lang['text_edit_heading'] 		        = 'Restaurant: %s';
$lang['payments_to_spotneat']			= 'Payments to EatT';
$lang['column_staff']					= 'Client Name';
$lang['column_location']				= 'Restaurant/Café';
$lang['column_percentage']				= 'Commission Percentage';
$lang['column_amount']					= 'Total Sales Amount';
$lang['column_Commission']				= 'Commission Amount';
$lang['column_admin_Commission']		= 'Table Booking Amount';
$lang['column_date']					= 'Date';
$lang['column_reservation']				= 'Reservation ID';
$lang['text_empty'] 			        = 'There is no data to display';