<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Refund';
$lang['text_heading'] 		            = 'Refund';
$lang['text_edit_heading'] 		        = 'Table: %s';
$lang['text_list'] 		                = 'Refund List';
$lang['text_tab_general'] 		        = 'Refund Details';
$lang['text_empty'] 		            = 'There is no data to display';
$lang['text_no_match'] 		            = 'No Matches Found';
$lang['text_filter_search'] 		    = 'Search table name.';
$lang['text_filter_status'] 		    = 'View all status';
$lang['text_pay_button'] 		        = 'Bulk Refund';
$lang['column_name'] 		            = 'Name';
$lang['column_reservation']             = 'Reservation ID';
$lang['column_cust_fname']              = 'First Name';
$lang['column_cust_lname']              = 'Last Name';
$lang['column_email']                   = 'Email ID';
$lang['column_refund_amount'] 		    = 'Refund amount';
$lang['column_capacity'] 		        = 'Capacity';
$lang['column_status'] 		            = 'Status';
$lang['column_type'] 		            = 'Payment status';
$lang['column_id'] 		                = 'ID';
$lang['text_requested'] 		        = 'Requested';
$lang['text_paid'] 		                = 'Paid';
$lang['label_name'] 		            = 'Name';
$lang['label_min_capacity'] 		    = 'Minimum';
$lang['label_capacity'] 		        = 'Capacity';
$lang['label_status'] 		            = 'Status';

$lang['error_capacity'] 		        = 'The Maximum capacity value must be greater than minimum capacity value.';
$lang['label_additional_charge'] 	    = 'Additional Charge';
$lang['label_total_price'] 	            = 'Total Price';

/* End of file tables_lang.php */
/* Location: ./admin/language/english/tables_lang.php */