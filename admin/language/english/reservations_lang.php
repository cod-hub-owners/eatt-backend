<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Reservations';
$lang['text_heading'] 		            = 'Reservations';
$lang['text_edit_heading'] 		        = 'Reservation: %s';
$lang['text_list'] 		                = 'Reservation List';
$lang['text_tab_general'] 		        = 'Reservation';
$lang['text_tab_status'] 		        = 'Status & Assign';
$lang['text_tab_table'] 		        = 'Table';
$lang['text_tab_store'] 		    	= 'Restaurant';
$lang['text_tab_customer'] 		        = 'Customer';
$lang['text_tab_delivery_boy'] 		    = 'Assign Delivery Boy';
$lang['text_status_history'] 		    = 'Status History';
$lang['text_empty'] 		            = 'There are no reservations available.';
$lang['text_no_status_history'] 		= 'There are no status history for this reservation.';
$lang['text_filter_search'] 		    = 'Search id, location customer name or table.';
$lang['text_filter_location'] 		    = 'View all stores';
$lang['text_filter_status'] 		    = 'View all status';
$lang['text_filter_date'] 		        = 'View all dates';
$lang['text_filter_month'] 		        = 'View all month';
$lang['text_filter_year'] 		        = 'View all year';
$lang['text_switch_to_list'] 		    = 'Switch to list view';
$lang['text_switch_to_calendar'] 		= 'Switch to calender view';
$lang['text_email_sent'] 		        = 'Reservation Confirmation Email SENT';
$lang['text_email_not_sent'] 		    = 'Reservation Confirmation Email not SENT';
$lang['text_tab_menu'] 		            = 'Menu Items  &nbsp;<span class="badge">%s</span>';
$lang['text_no_booking'] 		        = 'No Bookings';
$lang['text_half_booking'] 		        = 'Half Booked';
$lang['text_fully_booked'] 		        = 'Fully Booked';
$lang['text_no_comment'] 		        = 'No status update comment';

$lang['column_id'] 		                = 'ID';
$lang['column_location'] 		        = 'Store';
$lang['column_customer_name'] 		    = 'Name';
$lang['column_guest'] 		            = 'Guest(s)';
$lang['column_table'] 		            = 'Table';
$lang['column_status'] 		            = 'Status';
$lang['column_staff'] 		            = 'Assigned Staff';
$lang['column_time_date'] 		        = 'Time - Date';
$lang['column_comment'] 		        = 'Comment';
$lang['column_assignee'] 		        = 'Staff Assignee';
$lang['column_notify'] 		            = 'Customer Notified';
$lang['column_otp'] 		            = 'Unique Code';
$lang['column_name_option'] 		    = 'Name/Options';
$lang['column_price'] 		            = 'Price';
$lang['column_total'] 		            = 'Total';

$lang['label_reservation_id'] 		    = 'Reservation ID';
$lang['label_guest'] 		            = 'Guest Number';
$lang['label_reservation_date'] 		= 'Reservation Date';
$lang['label_reservation_time'] 		= 'Reservation Time';
$lang['label_occasion'] 		        = 'Occasion';
$lang['label_date_added'] 		        = 'Date Added';
$lang['label_date_modified'] 		    = 'Date Modified';
$lang['label_notify'] 		            = 'Notify Customer';
$lang['label_comment'] 		            = 'Comment';
$lang['label_user_agent'] 		        = 'User Agent';
$lang['label_ip_address'] 		        = 'IP Address';
$lang['label_status'] 		            = 'Reservation Status';
$lang['label_assign_staff'] 		    = 'Assign Staff';
$lang['label_store_name'] 		    = 'Store Name';
$lang['label_store_address'] 		= 'Store Address';
$lang['label_table_name'] 		        = 'Table Name';
$lang['label_table_capacity'] 		    = 'Table Capacity';
$lang['label_table_min_capacity'] 		= 'Table Minimum';
$lang['label_customer_name'] 		    = 'Name';
$lang['label_customer_email'] 		    = 'Email';
$lang['label_customer_telephone'] 		= 'Telephone';
$lang['label_unique_code'] 				= 'Unique Code';
$lang['column_bookingon'] 				= 'Booking on';
$lang['label_booking_price']	 		= 'Table Booking Price';
$lang['no_orders']	 		= 'No orders';
$lang['full_refund']	 		= 'Full Refund';
$lang['apply_policy']	 		= 'Refund Policy';
$lang['cancel_policy']	 		= 'Cancellation & Refund Policy';
$lang['cancel_percent']           ='% of Cancellation';
$lang['reservation_time']           ='Reservation Time';
$lang['cancellation_time']           ='Cancellation Time';
$lang['cancel_percent']           ='% of Cancellation';
$lang['reservation_time']           ='Reservation Time';
$lang['cancellation_time']           ='Cancellation Time';
$lang['total_amount']	='Total Amount';
$lang['refund_amount']	='Refund Amount';
/* End of file reservations_lang.php */
/* Location: ./admin/language/english/reservations_lang.php */