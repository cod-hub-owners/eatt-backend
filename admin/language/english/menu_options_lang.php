<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Menu Options';
$lang['text_heading'] 		            = 'Menu Options';
$lang['text_edit_heading'] 		        = 'Menu Option: %s';
$lang['text_list'] 		                = 'Menu Option List';
$lang['text_tab_general'] 		        = 'Details';
$lang['text_tab_values'] 		        = 'Option Values';
$lang['text_tab_sub_options'] 		    = 'Sub Options';
$lang['text_no_match'] 		            = 'No Matches Found';
$lang['text_empty'] 		            = 'There are no menu options available.';
$lang['text_filter_search'] 		    = 'Search name or price.';
$lang['text_filter_display_type'] 	    = 'View all display types';
$lang['text_select'] 		            = 'Select';
$lang['text_radio'] 		            = 'Radio';
$lang['text_checkbox'] 		            = 'Checkbox';

$lang['column_name'] 		            = 'Name';
$lang['column_priority'] 		        = 'Priority';
$lang['column_display_type'] 		    = 'Display Type';
$lang['column_priority'] 		        = 'Priority';
$lang['column_id'] 		                = 'ID';

$lang['label_option_name'] 		        = 'Option Name';
$lang['label_option_name_arabic']       = 'Arabic Option Name';
$lang['label_priority'] 		        = 'Priority';
$lang['label_display_type'] 		    = 'Display Type';
$lang['label_option_value_arabic']      = 'Arabic Option Value';
$lang['label_option_value'] 		    = 'Option Value';
$lang['label_option_price'] 		    = 'Option Price';
$lang['label_option_id'] 		        = 'Option ID';
$lang['label_sub_option_name'] 		    = 'Sub Options';


/* End of file menu_options_lang.php */
/* Location: ./admin/language/english/menu_options_lang.php */