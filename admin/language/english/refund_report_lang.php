<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		     = 'Commission Report';
$lang['text_heading'] 		     = 'Commission Report';
$lang['text_edit_heading'] 		 = 'Restaurant: %s';
$lang['payments_report']		 = 'Commission Report';
$lang['column_reserve']			 = 'Reservation No';
$lang['column_cust_name']		 = 'Customer Name';
$lang['column_cust_email']		 = 'Customer Email';
$lang['column_receipt']			 = 'Receipt No';
$lang['column_total_amount']	 = 'Total Amount';
$lang['column_amount_refund']	 = 'Refunded Amount';
$lang['column_amount_received']	 = 'Amount Received';
$lang['column_payment_date']	 = 'Payment Date';
$lang['column_payment_type']	 = 'Payment Type';
$lang['column_date']			 = 'Date';
$lang['column_no_orders']		 = 'No Of Orders';
$lang['column_reservation_id']	 = 'Reservation ID';
$lang['text_empty']	 = 'There is no data to display';