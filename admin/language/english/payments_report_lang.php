<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Commission Report';
$lang['text_heading'] 		            = 'Commission Report';
$lang['text_edit_heading'] 		        = 'Restaurant: %s';
$lang['payments_report']				= 'Commission Report';
$lang['column_receipt']					= 'Receipt Number';
$lang['column_total_amount']			= 'Total Order Amount';
$lang['column_amount_paid']				= 'Amount Paid';
$lang['column_comm_sent']				= 'Commission Sent';
$lang['column_comm_rcvd']				= 'Commission Received';
$lang['column_payment_date']			= 'Payment Date';
$lang['column_description']				= 'Description';
$lang['column_date']					= 'Date';
$lang['column_no_orders']				= 'No Of Orders';
$lang['column_reservation_id']			= 'Reservation ID';
$lang['text_empty'] 			        = 'There is no data to display';
$lang['column_payment_method']			= 'Payment method';
$lang['column_amount_sent']				= 'Amount Sent';
$lang['column_amount_rcvd']				= 'Amount Received';