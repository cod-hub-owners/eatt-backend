<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Staff';
$lang['text_heading'] 		            = 'Staff';
$lang['text_edit_heading'] 		        = 'Staff: %s';
$lang['vendor_edit_heading'] 		    = 'General Settings';
$lang['text_list'] 		                = 'Staff List';
$lang['text_tab_general'] 		        = 'Staff Details';
$lang['text_tab_setting'] 		        = 'Basic Settings';
$lang['text_filter_search'] 		    = 'Search Store, name or email.';
$lang['text_filter_group'] 		        = 'View all groups';
$lang['text_filter_location'] 		    = 'View all Stores';
$lang['text_filter_date'] 		        = 'View all dates';
$lang['text_filter_status'] 		    = 'View all status';
$lang['text_empty'] 		            = 'There are no staffs available.';
$lang['text_no_match'] 		            = 'No Matches Found';

$lang['column_name'] 		            = 'Name';
$lang['column_email'] 		            = 'Email';
$lang['column_telephone'] 		        = 'Telephone';
$lang['column_group'] 		            = 'Clients/Admin';
$lang['column_location'] 		        = 'Restaurant';
$lang['column_store'] 		        	= 'Restaurant Name';
$lang['column_date_added'] 		        = 'Date Added';
$lang['column_status'] 		            = 'Status';
$lang['column_id'] 		                = 'ID';

$lang['label_name'] 		            = 'Name';
$lang['label_email'] 		            = 'Email';
$lang['label_telephone'] 		        = 'Telephone';
$lang['label_username'] 		        = 'Username';
$lang['label_password'] 		        = 'Password';
$lang['label_confirm_password'] 		= 'Password Confirm';
$lang['label_commission'] 				= 'Commission';
$lang['label_delivery_commission'] 		= 'Delivery Boy Commission';
$lang['label_group'] 		            = 'Roles';
$lang['label_type'] 		            = 'Type';
$lang['label_location'] 		        = 'Restaurant Name';
$lang['label_status'] 		            = 'Status';

$lang['help_password'] 		            = 'Leave blank to leave password unchanged';
$lang['help_username'] 		            = 'Username can not be changed.';
$lang['text_tab_permission'] 	        = 'Privileges';

/* End of file staffs_lang.php */
/* Location: ./admin/language/english/staffs_lang.php */

$lang['text_tab_payment_settings'] 		= 'Payment Settings';

$lang['text_payment_title'] 		    = '2checkout payment';
$lang['text_payment_sandbox'] 	        = 'Sandbox';
$lang['text_payment_go_live'] 	        = 'Go Live';
$lang['text_payment_sale'] 	            = 'SALE';
$lang['text_payment_authorization']     = 'AUTHORIZATION';

$lang['label_payment_title'] 	        = 'Title';
$lang['label_payment_type'] 	        = 'Payment Type';
$lang['label_payment_username'] 	    = 'username';
$lang['label_payment_password'] 	    = 'password';
$lang['label_payment_seller_id'] 	    = 'Seller ID';
$lang['label_payment_private_key'] 	    = 'Private Key';
$lang['label_payment_secret_word'] 	    = 'Secret Word';
$lang['label_publishable_key'] 	        = 'Publisable key';
$lang['label_api_pay_username'] 	    = 'Api Username';
$lang['label_api_pay_password'] 	    = 'Api password';

$lang['label_payment_api_signature']    = 'API Signature';
$lang['label_payment_api_mode'] 	    = 'Mode';
$lang['label_payment_api_action'] 	    = 'Payment Action';
$lang['label_payment_order_total'] 	    = 'Order Total';
$lang['label_payment_order_status']     = 'Order Status';
$lang['label_payment_priority'] 	    = 'Priority';
$lang['label_payment_status'] 	        = 'Status';

$lang['label_first_name'] 	       		= 'First Name';
$lang['label_user_name'] 	       		= 'User Name';
$lang['label_password'] 	       		= 'Password';
$lang['label_payment_key'] 	       		= 'Payment Key';
$lang['label_merchant_id'] 	       		= 'Merchant ID';
$lang['label_status'] 	       			= 'Status';
$lang['label_last_name'] 	        	= 'Last Name';
$lang['label_business_url'] 	        = 'Business Url';
$lang['label_business_mcc'] 	        = 'Business Mcc';
$lang['label_city'] 	        		= 'City';
$lang['label_country'] 	        		= 'Country';
$lang['label_line1'] 	        		= 'Address Line1';
$lang['label_postal_code'] 	        	= 'Postal Code';
$lang['label_state'] 	        		= 'State';
$lang['label_dob_day'] 	        		= 'Day';
$lang['label_dob'] 	        		= 'Date of birth';
$lang['label_dob_month'] 	        	= 'Month';
$lang['label_dob_year'] 	        	= 'Year';
$lang['label_gender'] 	        		= 'Gender';
$lang['label_ssn_last_4'] 	        	= 'Last 4 SSN';
$lang['label_phone'] 	        		= 'Phone';
$lang['label_front_doc'] 	        	= 'Front Doc';
$lang['label_back_doc'] 	        	= 'Back Doc';
$lang['label_routing_number'] 	        = 'Routing Number';
$lang['label_account_number'] 	        = 'Account Number';
$lang['label_tax_id'] 	        		= 'Tax ID';
$lang['column_restaurant'] 	        	= 'Restaurant';
