<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Categories';
$lang['text_heading'] 		            = 'Categories';
$lang['text_edit_heading'] 		        = 'Category: %s';
$lang['text_list'] 		                = 'Category List';
$lang['text_tab_general'] 		        = 'Category Details';
$lang['text_filter_search'] 		    = 'Search category name, description or status.';
$lang['text_filter_status'] 		    = 'View all status';
$lang['text_empty'] 		            = 'There are no categories available.';

$lang['column_name'] 		            = 'Name';
$lang['column_description'] 		    = 'Description';
$lang['column_parent'] 		            = 'Parent';
$lang['column_priority'] 		        = 'Priority';
$lang['column_status'] 		            = 'Status';
$lang['column_id'] 		                = 'ID';

$lang['label_name'] 		            = 'Name';
$lang['label_description'] 		        = 'Description';
$lang['label_name_ar'] 		            = 'Arabic Name';
$lang['label_description_ar'] 		    = 'Arabic Description';
$lang['label_permalink_id'] 		    = 'Permalink ID';
$lang['label_permalink_slug'] 		    = 'Permalink Slug';
$lang['label_parent'] 		            = 'Parent';
$lang['label_image'] 		            = 'Image';
$lang['label_priority'] 		        = 'Priority';
$lang['label_status'] 		            = 'Status';
$lang['label_alcohol_status'] 		    = 'Alcohol';
$lang['category_exists'] 		    	= 'Category already exist.';

$lang['help_photo'] 		            = 'Select a file to update category image, otherwise leave blank.';
$lang['help_permalink'] 		        = 'Use ONLY alpha-numeric lowercase characters, underscores or dashes and make sure it is unique GLOBALLY.';


/* End of file categories_lang.php */
/* Location: ./admin/language/english/categories_lang.php */