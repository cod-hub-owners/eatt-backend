<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		            = 'Delivery Partners List';
$lang['text_heading'] 		            = 'Delivery Payouts';
$lang['partner_name'] 		            = 'Partner Name';
$lang['rejected']	 		            = 'Rejected';
$lang['completed']	 		            = 'Completed';
$lang['select']	 		            	= 'Select';
$lang['view_all']	 		            = 'View All';
$lang['partner_email'] 		            = 'Partner Email';
$lang['wallet'] 		            	= 'Wallet Amount';
$lang['request'] 		            	= 'Request Amount';
$lang['actions'] 		            	= 'Action';
$lang['completed_history'] 		        = 'Completed/Rejected History';
$lang['pending_history'] 		        = 'Pending Requests';
$lang['request_date'] 		        	= 'Request Date';
$lang['request_amount'] 		        = 'Request Amount';
$lang['invoice_no'] 		        	= 'Invoice No';
$lang['status'] 		        		= 'Status';