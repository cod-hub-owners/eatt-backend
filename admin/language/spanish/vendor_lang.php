<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'clientes' ;
$lang['text_heading'] = '%s' ;
$lang['text_edit_heading'] = 'Categoria: %s' ;
$lang['text_list'] = 'Lista de usuarios' ;
$lang['text_tab_general'] = 'detalles de categoria' ;
$lang['text_tab_payout'] = 'Detalles de pago' ;
$lang['text_filter_search'] = 'Buscar el nombre, la descripción o el estado de la categoría.' ;
$lang['text_filter_status'] = 'Ver todos los estados' ;
$lang['text_empty'] = 'No hay categorías disponibles.' ;
$lang['text_payout'] = 'No hay pagos disponibles.' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_description'] = 'Descripción' ;
$lang['column_parent'] = 'Padre' ;
$lang['column_priority'] = 'Prioridad' ;
$lang['column_status'] = 'Estado' ;
$lang['column_id'] = 'Identificación' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_description'] = 'Descripción' ;
$lang['label_permalink_id'] = 'ID de Permalink ' ;
$lang['label_permalink_slug'] = 'indicacion de Permalink ' ;
$lang['label_parent'] = 'Padre' ;
$lang['label_image'] = 'Imagen' ;
$lang['label_priority'] = 'Prioridad' ;
$lang['label_status'] = 'Estado' ;
$lang['label_amount'] = 'Cantidad' ;
$lang['help_photo'] = 'Seleccione un archivo para actualizar la imagen de la categoría, de lo contrario, déjelo en blanco.' ;
$lang['help_permalink'] = 'Utilice SOLAMENTE caracteres alfanuméricos en minúsculas, subrayados o guiones y asegúrese de que es único GLOBALMENTE.' ;
$lang['client_name'] = 'Nombre de usuario' ;
$lang['client_email'] = 'correo electrónico del usuario' ;
$lang['view_categories'] = 'Ver Categorías' ;
$lang['pay_to_bank'] = 'Pagar a Banco' ;
$lang['actions'] = 'accion' ;
$lang['view_menus'] = 'Ver menús' ;
$lang['view_reservations'] = 'Ver Reservas' ;
$lang['view_orders'] = 'Ver pedidos' ;
$lang['view_reviews'] = 'Ver comentarios' ;
$lang['rest/cafe'] = 'nombre del restaurante' ;
$lang['credit'] = 'Crédito' ;
$lang['debit'] = 'Débito' ;
$lang['wallet_amount'] = 'Cantidad de la cartera' ;
$lang['pending_amount'] = 'importe de la solicitud' ;
$lang['completed_amount'] = 'Cantidad' ;
$lang['invoice_no'] = 'no factura' ;




/* End of file categories_lang.php */
/* Location: ./admin/language/english/categories_lang.php */