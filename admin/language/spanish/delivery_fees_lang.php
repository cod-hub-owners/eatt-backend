<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Gastos de envío' ;
$lang['text_heading'] = 'Gastos de envío' ;
$lang['text_delivery_fee'] = 'Gastos de envío' ;
$lang['label_standard_fee'] = 'Tarifa de entrega estándar' ;
$lang['label_premium_fee'] = 'Cuota de entrega Premium' ;
$lang['standard_fee'] = 'Tarifa de entrega estándar' ;
$lang['premium_fee'] = 'Cuota de entrega Premium' ;
$lang['label_first_name'] = 'Nombre de pila' ;
$lang['label_user_name'] = 'Nombre de usuario' ;