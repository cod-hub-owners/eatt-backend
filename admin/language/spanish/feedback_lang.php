<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Comentarios' ;
$lang['text_heading'] = 'Comentarios' ;
$lang['text_edit_heading'] = 'comentarios: %s' ;
$lang['text_list'] = 'Lista de comentarios' ;
$lang['text_tab_general'] = 'Comentarios' ;
$lang['text_tab_history'] = 'Historia' ;
$lang['text_filter_search'] = 'Nombre o código de búsqueda.' ;
$lang['text_filter_type'] = 'Ver todos los tipos' ;
$lang['text_filter_status'] = 'Ver todos los restaurantes' ;
$lang['text_empty'] = 'Datos no disponibles.' ;
$lang['text_no_history'] = 'No hay historia de este cupón.' ;
$lang['text_coupon'] = 'evaluaciones' ;
$lang['text_fixed_amount'] = 'Queja' ;
$lang['text_percentage'] = 'Sugerencia' ;
$lang['text_24_hour'] = '24 horas' ;
$lang['text_custom'] = 'personalizado' ;
$lang['text_leading_zeros'] = '' ;
$lang['column_username'] = 'Nombre de usuario' ;
$lang['column_store'] = 'restaurante' ;
$lang['column_feedback_type'] = 'Tipo' ;
$lang['column_feedback_message'] = 'Mensaje' ;
$lang['column_validity'] = 'Validez' ;
$lang['column_status'] = 'Estado' ;
$lang['column_order_id'] = 'Solicitar ID' ;
$lang['column_customer'] = 'Cliente' ;
$lang['column_amount'] = 'Cantidad' ;
$lang['column_date_used'] = 'Fecha de uso' ;
$lang['label_name'] = 'Nombre del cupón' ;
$lang['label_code'] = 'Código' ;
$lang['label_type'] = 'Tipo' ;
$lang['label_discount'] = 'Descuento' ;
$lang['label_min_total'] = 'Total minimo' ;
$lang['label_redemption'] = 'reembolsos' ;
$lang['label_customer_redemption'] = 'Reembolsos de clientes' ;
$lang['label_description'] = 'Descripción' ;
$lang['label_validity'] = 'Validez' ;
$lang['label_order_restriction'] = 'Restricción de pedidos' ;
$lang['label_date'] = 'Fecha' ;
$lang['label_fixed_date'] = 'Fecha fijada' ;
$lang['label_fixed_time'] = 'Horario fijo' ;
$lang['label_fixed_from_time'] = 'Resuuelto desde el tiempo' ;
$lang['label_fixed_to_time'] = 'Resuelto en tiempo' ;
$lang['label_period_start_date'] = 'Fecha de inicio del período' ;
$lang['label_period_end_date'] = 'Fecha de fin de período' ;
$lang['label_recurring_every'] = 'Recurrir a cada' ;
$lang['label_recurring_time'] = 'Tiempo recurrente' ;
$lang['label_recurring_from_time'] = 'Recurrente del tiempo' ;
$lang['label_recurring_to_time'] = 'Recurriente en tiempo' ;
$lang['label_status'] = 'Estado' ;
$lang['help_type'] = 'Ya sea para restar un importe fijo o un porcentaje del total del pedido.' ;
$lang['help_redemption'] = 'El número total de veces que este cupón puede ser canjeado. Introduzca 0 para reembolsos ilimitados.' ;
$lang['help_customer_redemption'] = 'El número de veces que un cliente específico puede canjear este cupón. Introduzca 0 para canjes ilimitados.' ;
$lang['help_order_restriction'] = 'Ya sea para restringir el cupón a una clase de orden específica.' ;


/* End of file coupons_lang.php */
/* Location: ./admin/language/english/coupons_lang.php */