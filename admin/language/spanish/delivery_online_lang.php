<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Entrega en línea' ;
$lang['text_heading'] = 'Entrega en línea' ;
$lang['text_all_heading'] = 'Entrega en línea: todo' ;
$lang['text_online'] = 'En línea' ;
$lang['text_all'] = 'Todos' ;
$lang['text_filter_search'] = 'Busque la IP, la entrega o el navegador.' ;
$lang['text_filter_access'] = 'Ver todos los accesos' ;
$lang['text_filter_date'] = 'Ver todas las fechas' ;
$lang['text_empty'] = 'No hay entrega en línea.' ;
$lang['text_empty_report'] = 'No hay disponible ningún informe de entrega en línea.' ;
$lang['text_guest'] = 'Invitado' ;
$lang['text_private'] = 'Privado' ;
$lang['text_browser'] = 'Navegador' ;
$lang['text_mobile'] = 'Móvil' ;
$lang['text_robot'] = 'Robot' ;
$lang['column_name'] = 'Nombre' ;
$lang['email'] = 'Email' ;
$lang['phone'] = 'Teléfono' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['column_ip'] = 'IP' ;
$lang['column_delivery'] = 'Entrega' ;
$lang['column_access'] = 'Acceso' ;
$lang['column_browser'] = 'Navegador' ;
$lang['column_agent'] = 'Agente de usuario' ;
$lang['column_request_uri'] = 'URL del último pedido' ;
$lang['column_referrer_url'] = 'Último URL de referencia' ;
$lang['column_last_activity'] = 'Última actividad' ;

/* End of file delivery_online_lang.php */
/* Location: ./admin/language/english/delivery_online_lang.php */