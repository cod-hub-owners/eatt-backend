<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Informe de reembolso' ;
$lang['text_heading'] = 'Informe de reembolso' ;
$lang['text_edit_heading'] = 'Restuarante: %s' ;
$lang['payments_report'] = 'Informe de reembolso' ;
$lang['column_reserve'] = 'Sin reserva' ;
$lang['column_cust_name'] = 'Nombre del cliente' ;
$lang['column_cust_email'] = 'Correo electrónico del cliente' ;
$lang['column_receipt'] = 'sin recibo ' ;
$lang['column_total_amount'] = 'Cantidad total' ;
$lang['column_amount_refund'] = 'cantidad de reembolso' ;
$lang['column_amount_received'] = 'Cantidad recibida' ;
$lang['column_payment_date'] = 'Fecha de pago' ;
$lang['column_payment_type'] = 'Tipo de pago' ;
$lang['column_date'] = 'Fecha' ;
$lang['column_no_orders'] = 'No de pedidos' ;
$lang['column_reservation_id'] = 'ID de reserva' ;
$lang['text_empty'] = 'No se han encontrado resultados' ;