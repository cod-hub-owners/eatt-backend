<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'ajustes' ;
$lang['text_heading'] = 'ajustes' ;
$lang['text_tab_general'] = 'General' ;
$lang['text_tab_store'] = 'restaurante' ;
$lang['text_tab_options'] = 'opciones' ;
$lang['text_tab_order'] = 'Orden' ;
$lang['text_tab_reservation'] = 'reservación ' ;
$lang['text_tab_media_manager'] = 'administrador de imagen' ;
$lang['text_tab_mail'] = 'Correo' ;
$lang['text_tab_server'] = 'Servidor' ;
$lang['text_tab_title_maps'] = 'mapas' ;
$lang['text_tab_title_date_time'] = 'Fecha y hora' ;
$lang['text_tab_title_currency'] = 'Moneda' ;
$lang['text_tab_title_language'] = 'Idioma' ;
$lang['text_tab_title_menus'] = 'menús' ;
$lang['text_tab_title_reviews'] = 'Comentarios' ;
$lang['text_tab_title_terms'] = 'Términos y condiciones' ;
$lang['text_tab_title_taxation'] = 'Impuestos' ;
$lang['text_tab_title_account'] = 'Cuenta' ;
$lang['text_tab_title_stock'] = 'Acciones' ;
$lang['text_tab_title_invoice'] = 'Facturación' ;
$lang['text_tab_title_checkout'] = 'Pago' ;
$lang['text_tab_title_restrictions'] = 'restricciones' ;
$lang['text_tab_title_maintenance'] = 'Mantenimiento' ;
$lang['text_tab_title_permalink'] = 'Enlace permanente' ;
$lang['text_tab_title_customer_online'] = 'Cliente en línea' ;
$lang['text_tab_title_caching'] = 'Caché' ;
$lang['text_order'] = 'Orden' ;
$lang['text_reservation'] = 'reserva' ;
$lang['text_utc_time'] = 'Hora actual del UTC' ;
$lang['text_server'] = 'Servidor' ;
$lang['text_24_hour'] = '24 horas' ;
$lang['text_3_days'] = '3 días' ;
$lang['text_5_days'] = '5 dias' ;
$lang['text_1_week'] = '1 semana' ;
$lang['text_never_delete'] = 'No borrar nunca' ;
$lang['text_1_month'] = '1 mes' ;
$lang['text_3_months'] = '3 meses' ;
$lang['text_6_months'] = '6 meses' ;
$lang['text_12_months'] = '12 meses' ;
$lang['text_minutes'] = 'minutos' ;
$lang['text_seconds'] = 'segundos' ;
$lang['text_delete_thumbs'] = 'Eliminar los pulgares' ;
$lang['text_auto'] = 'Automáticamente' ;
$lang['text_manual'] = 'Manualmente' ;
$lang['text_miles'] = 'Millas' ;
$lang['text_kilometers'] = 'kilómetros' ;
$lang['text_mail'] = 'CORREO' ;
$lang['text_sendmail'] = 'ENVIAR CORREO' ;
$lang['text_smtp'] = 'SMTP' ;
$lang['text_to_customer'] = 'Al cliente' ;
$lang['text_to_admin'] = 'Al administrador' ;
$lang['text_to_location'] = 'Al restaurante ' ;
$lang['text_send_test_email'] = 'Envío del correo electrónico de prueba' ;
$lang['text_internal_sequence_prefix'] = 'Prefijo más Secuencia de numeración interna' ;
$lang['text_order_number_prefix'] = 'Prefijo más Número de pedido' ;
$lang['text_order_number'] = 'Sólo número de pedido' ;
$lang['text_menu_price_include_tax'] = 'Los precios de los menús ya incluyen el impuesto' ;
$lang['text_apply_tax_on_menu_price'] = 'Aplicar impuestos sobre el precio de mi menú' ;
$lang['text_percentage_sign'] = '%' ;
$lang['button_add_location'] = 'Ancho de miniaturas' ;
$lang['label_site_name'] = 'Añadir nuevos restaurantes' ;
$lang['label_site_email'] = 'correo electrónico del administrador' ;
$lang['label_site_logo'] = 'Logo' ;
$lang['label_timezone'] = 'La zona horaria predeterminada' ;
$lang['label_date_format'] = 'Formato de la fecha' ;
$lang['label_time_format'] = 'Formato del tiempo' ;
$lang['label_site_currency'] = 'moneda predeterminada' ;
$lang['label_auto_update_rates'] = 'Tasas de actualización automática' ;
$lang['label_accepted_currency'] = 'Moneda aceptada' ;
$lang['label_detect_language'] = 'Detectar idioma del navegador ' ;
$lang['label_site_language'] = 'Idioma predeterminado' ;
$lang['label_admin_language'] = 'Idioma predeterminado del administrador' ;
$lang['label_customer_group'] = 'grupo de clientes' ;
$lang['label_page_limit'] = 'Artículos por página' ;
$lang['label_meta_description'] = 'Metadescripción' ;
$lang['label_meta_keyword'] = 'meta Keywords' ;
$lang['label_menu_page_limit'] = 'Menús por Página' ;
$lang['label_show_menu_image'] = 'Mostrar imagenes del menú ' ;
$lang['label_menu_image'] = 'Imágenes de menú Dimensión' ;
$lang['label_menu_image_height'] = 'Imágenes de menú Altura' ;
$lang['label_menu_image_width'] = 'Imágenes de menú Ancho' ;
$lang['label_special_category'] = 'Categoría especial' ;
$lang['label_country'] = 'País' ;
$lang['label_default_location'] = 'restaurante' ;
$lang['label_maps_api_key'] = 'Clave del API de Google Maps' ;
$lang['label_distance_unit'] = 'Unidad de distancia' ;
$lang['label_tax_mode'] = 'Modo de impuestos' ;
$lang['label_tax_title'] = 'Título del impuesto' ;
$lang['label_tax_percentage'] = 'Porcentaje del impuesto' ;
$lang['label_tax_menu_price'] = 'Precio del menú de impuestos' ;
$lang['label_tax_delivery_charge'] = 'Cargo por entrega de impuestos' ;
$lang['label_stock_checkout'] = 'Comprobación de existencias' ;
$lang['label_show_stock_warning'] = 'Mostrar aviso de stock' ;
$lang['label_allow_reviews'] = 'Permitir Comentarios' ;
$lang['label_approve_reviews'] = 'aprobar Comentarios' ;
$lang['label_registration_terms'] = 'Condiciones de registro' ;
$lang['label_checkout_terms'] = 'Condiciones de compra' ;
$lang['label_default_order_status'] = 'Estado de la orden por defecto' ;
$lang['label_processing_order_status'] = 'Tratamiento del status de la orden' ;
$lang['label_completed_order_status'] = 'Estado de la orden concluida' ;
$lang['label_canceled_order_status'] = 'Estado de la orden de anulación' ;
$lang['label_delivery_time'] = 'Tiempo de entrega ' ;
$lang['label_collection_time'] = 'Hora de recogida' ;
$lang['label_guest_order'] = 'Permitir el pedido de invitados' ;
$lang['label_location_order'] = 'Requerir el restaurante para pedir' ;
$lang['label_future_order'] = 'Aceptar ordenes futuras' ;
$lang['label_auto_invoicing'] = 'Generar factura' ;
$lang['label_invoice_prefix'] = 'Prefijo de la factura' ;
$lang['label_reservation_mode'] = 'Modo de reserva' ;
$lang['label_default_reservation_status'] = 'Estado de la reserva por defecto' ;
$lang['label_confirmed_reservation_status'] = 'Estado de la reserva confirmado ' ;
$lang['label_canceled_reservation_status'] = 'Estado de la reservación cancelada' ;
$lang['label_reservation_time_interval'] = 'Intervalo de tiempo de reservación' ;
$lang['label_reservation_stay_time'] = 'Reservación de tiempo de estancia ' ;
$lang['label_media_max_size'] = 'Tamaño máximo de archivo' ;
$lang['label_media_thumb'] = 'Dimensión miniatura' ;
$lang['label_media_thumb_height'] = 'Altura miniatura' ;
$lang['label_media_thumb_width'] = 'Ancho de miniaturas' ;
$lang['label_media_uploads'] = 'subidas' ;
$lang['label_media_new_folder'] = 'Nueva carpeta' ;
$lang['label_media_copy'] = 'Copiar' ;
$lang['label_media_move'] = 'Mover' ;
$lang['label_media_rename'] = 'Renombrar' ;
$lang['label_media_delete'] = 'Eliminar' ;
$lang['label_media_transliteration'] = 'Transcripción' ;
$lang['label_media_remember_days'] = 'Recordar la ultima carpeta' ;
$lang['label_thumbs'] = 'pulgares' ;
$lang['label_registration_email'] = 'Registro electrónico' ;
$lang['label_order_email'] = 'Confirmación del pedido/ Email de alerta' ;
$lang['label_reservation_email'] = 'Confirmación de la reservación/Correo electrónico de alerta' ;
$lang['label_protocol'] = 'Protocolo de correo' ;
$lang['label_smtp_host'] = 'Host SMTP' ;
$lang['label_smtp_port'] = 'Puerto SMTP' ;
$lang['label_smtp_user'] = 'Nombre de usuario SMTP' ;
$lang['label_smtp_pass'] = 'Contraseña SMTP' ;
$lang['label_test_email'] = 'Email de prueba' ;
$lang['label_customer_online_time_out'] = 'Tiempo de espera en línea para el cliente' ;
$lang['label_customer_online_archive_time_out'] = 'Tiempo de espera del archivo en línea del cliente' ;
$lang['label_permalink'] = 'Enlace permanente' ;
$lang['label_maintenance_mode'] = 'Modo de mantenimiento' ;
$lang['label_maintenance_message'] = 'Mensaje de mantenimiento' ;
$lang['label_cache_mode'] = 'Modo de caché' ;
$lang['label_cache_time'] = 'Tiempo de Cache' ;
$lang['alert_email_sending'] = 'Enviando correo electrónico... ' ;
$lang['alert_email_sent'] = 'Mensaje enviado a %s' ;
$lang['alert_success_thumb_deleted'] = 'Pulgares borrados con éxito.' ;
$lang['help_timezone'] = 'La zona horaria por defecto. Elija una ciudad en la misma zona horaria que su tienda.' ;
$lang['help_detect_language'] = 'Activar o desactivar la detección de idioma del navegador del usuario. Si se habilita EatT será traducido al idioma del navegador.' ;
$lang['help_auto_update_rates'] = 'Establecer si desea actualizar los tipos de cambio automáticamente cada 24 horas o de forma manual. Los tipos de cambio son importados de la base de datos de finanzas Yahoo.' ;
$lang['help_accepted_currency'] = 'Seleccionar todas las monedas que acepta como medio de pago, así como su moneda por defecto' ;
$lang['help_page_limit'] = 'Limitar el número de elementos que se muestran por página' ;
$lang['help_menu_page_limit'] = 'Limitar el número de menús que se muestran por página' ;
$lang['help_show_menu_image'] = 'Mostrar u ocultar las imágenes del menú en la página del menú de visualización' ;
$lang['help_dimension'] = '(Alto x ancho)' ;
$lang['help_special_category'] = 'Seleccione la categoría que se utilizará automáticamente para los menús especiales' ;
$lang['help_tax_mode'] = 'Establezca si desea activar o desactivar el cálculo de impuestos en la tienda' ;
$lang['help_tax_title'] = 'Introduzca el título del impuesto tal y como debería mostrarse en la tienda. Ej. IVA' ;
$lang['help_tax_percentage'] = 'Introduzca el porcentaje para calcular los impuestos. Ej. 15' ;
$lang['help_tax_menu_price'] = 'Establecer si el precio del menú ya incluye impuestos o si los impuestos deben ser calculados en el precio del menú' ;
$lang['help_tax_delivery_charge'] = 'Fijar si los gastos de entrega son gravables' ;
$lang['help_stock_checkout'] = 'Permita a los clientes revisar si el menú que están pidiendo no está en stock.' ;
$lang['help_show_stock_warning'] = 'Mostrar el mensaje de fuera de stock en el carrito si un menú está fuera de stock pero la salida de stock está habilitada. (La advertencia siempre muestra si la salida de stock está desactivada)' ;
$lang['help_allow_reviews'] = 'Habilitar o deshabilitar la entrada de revisiones de nuevos clientes y la visualización de las revisiones en la tienda' ;
$lang['help_approve_reviews'] = 'Aprobar nueva entrada de revisión automática o manualmente' ;
$lang['help_checkout_terms'] = 'Requerir a los clientes que acepten los términos antes de la compra' ;
$lang['help_registration_terms'] = 'Requerir que los clientes acepten los términos antes de que se registre una cuenta' ;
$lang['help_default_location'] = 'Elija o añada una nueva ubicación para establecerla como ubicación principal/por defecto de la tienda.' ;
$lang['help_default_order_status'] = 'Seleccione el estado de pedido predeterminado cuando se realiza/recibe un nuevo pedido' ;
$lang['help_processing_order_status'] = 'Seleccione el status de la orden que debe alcanzar una orden antes de que ésta inicie la reducción de stock y redención de cupones' ;
$lang['help_completed_order_status'] = 'Seleccione el estado del pedido para marcar un pedido como completado antes de que se cree la factura del pedido y un cliente pueda dejar la revisión' ;
$lang['help_canceled_order_status'] = 'Seleccione el estado de la orden cuando una orden está marcado como cancelado o se sospecha actividad fraudulenta' ;
$lang['help_delivery_time'] = 'Número establecido de minutos que un pedido será entregado después de ser realizado' ;
$lang['help_collection_time'] = 'Establecer el número de minutos que un pedido estará listo para ser recogido después de ser colocado' ;
$lang['help_guest_order'] = 'Permitir al cliente hacer un pedido sin crear una cuenta.' ;
$lang['help_location_order'] = 'Establezca si el cliente DEBE introducir su código postal/dirección antes de poder realizar un pedido. Si está desactivado, el cliente podrá realizar el pedido sin introducir su código postal/dirección, pero seguirá necesitando que se cubra su dirección de entrega.' ;
$lang['help_future_order'] = 'Permita que el cliente haga el pedido para más tarde cuando la tienda esté cerrada para la entrega o la recogida durante las horas de apertura. Esta configuración afecta a todas las ubicaciones' ;
$lang['help_auto_invoicing'] = 'Generar factura tan pronto como el orden se marque como ejecutada de forma automática o manualmente haciendo clic con el botón de generar en la página de pedido.' ;
$lang['help_invoice_prefix'] = 'Establezca el prefijo de la factura (p. ej. <b>INV-2015-00</b>1123). Deje en blanco para no utilizar ningún prefijo. Las siguientes macros están disponibles: {año} {mes} {día}' ;
$lang['help_reservation_mode'] = 'Habilitar o deshabilitar la reserva de mesas en la tienda.' ;
$lang['help_default_reservation_status'] = 'Seleccione el status de reserva por defecto cuando se reciba una nueva reserva' ;
$lang['help_canceled_reservation_status'] = 'Seleccione el estado de la reserva cuando se marca una reserva como cancelada o se sospecha de actividad fraudulenta' ;
$lang['help_confirmed_reservation_status'] = 'Seleccione el estado de la reserva para marcar una reserva como confirmada antes de reservar la mesa' ;
$lang['help_reservation_time_interval'] = 'Establecer el número de minutos entre cada tiempo de reserva' ;
$lang['help_reservation_stay_time'] = 'Fijar en minutos el tiempo promedio de permanencia de un huésped en una mesa' ;
$lang['help_delete_thumbs'] = 'Esto eliminará todos los pulgares creados. Los pulgares de nota se crean automáticamente.' ;
$lang['help_media_max_size'] = 'El límite de tamaño máximo (en kilobytes) para el archivo al subirlo.' ;
$lang['help_media_upload'] = 'Activar o desactivar la carga de archivos' ;
$lang['help_media_new_folder'] = 'Habilitar o deshabilitar la creación de carpetas' ;
$lang['help_media_copy'] = 'Habilitar o deshabilitar la copia de archivos/carpetas' ;
$lang['help_media_move'] = 'Habilitar o deshabilitar el movimiento de archivos/carpetas' ;
$lang['help_media_rename'] = 'Habilitar o deshabilitar el cambio de nombre de archivos/carpetas' ;
$lang['help_media_delete'] = 'Activar o desactivar la eliminación de archivos / carpetas' ;
$lang['help_media_transliteration'] = 'Activar o desactivar la conversión de todos los caracteres no deseados' ;
$lang['help_media_remember_days'] = 'Cuánto tiempo para guardar la última carpeta abierta en la cookie.' ;
$lang['help_registration_email'] = 'Envíe un correo electrónico de confirmación al cliente y/o al administrador después de haber registrado la cuenta con éxito' ;
$lang['help_order_email'] = 'Enviar un correo de confirmación al cliente, administrador y/o correo electrónico de la ubicación después de que se haya creado un nuevo pedido' ;
$lang['help_reservation_email'] = 'Enviar un correo de confirmación al cliente, administrador y/o correo electrónico de la ubicación cuando se recibe una nueva reservación' ;
$lang['help_maintenance'] = 'Habilitar para evitar que los clientes vean su tienda. El mensaje de mantenimiento se mostrará a los clientes excepto al administrador registrado.' ;
$lang['help_permalink'] = 'Para utilizar las URLs de SEO, debe instalarse el módulo mod-rewrite de apache.' ;
$lang['help_customer_online'] = 'El número de segundos que un cliente aparecerá en línea. Los segundos deben ser más de 120 segundos' ;
$lang['help_customer_online_archive'] = 'Borrar todos los informes online de clientes anteriores a' ;
$lang['help_cache_mode'] = 'Habilite esta opción si desea guardar en caché las páginas para obtener el máximo rendimiento.' ;
$lang['help_cache_time'] = 'Establezca el número de minutos que una página permanece en la memoria caché.' ;
$lang['text_tab_reward'] = 'recompensas' ;
$lang['label_rewards_points'] = 'Los puntos de recompensas en (%)' ;
$lang['help_rewards_points'] = 'Puntos de recompensa calculados a partir de la cantidad del subtotal <br> Por ejemplo: la cantidad del subtotal es 500 y su punto de recompensa es (500 * 10%) 50' ;
$lang['label_point_system'] = 'Los puntos de ajuste en (%)' ;
$lang['help_point_system'] = 'Ej: Establezca un 10% de puntos, si tiene 20 puntos en su cuenta y el monto de su pedido fue de 1500, este monto será deducido por 200. Así que, finalmente su monto a pagar es de 1300 solamente.' ;
$lang['label_customer_mobile'] = 'Dígitos para el número móvil' ;