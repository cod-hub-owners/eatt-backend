<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Mesas' ;
$lang['text_heading'] = 'Mesas' ;
$lang['text_edit_heading'] = 'mesa: %s' ;
$lang['text_list'] = 'Lista de mesa' ;
$lang['text_tab_general'] = 'Tabla detalles' ;
$lang['text_empty'] = 'No hay mesas disponibles.' ;
$lang['text_no_match'] = 'No se encontraron coincidencias' ;
$lang['text_filter_search'] = 'buscar el nombre de la mesa' ;
$lang['text_filter_status'] = 'Ver todos los estados' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_min_capacity'] = 'Mínimo' ;
$lang['column_capacity'] = 'Capacidad' ;
$lang['column_status'] = 'Estado' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_min_capacity'] = 'Mínimo' ;
$lang['label_capacity'] = 'Capacidad' ;
$lang['label_status'] = 'Estado' ;
$lang['error_capacity'] = 'El valor de capacidad máxima debe ser mayor que el valor de capacidad mínima.' ;
$lang['label_additional_charge'] = 'Cargos adicionales' ;
$lang['label_total_price'] = 'Precio total' ;

/* End of file tables_lang.php */
/* Location: ./admin/language/english/tables_lang.php */