<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Informe de la Comisión' ;
$lang['text_heading'] = 'Informe de la Comisión' ;
$lang['text_edit_heading'] = 'Restaurante: %s' ;
$lang['payments_report'] = 'Informe de la Comisión' ;
$lang['column_receipt'] = 'N º de pedido' ;
$lang['column_total_amount'] = 'Importe total del pedido' ;
$lang['column_amount_paid'] = 'Cantidad pagada' ;
$lang['column_amount_received'] = 'Comisión recibida' ;
$lang['column_payment_date'] = 'Fecha de pago' ;
$lang['column_description'] = 'Descripción' ;
$lang['column_date'] = 'Fecha' ;
$lang['column_no_orders'] = 'No. de Pedidos' ;
$lang['column_reservation_id'] = 'ID de reserva' ;