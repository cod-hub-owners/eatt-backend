<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'diseños' ;
$lang['text_heading'] = 'diseños' ;
$lang['text_edit_heading'] = 'Diseño: %s' ;
$lang['text_list'] = 'Lista de diseño' ;
$lang['text_empty'] = 'No hay diseños disponibles.' ;
$lang['text_partial_empty'] = 'No hay módulos añadidos.' ;
$lang['text_tab_general'] = 'Módulos de diseño' ;
$lang['text_tab_details'] = 'detalles' ;
$lang['text_modules'] = 'módulos' ;
$lang['column_name'] = 'Diseño' ;
$lang['column_module'] = 'Módulo' ;
$lang['column_partial'] = 'Área parcial' ;
$lang['column_priority'] = 'Prioridad' ;
$lang['column_status'] = 'Estado' ;
$lang['column_uri_route'] = 'Ruta URI' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_route'] = 'Ruta' ;
$lang['label_module_title'] = 'Título' ;
$lang['label_module_fixed'] = 'Fijo' ;
$lang['label_fixed_offset'] = 'Desplazamiento fijo (arriba x abajo)' ;
$lang['label_module_code'] = 'Código del modulo' ;
$lang['label_module_partial'] = 'módulo parcial' ;
$lang['label_module_priority'] = 'Prioridad del módulo' ;
$lang['label_module_status'] = 'Estado' ;

/* End of file layouts_lang.php */
/* Location: ./admin/language/english/layouts_lang.php */