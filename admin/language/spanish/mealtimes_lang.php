<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Horario de comidas' ;
$lang['text_heading'] = 'Horario de comidas' ;
$lang['text_list'] = 'lista de horario de comida' ;
$lang['text_empty'] = 'No hay horas de comida, por favor agregue.' ;
$lang['column_mealtime_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['column_mealtime_name'] = 'Nombre' ;
$lang['column_start_time'] = 'Hora de inicio' ;
$lang['column_end_time'] = 'Hora de finalización' ;
$lang['column_mealtime_status'] = 'Estado' ;
$lang['label_mealtime_id'] = 'Identificador de la hora de comer' ;
$lang['label_mealtime_name'] = 'Nombre de la comida' ;
$lang['label_start_time'] = 'Hora de inicio' ;
$lang['label_end_time'] = 'Hora de finalización' ;
$lang['label_mealtime_status'] = 'Estado' ;

/* End of file mealtimes_lang.php */
/* Location: ./admin/language/english/mealtimes_lang.php */