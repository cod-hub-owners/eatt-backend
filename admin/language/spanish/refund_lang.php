<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Reembolso' ;
$lang['text_heading'] = 'Reembolso' ;
$lang['text_edit_heading'] = 'Clasificación: %s' ;
$lang['text_list'] = 'Lista de reembolso' ;
$lang['text_tab_general'] = 'Detalles del reembolso' ;
$lang['text_empty'] = 'No hay reembolsos disponibles ' ;
$lang['text_no_match'] = 'No se encontraron coincidencias' ;
$lang['text_filter_search'] = 'Buscar nombre de la clasificación ' ;
$lang['text_filter_status'] = 'Ver todos los estados' ;
$lang['text_pay_button'] = 'Reembolso masivo' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_reservation'] = 'ID de reserva' ;
$lang['column_cust_fname'] = 'Nombre de pila' ;
$lang['column_cust_lname'] = 'Apellido' ;
$lang['column_email'] = 'Identificación de correo' ;
$lang['column_refund_amount'] = 'Cantidad de reembolso' ;
$lang['column_capacity'] = 'Capacidad' ;
$lang['column_status'] = 'Estado' ;
$lang['column_type'] = 'Estado de pago' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['text_requested'] = 'Pedido' ;
$lang['text_paid'] = 'Pagado' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_min_capacity'] = 'Mínimo' ;
$lang['label_capacity'] = 'Capacidad' ;
$lang['label_status'] = 'Estado' ;
$lang['error_capacity'] = 'El valor máximo de capacidad debe ser mayor que el valor mínimo de capacidad.' ;
$lang['label_additional_charge'] = 'Cargos adicionales' ;
$lang['label_total_price'] = 'Precio total' ;

/* End of file tables_lang.php */
/* Location: ./admin/language/english/tables_lang.php */