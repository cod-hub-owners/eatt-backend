<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'grupos del personal' ;
$lang['text_heading'] = 'grupos del personal' ;
$lang['text_edit_heading'] = 'grupo de personal: %s' ;
$lang['text_list'] = 'Lista de grupos de personal' ;
$lang['text_tab_general'] = 'Grupo de personal' ;
$lang['text_tab_permission'] = 'privilegios' ;
$lang['text_empty'] = 'No hay grupos de personal disponible.' ;
$lang['text_staff_location'] = 'personal del restaurante' ;
$lang['text_all_location'] = 'todos los restaurantes' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_users'] = '# usuarios' ;
$lang['column_access'] = 'Acceso' ;
$lang['column_manage'] = 'Gestionar' ;
$lang['column_add'] = 'Añadir' ;
$lang['column_delete'] = 'Eliminar' ;
$lang['column_description'] = 'Descripción' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_customer_account_access'] = 'Acceso a la cuenta del cliente' ;
$lang['label_location_access'] = 'tienda estricta' ;
$lang['help_approval'] = 'Los nuevos empleados deben ser aprobados antes de que puedan iniciar la sesión.' ;
$lang['help_customer_account_access'] = 'Habilitar significa que el personal de este grupo puede acceder a cualquier cuenta de cliente utilizando la contraseña del personal. El personal debe estar conectado como administrador.' ;
$lang['help_location'] = 'Habilitar significa que el personal de este grupo SÓLO puede ver los pedidos, las reservas y los cupones en su ubicación.' ;

/* End of file staff_groups_lang.php */
/* Location: ./admin/language/english/staff_groups_lang.php */