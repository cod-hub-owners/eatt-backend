<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Pancartas' ;
$lang['text_heading'] = 'Pancartas' ;
$lang['text_edit_heading'] = 'Pancarta: %s' ;
$lang['text_list'] = 'Lista de Banners' ;
$lang['text_tab_general'] = 'Detalles del Banner' ;
$lang['text_empty'] = 'No hay banners disponibles.' ;
$lang['text_image'] = 'Imagen' ;
$lang['text_carousel'] = 'Carrusel' ;
$lang['text_custom'] = 'Personalizado' ;
$lang['button_modules'] = 'módulos' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_type'] = 'Tipo' ;
$lang['column_status'] = 'Estado' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_type'] = 'Tipo' ;
$lang['label_click_url'] = 'Haga clic en el URL' ;
$lang['label_language'] = 'Idioma' ;
$lang['label_alt_text'] = 'Texto alternativo' ;
$lang['label_status'] = 'Estado' ;
$lang['label_image'] = 'Imagen' ;
$lang['label_images'] = 'imágenes' ;
$lang['label_images_content'] = 'Subir Número de Imágenes en Números Impar (Ex - 2,4,6,8 ....)' ;
$lang['label_custom_code'] = 'Código personalizado' ;
$lang['help_click_url'] = 'Puede utilizar una URL de sitio relativa o absoluta' ;

/* End of file banners_lang.php */
/* Location: ./admin/language/english/banners_lang.php */