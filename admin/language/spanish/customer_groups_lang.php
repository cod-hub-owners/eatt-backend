<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Grupos de clientes' ;
$lang['text_heading'] = 'Grupos de clientes' ;
$lang['text_edit_heading'] = 'Grupo de cliente: %s' ;
$lang['text_list'] = 'Lista de grupos de clientes' ;
$lang['text_tab_general'] = 'Detalles del grupo de cliente' ;
$lang['text_empty'] = 'No hay grupos de clientes disponibles.' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_approval'] = 'Aprobación' ;
$lang['label_description'] = 'Descripción' ;
$lang['help_approval'] = 'Los nuevos clientes deben ser aprobados antes de que puedan iniciar sesión.' ;

/* End of file customer_groups_lang.php */
/* Location: ./admin/language/english/customer_groups_lang.php */