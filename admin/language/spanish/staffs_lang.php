<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Personal' ;
$lang['text_heading'] = 'Personal' ;
$lang['text_edit_heading'] = 'Personal:% s' ;
$lang['vendor_edit_heading'] = 'Configuración general' ;
$lang['text_list'] = 'Lista de personal' ;
$lang['text_tab_general'] = 'Detalles personal' ;
$lang['text_tab_setting'] = 'Ajustes básicos' ;
$lang['text_filter_search'] = 'Buscar tienda, nombre o correo electrónico.' ;
$lang['text_filter_group'] = 'Ver todos los grupos' ;
$lang['text_filter_location'] = 'Ver todas las tiendas' ;
$lang['text_filter_date'] = 'Ver todas las fechas' ;
$lang['text_filter_status'] = 'Ver todos los estados' ;
$lang['text_empty'] = 'No hay personal disponible.' ;
$lang['text_no_match'] = 'No se encontraron coincidencias' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_email'] = 'Email' ;
$lang['column_telephone'] = 'Teléfono' ;
$lang['column_group'] = 'Clientes / Administrador' ;
$lang['column_location'] = 'restaurante' ;
$lang['column_store'] = 'Nombre restaurante' ;
$lang['column_date_added'] = 'Fecha Agregada' ;
$lang['column_status'] = 'Estado' ;
$lang['column_id'] = 'Identificación' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_email'] = 'Email' ;
$lang['label_telephone'] = 'Teléfono' ;
$lang['label_username'] = 'Nombre de usuario' ;
$lang['label_password'] = 'Contraseña' ;
$lang['label_confirm_password'] = 'Contraseña confirmada' ;
$lang['label_commission'] = 'Comisión' ;
$lang['label_delivery_commission'] = 'Comisión del repartidor' ;
$lang['label_group'] = 'Roles' ;
$lang['label_type'] = 'Tipo' ;
$lang['label_location'] = 'Nombre restaurante' ;
$lang['label_status'] = 'Estado' ;
$lang['help_password'] = 'Dejar en blanco para no modificar la contraseña' ;
$lang['help_username'] = 'Nombre de usuario no se puede cambiar.' ;
$lang['text_tab_permission'] = 'privilegios' ;
$lang['text_tab_payment_settings'] = 'Configuración de pago' ;
$lang['text_payment_title'] = 'Pago 2 comprobaciones' ;
$lang['text_payment_sandbox'] = 'Versión de prueba' ;
$lang['text_payment_go_live'] = 'Ir a Producción' ;
$lang['text_payment_sale'] = 'Venta' ;
$lang['text_payment_authorization'] = 'AUTORIZACIÓN' ;
$lang['label_payment_title'] = 'Título' ;
$lang['label_payment_type'] = 'Tipo de pago' ;
$lang['label_payment_username'] = 'nombre de usuario' ;
$lang['label_payment_password'] = 'contraseña' ;
$lang['label_payment_seller_id'] = 'identificación del vendedor' ;
$lang['label_payment_private_key'] = 'Llave privada' ;
$lang['label_payment_secret_word'] = 'Palabra secreta' ;
$lang['label_publishable_key'] = 'Clave pública' ;
$lang['label_api_pay_username'] = 'api nombre de usuario' ;
$lang['label_api_pay_password'] = 'contraseña de la API' ;
$lang['label_payment_api_signature'] = 'Firma API' ;
$lang['label_payment_api_mode'] = 'Modo' ;
$lang['label_payment_api_action'] = 'Acción de pago' ;
$lang['label_payment_order_total'] = 'Total del pedido' ;
$lang['label_payment_order_status'] = 'Estado del pedido' ;
$lang['label_payment_priority'] = 'Prioridad' ;
$lang['label_payment_status'] = 'Estado' ;
$lang['label_first_name'] = 'Nombre de pila' ;
$lang['label_user_name'] = 'Nombre de usuario' ;
$lang['label_payment_key'] = 'clave de pago' ;
$lang['label_merchant_id'] = 'Identificación del comerciante' ;
$lang['label_last_name'] = 'Apellido' ;
$lang['label_business_url'] = 'URL de la empresa' ;
$lang['label_business_mcc'] = 'Mcc negocio' ;
$lang['label_city'] = 'Ciudad' ;
$lang['label_country'] = 'País' ;
$lang['label_line1'] = 'Dirección Línea 1' ;
$lang['label_postal_code'] = 'Código postal' ;
$lang['label_state'] = 'Estado' ;
$lang['label_dob_day'] = 'Día' ;
$lang['label_dob'] = 'Fecha de nacimiento' ;
$lang['label_dob_month'] = 'Mes' ;
$lang['label_dob_year'] = 'Año' ;
$lang['label_gender'] = 'Género' ;
$lang['label_ssn_last_4'] = 'Ultimo 4 SNN' ;
$lang['label_phone'] = 'Teléfono' ;
$lang['label_front_doc'] = 'Doc delante' ;
$lang['label_back_doc'] = 'Doc posterior' ;
$lang['label_routing_number'] = 'Número de ruta' ;
$lang['label_account_number'] = 'Número de cuenta' ;
$lang['label_tax_id'] = 'Identidficación de impuesto' ;
$lang['column_restaurant'] = 'Restaurante' ;
