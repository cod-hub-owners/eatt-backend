<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'estados' ;
$lang['text_heading'] = 'estados' ;
$lang['text_edit_heading'] = 'estado: %s' ;
$lang['text_list'] = 'Lista de Estado' ;
$lang['text_tab_general'] = 'detalles' ;
$lang['text_filter_search'] = 'Buscar nombre o código.' ;
$lang['text_filter_status'] = 'Ver todos los tipos de estados' ;
$lang['text_empty'] = 'No hay estados disponibles.' ;
$lang['text_order'] = 'Orden' ;
$lang['text_reservation'] = 'reservaciones' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_code'] = 'Código' ;
$lang['column_comment'] = 'Estado del comentario' ;
$lang['column_type'] = 'Tipo de estado' ;
$lang['column_notify'] = 'Notificar' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_code'] = 'Código' ;
$lang['label_for'] = 'Estado para' ;
$lang['label_color'] = 'Color' ;
$lang['label_comment'] = 'Comentario' ;
$lang['label_notify'] = 'Notificación de correo electrónico' ;
$lang['label_status'] = 'Estado' ;
$lang['help_notify'] = 'Establezca si se envía un correo electrónico al cliente después de que este estado se establezca en el pedido. Esta opción puede ser anulada desde la página de edición del pedido.' ;

/* End of file statuses_lang.php */
/* Location: ./admin/language/english/statuses_lang.php */