<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Monedas' ;
$lang['text_heading'] = 'Monedas' ;
$lang['text_edit_heading'] = 'moneda: %s' ;
$lang['text_list'] = 'Lista de monedas' ;
$lang['text_tab_general'] = 'detalles' ;
$lang['text_filter_search'] = 'Buscar nombre o código.' ;
$lang['text_filter_status'] = 'Ver todos los estados' ;
$lang['text_empty'] = 'No hay monedas disponibles.' ;
$lang['text_right'] = 'Derecha' ;
$lang['text_left'] = 'Izquierda' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_code'] = 'Código' ;
$lang['column_country'] = 'País' ;
$lang['column_symbol'] = 'Símbolo' ;
$lang['column_rate'] = 'Tarifa' ;
$lang['column_status'] = 'Estado' ;
$lang['button_update_rate'] = '<i class="fa fa-refresh"></i>&nbsp;&nbsp;Update Rates' ;
$lang['label_title'] = 'Título' ;
$lang['label_code'] = 'Código' ;
$lang['label_country'] = 'País' ;
$lang['label_symbol'] = 'Símbolo' ;
$lang['label_symbol_position'] = 'Posición del símbolo' ;
$lang['label_rate'] = 'Tarifa' ;
$lang['label_thousand_sign'] = 'Signo de mil' ;
$lang['label_decimal_sign'] = 'Signo decimal' ;
$lang['label_decimal_position'] = 'Decimal' ;
$lang['label_status'] = 'Estado' ;
$lang['help_iso'] = 'Más información sobre <a target="_blank" href="http://en.wikipedia.org/wiki/ISO_3166-1">ISO Alpha 2, 3, numérico</a>' ;

/* End of file currencies_lang.php */
/* Location: ./admin/language/english/currencies_lang.php */