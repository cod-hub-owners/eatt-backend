<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'idiomas' ;
$lang['text_heading'] = 'idiomas' ;
$lang['text_edit_heading'] = 'Language: %s' ;
$lang['text_list'] = 'Lista de idiomas' ;
$lang['text_tab_general'] = 'detalles' ;
$lang['text_tab_files'] = 'archivos' ;
$lang['text_tab_edit_file'] = 'Editar el archivo de idioma' ;
$lang['text_filter_search'] = 'Buscar nombre.' ;
$lang['text_filter_status'] = 'Ver todos los estados' ;
$lang['text_empty'] = 'No hay idiomas disponibles.' ;
$lang['text_files'] = 'archivos' ;
$lang['text_cloned'] = 'clonado' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_code'] = 'Código' ;
$lang['column_image'] = 'Icono' ;
$lang['column_status'] = 'Estado' ;
$lang['column_variable'] = 'Variable del idioma' ;
$lang['column_language'] = 'Traducción de idiomas' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_code'] = 'Código del idioma' ;
$lang['label_image'] = 'Icono' ;
$lang['label_idiom'] = 'Idioma' ;
$lang['label_clone'] = 'Lenguaje de clonación' ;
$lang['label_language'] = 'Lenguaje para clonar' ;
$lang['label_can_delete'] = 'se puede eliminar' ;
$lang['label_status'] = 'Estado' ;
$lang['help_idiom'] = 'El lenguaje del idioma, debe ser el mismo que el nombre del directorio del lenguaje.' ;
$lang['help_language'] = 'Prefijo de la url del lenguaje' ;
$lang['alert_save_changes'] = 'Sus cambios se perderán si no los guarda antes de editar otro archivo de lenguaje.' ;
$lang['alert_warning_file'] = 'Se ha producido un error, el archivo de idioma no era %s' ;
$lang['alert_caution_edit'] = '<b>ATENCIÓN!</b> Usted puede perder sus modificaciones en este idioma cuando se actualiza EatT.<br> Clonar el idioma por defecto [Inglés] de la página de agregar nuevo idioma para hacer cambios.' ;
$lang['error_invalid_idiom'] = 'El nombre del idioma especificado no existe en la carpeta de idioma principal o de administración.' ;

/* End of file languages_lang.php */
/* Location: ./admin/language/english/languages_lang.php */