<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_heading'] = 'Plantillas de correo' ;
$lang['text_edit_heading'] = 'Plantillas de correo: %s' ;
$lang['text_list'] = 'Lista de plantillas de correo' ;
$lang['text_tab_general'] = 'General' ;
$lang['text_tab_templates'] = 'Plantillas' ;
$lang['text_empty'] = 'No hay plantillas de correo disponibles.' ;
$lang['text_no_template'] = 'No hay ningún mensaje de plantilla disponible.' ;
$lang['text_variables'] = 'Variables' ;
$lang['text_registration'] = 'correo electrónico de registro de cliente' ;
$lang['text_password_reset'] = 'restablecimiento de contraseña del correo electronico del cliente' ;
$lang['text_password_reset_app'] = 'restablecimiento de contraseña del correo electrónico de los clientes (APP)' ;
$lang['text_password_reset_alert'] = 'restablecimiento de contraseña del correo electronico del admin' ;
$lang['text_order'] = 'Correo electrónico del pedido al cliente' ;
$lang['text_reservation'] = 'correo electrónico de reserva al cliente' ;
$lang['text_reservation_location'] = 'correo electrónico de reserva al restaurante' ;
$lang['text_guest_reservation'] = 'correo electrónico de reserva a los clientes invitados' ;
$lang['text_internal'] = 'Mensaje interno' ;
$lang['text_contact'] = 'Correo electrónico de contacto con el administrador' ;
$lang['text_registration_alert'] = 'Correo electrónico de alerta de registro al administrador' ;
$lang['text_order_alert'] = 'Correo electrónico de alerta de pedido al administrador' ;
$lang['text_reservation_alert'] = 'Alerta de reserva a la administración' ;
$lang['text_order_update'] = 'Correo electrónico de actualización del estado del pedido al cliente' ;
$lang['text_reservation_update'] = 'Correo electrónico de actualización del estado de la reservación al administrador' ;
$lang['text_is_default'] = 'Defecto' ;
$lang['text_english_option'] = 'Inglés' ;
$lang['text_set_default'] = 'Establecer el defecto' ;
$lang['button_modules'] = 'módulos' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_title'] = 'Título' ;
$lang['column_date_added'] = 'Fecha de adición' ;
$lang['column_date_updated'] = 'Fecha de actualización' ;
$lang['column_status'] = 'Estado' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_clone'] = 'Plantilla de clonación' ;
$lang['label_language'] = 'Idioma' ;
$lang['label_status'] = 'Estado' ;
$lang['label_code'] = 'Código' ;
$lang['label_subject'] = 'Tema' ;
$lang['label_body'] = 'Cuerpo' ;
$lang['alert_set_default'] = 'La plantilla de correo se ha establecido correctamente de forma predeterminada.' ;
$lang['alert_template_missing'] = 'Las plantillas se añadirán después de que usted guarde esta plantilla de correo.' ;
$lang['alert_caution_edit'] = '<B> ¡CUIDADO! </ B> Es posible que pierda sus modificaciones a esta plantilla de correo electrónico cuando actualicé EatT. <br> Clone la plantilla predeterminada desde la página de adición de una nueva plantilla de correo para realizar cambios.' ;

/* End of file mail_templates_lang.php */
/* Location: ./admin/language/english/mail_templates_lang.php */