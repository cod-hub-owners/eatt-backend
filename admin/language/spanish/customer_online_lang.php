<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'clinete en linea' ;
$lang['text_heading'] = 'cliente en linea' ;
$lang['text_all_heading'] = 'cliente en linea: todos' ;
$lang['text_online'] = 'En línea' ;
$lang['text_all'] = 'Todos' ;
$lang['text_filter_search'] = 'Buscar ip, cliente o navegador.' ;
$lang['text_filter_access'] = 'Ver todos los accesos' ;
$lang['text_filter_date'] = 'Ver todas las fechas' ;
$lang['text_empty'] = 'No hay cliente en línea.' ;
$lang['text_empty_report'] = 'No hay ningún informe de cliente en línea disponible.' ;
$lang['text_guest'] = 'Invitado' ;
$lang['text_private'] = 'Privado' ;
$lang['text_browser'] = 'Navegador' ;
$lang['text_mobile'] = 'Móvil' ;
$lang['text_robot'] = 'Robot' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['column_ip'] = 'IP' ;
$lang['column_customer'] = 'Cliente' ;
$lang['column_access'] = 'Acceso' ;
$lang['column_browser'] = 'Navegador' ;
$lang['column_agent'] = 'Agente de usuario' ;
$lang['column_request_uri'] = 'URL del último pedido' ;
$lang['column_referrer_url'] = 'Último URL de referencia' ;
$lang['column_last_activity'] = 'Última actividad' ;

/* End of file customer_online_lang.php */
/* Location: ./admin/language/english/customer_online_lang.php */