<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Opciones de menú' ;
$lang['text_heading'] = 'Opciones de menú' ;
$lang['text_edit_heading'] = 'opciones de menú: %s' ;
$lang['text_list'] = 'Lista de opciones de menú' ;
$lang['text_tab_general'] = 'detalles' ;
$lang['text_tab_values'] = 'Valores de opción' ;
$lang['text_no_match'] = 'No se encontraron coincidencias' ;
$lang['text_empty'] = 'No hay opciones de menú disponibles.' ;
$lang['text_filter_search'] = 'Buscar nombre o precio.' ;
$lang['text_filter_display_type'] = 'Ver todos los tipos de visualización' ;
$lang['text_select'] = 'Seleccione' ;
$lang['text_radio'] = 'Radio' ;
$lang['text_checkbox'] = 'Casilla de verificación' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_priority'] = 'Prioridad' ;
$lang['column_display_type'] = 'Tipo de visualización' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['label_option_name'] = 'Nombre de la opción' ;
$lang['label_option_name_arabic'] = 'Nombre de la opción en árabe' ;
$lang['label_priority'] = 'Prioridad' ;
$lang['label_display_type'] = 'Tipo de visualización' ;
$lang['label_option_value_arabic'] = 'Valor de la opción en árabe ' ;
$lang['label_option_value'] = 'Valor de la opción' ;
$lang['label_option_price'] = 'Precio de la opción' ;
$lang['label_option_id'] = 'Opcion de identificacion ' ;


/* End of file menu_options_lang.php */
/* Location: ./admin/language/english/menu_options_lang.php */