<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'páginas' ;
$lang['text_heading'] = 'páginas' ;
$lang['text_edit_heading'] = 'Página: %s' ;
$lang['text_list'] = 'Lista de páginas del sitio ' ;
$lang['text_tab_general'] = 'General' ;
$lang['text_tab_content'] = 'Contenido' ;
$lang['text_tab_content_arabic'] = 'contenido árabe' ;
$lang['text_empty'] = 'No hay páginas disponibles.' ;
$lang['text_filter_search'] = 'Buscar nombre.' ;
$lang['text_filter_status'] = 'Ver todos los estados' ;
$lang['text_header'] = 'Encabezado' ;
$lang['text_side_bar'] = 'Barra lateral' ;
$lang['text_footer'] = 'Pie de página' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_preview'] = 'Avance' ;
$lang['column_language'] = 'Idioma' ;
$lang['column_date_updated'] = 'Fecha actualizada' ;
$lang['column_status'] = 'Estado' ;
$lang['label_title'] = 'Título' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_heading'] = 'Encabezamiento' ;
$lang['label_permalink_id'] = 'ID de permalink' ;
$lang['label_permalink_slug'] = 'Indicación de permalink' ;
$lang['label_content'] = 'Contenido' ;
$lang['label_language'] = 'Idioma' ;
$lang['label_meta_description'] = 'Descripción de la Meta' ;
$lang['label_meta_keywords'] = 'Meta Palabras Clave' ;
$lang['label_layout'] = 'Diseño' ;
$lang['label_navigation'] = 'Navegación' ;
$lang['label_status'] = 'Estado' ;
$lang['help_permalink'] = 'Utilice SOLAMENTE caracteres alfanuméricos en minúsculas, subrayados o guiones y asegúrese de que es único GLOBALMENTE.' ;
$lang['help_navigation'] = 'Seleccione dónde desea que se encuentre el enlace de esta página' ;

/* End of file pages_lang.php */
/* Location: ./admin/language/english/pages_lang.php */