<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Países' ;
$lang['text_heading'] = 'Países' ;
$lang['text_edit_heading'] = 'País: %s' ;
$lang['text_list'] = 'Lista de países' ;
$lang['text_tab_general'] = 'detalles' ;
$lang['text_filter_search'] = 'Nombre o código de búsqueda.' ;
$lang['text_filter_status'] = 'Ver todos los estados' ;
$lang['text_empty'] = 'No hay países disponibles.' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_iso_code2'] = 'Código ISO 2' ;
$lang['column_iso_code3'] = 'Código ISO 3' ;
$lang['column_status'] = 'Estado' ;
$lang['label_name'] = 'País' ;
$lang['label_flag'] = 'Bandera' ;
$lang['label_format'] = 'Formato' ;
$lang['label_iso_code2'] = 'Código ISO 2' ;
$lang['label_iso_code3'] = 'Código ISO 3' ;
$lang['label_status'] = 'Estado' ;
$lang['help_format'] = 'Address 1 = {address_1}<br />Address 2 = {address_2}<br />City = {city}<br />Postcode = {postcode}<br />State = {state}<br />Country = {country}' ;
$lang['help_iso'] = 'Learn more about <a target="_blank" href="http://en.wikipedia.org/wiki/ISO_3166-1">ISO Alpha 2 & 3</a>' ;

/* End of file countries_lang.php */
/* Location: ./admin/language/english/countries_lang.php */