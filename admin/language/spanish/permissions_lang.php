<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'permisos' ;
$lang['text_heading'] = 'permisos' ;
$lang['text_edit_heading'] = 'Permiso: %s' ;
$lang['text_list'] = 'Lista de permisos del personal' ;
$lang['text_tab_general'] = 'Detalles de permiso' ;
$lang['text_empty'] = 'No hay permisos disponibles.' ;
$lang['text_filter_search'] = 'Buscar nombre del permiso.' ;
$lang['text_filter_status'] = 'Ver todos los estados' ;
$lang['text_access'] = 'Acceso' ;
$lang['text_manage'] = 'Gestionar' ;
$lang['text_add'] = 'Añadir' ;
$lang['text_delete'] = 'Eliminar' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_actions'] = 'acciones disponibles' ;
$lang['column_description'] = 'Descripción' ;
$lang['column_status'] = 'Estado' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_description'] = 'Descripción' ;
$lang['label_status'] = 'Estado' ;
$lang['label_action'] = 'Acción' ;
$lang['help_name'] = 'El nombre de los permisos se compone de (Dominio.Contexto):<br />Dominio - Típicamente el nombre de dominio de la aplicación (por ejemplo, Admin, Principal, Módulo).<br />Contexto - El nombre de la clase del controlador (por ejemplo, Menús, Pedidos, Ubicaciones o Ajustes).' ;
$lang['help_action'] = 'La acción permitida (Acceder, Gestionar, Añadir, Borrar)' ;
/* End of file permissions_lang.php */
/* Location: ./admin/language/english/permissions_lang.php */