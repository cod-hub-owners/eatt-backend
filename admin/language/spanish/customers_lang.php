<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Clientes' ;
$lang['text_heading'] = 'Clientes' ;
$lang['text_edit_heading'] = 'Cliente: %s' ;
$lang['text_list'] = 'Lista de clientes' ;
$lang['text_tab_general'] = 'Cliente' ;
$lang['text_tab_address'] = 'Dirección' ;
$lang['text_filter_search'] = 'Buscar nombre del cliente o de correo electrónico.' ;
$lang['text_filter_status'] = 'Ver todos los estados' ;
$lang['text_filter_date'] = 'Ver todas las fechas' ;
$lang['text_empty'] = 'No hay clientes disponibles.' ;
$lang['text_no_match'] = 'No se encontraron coincidencias' ;
$lang['text_subscribe'] = 'Suscribir' ;
$lang['text_un_subscribe'] = 'Anular la suscripción' ;
$lang['text_login_as_customer'] = 'Ingresar como cliente' ;
$lang['column_first_name'] = 'Nombre de pila' ;
$lang['column_last_name'] = 'Apellido' ;
$lang['column_email'] = 'Direccion de correo electronico' ;
$lang['column_telephone'] = 'Teléfono' ;
$lang['column_date_added'] = 'Fecha de registro' ;
$lang['column_status'] = 'Estado' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['label_first_name'] = 'Nombre de pila' ;
$lang['label_last_name'] = 'Apellido' ;
$lang['label_email'] = 'Email' ;
$lang['label_password'] = 'Contraseña' ;
$lang['label_confirm_password'] = 'Confirmar contraseña' ;
$lang['label_telephone'] = 'Teléfono' ;
$lang['label_security_question'] = 'Pregunta de Seguridad' ;
$lang['label_security_answer'] = 'Respuesta de seguridad' ;
$lang['label_newsletter'] = 'Boletin informativo' ;
$lang['label_customer_group'] = 'grupo de clientes' ;
$lang['label_status'] = 'Estado' ;
$lang['label_address_1'] = 'Dirección 1' ;
$lang['label_address_2'] = 'Dirección 2' ;
$lang['label_city'] = 'Ciudad' ;
$lang['label_state'] = 'Estado' ;
$lang['label_postcode'] = 'Código postal' ;
$lang['label_country'] = 'País' ;
$lang['label_currency'] = 'Moneda' ;
$lang['label_language'] = 'Idioma' ;
$lang['help_password'] = 'Dejar en blanco para no modificar la contraseña' ;
$lang['alert_login_restricted'] = 'Advertencia: No tiene los permisos para <b> acceder a la cuente de cliente </b>, por favor contactese con el administrador del sistema' ;
$lang['label_vip'] = 'Cliente VIP' ;