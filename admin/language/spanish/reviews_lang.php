<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Comentarios' ;
$lang['text_heading'] = 'Comentarios' ;
$lang['text_edit_heading'] = 'comentario: %s' ;
$lang['text_list'] = 'Lista de revisión' ;
$lang['text_tab_general'] = 'Detalles de la revisión' ;
$lang['text_empty'] = 'No hay comentarios disponibles.' ;
$lang['text_filter_search'] = 'Busque el autor, el restaurante, la identificación del pedido o la clasificación.' ;
$lang['text_filter_location'] = 'Ver todas las tiendas' ;
$lang['text_filter_status'] = 'Ver todos los estados' ;
$lang['text_filter_date'] = 'Ver todas las fechas' ;
$lang['text_pending_review'] = 'Revisión pendiente' ;
$lang['text_approved'] = 'Aprobado' ;
$lang['text_order'] = 'Orden' ;
$lang['text_reservation'] = 'reservación' ;
$lang['column_location'] = 'restaurante' ;
$lang['column_author'] = 'Autor' ;
$lang['column_sale_id'] = 'identificación de venta' ;
$lang['column_sale_type'] = 'Tipo de venta' ;
$lang['column_status'] = 'Estado' ;
$lang['column_date_added'] = 'Fecha Agregada' ;
$lang['label_sale_type'] = 'Tipo de venta' ;
$lang['label_sale_id'] = 'identificación de venta ' ;
$lang['label_location'] = 'restaurante' ;
$lang['label_customer'] = 'Cliente' ;
$lang['label_author'] = 'Autor' ;
$lang['label_rating'] = 'Clasificación' ;
$lang['label_quality'] = 'calificacion de calidad' ;
$lang['label_delivery'] = 'calificacion de entrega' ;
$lang['label_service'] = 'Calificación del servicio' ;
$lang['label_text'] = 'Texto del comentario' ;
$lang['label_status'] = 'Estado de revisión' ;
$lang['error_not_found_in_order'] = 'Los %s introducidos no se encuentran en los pedidos' ;
$lang['error_not_found_in_reservation'] = 'Los %s introducidos no se encuentran en las reservas' ;
$lang['error_not_found'] = 'Los %s introducidos no se encuentran' ;
$lang['help_type'] = 'ya sea para restar un importe fijo o un porcentaje del total de la orden.' ;
$lang['help_redemption'] = 'El número total de veces que esta revisión puede ser redimida. Introduzca 0 para canjes ilimitados.' ;
$lang['help_customer_redemption'] = 'El número de veces que un cliente específico puede canjear esta revisión. Introduzca 0 para canjes ilimitados.' ;


/* End of file reviews_lang.php */
/* Location: ./admin/language/english/reviews_lang.php */