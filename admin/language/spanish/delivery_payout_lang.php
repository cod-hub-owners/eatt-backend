<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Lista de socios de entrega' ;
$lang['text_heading'] = 'Pagos de entrega' ;
$lang['partner_name'] = 'Nombre del socio' ;
$lang['rejected'] = 'Rechazado' ;
$lang['completed'] = 'Completado ' ;
$lang['select'] = 'Seleccione' ;
$lang['view_all'] = 'Ver todo' ;
$lang['partner_email'] = 'Compañero de correo electrónico' ;
$lang['wallet'] = 'Cantidad de la cartera' ;
$lang['request'] = 'Monto de la solicitud' ;
$lang['actions'] = 'Acción' ;
$lang['completed_history'] = 'Historial completado/rechazado' ;
$lang['pending_history'] = 'solicitudes pendientes' ;
$lang['request_date'] = 'Fecha de solicitud' ;
$lang['request_amount'] = 'Cantidad de la solicitud' ;
$lang['invoice_no'] = 'Factura no' ;
$lang['status'] = 'Estado' ;