<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Pagos a EatT' ;
$lang['text_heading'] = 'Pagos a EatT' ;
$lang['text_edit_heading'] = 'restaurante: %s' ;
$lang['payments_to_spotneat'] = 'Pagos a EatT' ;
$lang['column_staff'] = 'nombre del cliente' ;
$lang['column_location'] = 'Restaurante / cafetería' ;
$lang['column_percentage'] = 'porcentaje de comisión ' ;
$lang['column_amount'] = 'cantidad total de ventas' ;
$lang['column_Commission'] = 'Importe de la Comisión' ;
$lang['column_admin_Commission'] = 'cantidad de mesas en reservas' ;
$lang['column_date'] = 'Fecha' ;
$lang['column_reservation'] = 'ID de reserva' ;