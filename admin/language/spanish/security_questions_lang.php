<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Preguntas de seguridad' ;
$lang['text_heading'] = 'Preguntas de seguridad' ;
$lang['text_list'] = 'lista de preguntas de seguridad ' ;
$lang['text_empty'] = 'No hay preguntas de seguridad, por favor agregue.' ;
$lang['column_question'] = 'Pregunta' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['column_language'] = 'Idioma' ;
$lang['text_english'] = 'Inglés' ;
$lang['text_arabic'] = 'Arábica' ;
$lang['label_question'] = 'identificacion de la pregunta' ;
$lang['label_answer'] = 'Pregunta respuesta' ;

/* End of file security_questions_lang.php */
/* Location: ./admin/language/english/security_questions_lang.php */