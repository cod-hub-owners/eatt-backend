<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'categorías' ;
$lang['text_heading'] = 'categorías' ;
$lang['text_edit_heading'] = 'Categoria: %s' ;
$lang['text_list'] = 'Lista de categoría' ;
$lang['text_tab_general'] = 'Detalles de categoria' ;
$lang['text_filter_search'] = 'Buscar nombre de la categoría, descripción o condición' ;
$lang['text_filter_status'] = 'Ver todos los estados' ;
$lang['text_empty'] = 'No hay categorías disponibles.' ;
$lang['column_name'] = 'Nombre' ;
$lang['column_description'] = 'Descripción' ;
$lang['column_parent'] = 'Padre' ;
$lang['column_priority'] = 'Prioridad' ;
$lang['column_status'] = 'Estado' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['label_name'] = 'Nombre' ;
$lang['label_description'] = 'Descripción' ;
$lang['label_name_ar'] = 'Nombre árabe' ;
$lang['label_description_ar'] = 'Descripción árabe' ;
$lang['label_permalink_id'] = 'ID de Permalink' ;
$lang['label_permalink_slug'] = 'indicacion de Permalink ' ;
$lang['label_parent'] = 'Padre' ;
$lang['label_image'] = 'Imagen' ;
$lang['label_priority'] = 'Prioridad' ;
$lang['label_status'] = 'Estado' ;
$lang['label_alcohol_status'] = 'Alcohol' ;
$lang['help_photo'] = 'Seleccione un archivo para actualizar la imagen de la categoría, de lo contrario, déjelo en blanco.' ;
$lang['help_permalink'] = 'Utilice SOLAMENTE caracteres alfanuméricos en minúsculas, subrayados o guiones y asegúrese de que es único GLOBALMENTE.' ;


/* End of file categories_lang.php */
/* Location: ./admin/language/english/categories_lang.php */