<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'Iniciar sesión' ;
$lang['text_password_reset_title'] = 'Restablecer la contraseña' ;
$lang['text_forgot_password'] = '¿Olvidaste tu contraseña?' ;
$lang['text_reset_password_title'] = 'Restablecer su contraseña' ;
$lang['text_back_to_login'] = '<- volver al inicio' ;
$lang['button_login'] = 'Iniciar sesión' ;
$lang['button_reset_password'] = 'Obtener nueva contraseña' ;
$lang['label_username'] = 'Nombre de usuario' ;
$lang['label_password'] = 'Contraseña' ;
$lang['label_username_email'] = 'Nombre de usuario o correo electrónico' ;
$lang['alert_username_not_found'] = 'ID de correo electrónico o contraseña! no válido' ;
$lang['error_no_user_found'] = 'No hay ningún usuario registrado con esa dirección de correo electrónico.' ;
$lang['alert_success_reset'] = 'Una nueva contraseña ha sido enviada a su correo electrónico.' ;
$lang['alert_success_logout'] = 'Ha cerrado sesión ' ;
$lang['alert_email_not_sent'] = 'El correo electrónico no se pudo enviar. Posible motivo: su anfitrión puede haber desactivado la función correo ().' ;
$lang['login_label'] = 'Panel de inicio de sesión de administrador/cliente' ;

/* End of file login_lang.php */
/* Location: ./admin/language/english/login_lang.php */