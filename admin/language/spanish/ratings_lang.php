<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'calificaciones' ;
$lang['text_heading'] = 'calificaciones' ;
$lang['text_list'] = 'Lista de Clasificación' ;
$lang['text_empty'] = 'No hay calificación, por favor agregue.' ;
$lang['column_name'] = 'Nombre' ;
$lang['label_name'] = 'Nombre' ;

/* End of file ratings_lang.php */
/* Location: ./admin/language/english/ratings_lang.php */