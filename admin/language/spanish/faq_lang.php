<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] = 'preguntas frecuentes' ;
$lang['text_heading'] = 'preguntas frecuentes' ;
$lang['text_list'] = 'lista de preguntas frecuentes' ;
$lang['text_empty'] = 'No hay preguntas de seguridad, por favor agregue.' ;
$lang['column_question_english'] = 'Inglés' ;
$lang['column_question_arabic'] = 'Española' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['column_language'] = 'Idioma' ;
$lang['text_english'] = 'Inglés' ;
$lang['text_arabic'] = 'Española' ;
$lang['label_question'] = 'identificacion de la pregunta' ;
$lang['label_answer'] = 'pregunta y respuesta ' ;

/* End of file security_questions_lang.php */
/* Location: ./admin/language/english/security_questions_lang.php */