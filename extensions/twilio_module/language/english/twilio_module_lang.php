<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] 		    = 'Twilio SMS Gateway';
$lang['text_tab_general'] 	    = 'General';
$lang['text_sandbox'] 	        = 'Sandbox';
$lang['text_go_live'] 	        = 'Go Live';
$lang['text_sale'] 	            = 'SALE';
$lang['text_authorization']     = 'AUTHORIZATION';

$lang['label_title'] 	        = 'Title';
$lang['label_account_sid'] 	    = 'Accout SId';
$lang['label_api_version'] 	    = 'API Version';
$lang['label_auth_token']    	= 'Auth Token';
$lang['label_api_mode'] 	    = 'Mode';
$lang['label_api_action'] 	    = 'Payment Action';
$lang['label_account_number'] 	= 'Account Number';
$lang['label_status'] 	        = 'Status';

$lang['text_tab_general'] 	    = 'General';
$lang['text_tab_content'] 	    = 'Content';

$lang['help_account_number'] 		= 'Give Your API Account From Mobile Number';

/* End of file paypal_express_lang.php */
/* Location: ./extensions/cod/language/english/paypal_express_lang.php */

$lang['text_title'] 		            = 'Mail Templates';
$lang['text_heading'] 		            = 'Mail Templates';
$lang['text_edit_heading'] 		        = 'Mail Template: %s';
$lang['text_list'] 		                = 'Mail Template List';
$lang['text_tab_general'] 		        = 'General';
$lang['text_tab_templates'] 		    = 'Templates';
$lang['text_empty'] 		            = 'There are no mail templates available.';
$lang['text_no_template'] 		        = 'There is no template message available.';
$lang['text_variables'] 		        = 'Variables';
$lang['text_registration'] 		        = 'Registration email to customer';
$lang['text_password_reset'] 		    = 'Password reset email to customer';
$lang['text_password_reset_alert'] 	    = 'Password reset email to admin';
$lang['text_order'] 		            = 'Order email to customer';
$lang['text_reservation_english'] 		= 'Booking SMS to customer - English';
$lang['text_reservation_arabic'] 		= 'Booking SMS to customer - Arabic';
$lang['text_reservation_location_english'] 		= 'Booking SMS to Restaurant - English';
$lang['text_reservation_location_arabic'] 		= 'Booking SMS to Restaurant - Arabic';
$lang['text_register_english'] 		    = 'Welcome SMS to customer on register - English';
$lang['text_register_arabic'] 		    = 'Welcome SMS to customer on register - Arabic';
$lang['text_resend_arabic'] 		    = 'Resend 4 Digit OTP - Arabic';
$lang['text_resend_english'] 		    = 'Resend 4 Digit OTP - English';
$lang['text_internal'] 		            = 'Internal Message';
$lang['text_contact'] 		            = 'Contact email to admin';
$lang['text_registration_alert'] 	    = 'Registration alert email to admin';
$lang['text_order_alert'] 		        = 'Order alert email to admin';
$lang['text_reservation_alert'] 		= 'Reservation alert to admin';
$lang['text_order_update'] 		        = 'Order status update email to customer';
$lang['text_reservation_update'] 		= 'Reservation status update email to admin';
$lang['text_is_default'] 		        = 'Default';
$lang['text_english_option'] 		    = 'English';
$lang['text_is_default'] 		        = 'Default';
$lang['text_set_default'] 		        = 'Set Default';


$lang['button_modules'] 		        = 'Modules';

$lang['column_name'] 		            = 'Name';
$lang['column_title'] 		            = 'Title';
$lang['column_date_added'] 		        = 'Date Added';
$lang['column_date_updated'] 		    = 'Date Updated';
$lang['column_status'] 		            = 'Status';

$lang['label_name'] 		            = 'Name';
$lang['label_clone'] 		            = 'Clone Template';
$lang['label_language'] 		        = 'Language';
$lang['label_status'] 		            = 'Status';
$lang['label_code'] 		            = 'Code';
$lang['label_subject'] 		            = 'Subject';
$lang['label_body'] 		            = 'Body';

$lang['alert_set_default'] 		        = 'Mail Template set as default successfully.';
$lang['alert_template_missing'] 	    = 'Templates will be added after you save this mail template.';
$lang['alert_caution_edit'] 	        = '<b>CAUTION!</b> You may lose your modifications to this email template when you update SpotnEat.<br> Clone the default template from the add new mail template page to make changes.';

$lang['text_reservation_update_english']= 'Booking status update SMS to customer - English';
$lang['text_reservation_update_arabic']= 'Booking status update SMS to customer - Arabic';
$lang['text_reservation_location_update_english']= 'Booking status update SMS to Restaurant - English';
$lang['text_reservation_location_update_arabic']= 'Booking status update SMS to Restaurant - Arabic';