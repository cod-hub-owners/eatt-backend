<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] = 'banderas' ;
$lang['text_tab_general'] = 'General' ;
$lang['text_edit_banner'] = 'Editar Banner' ;
$lang['column_banner'] = 'Bandera' ;
$lang['column_dimension'] = 'Dimensión (W x H)' ;
$lang['column_layout_partial'] = 'Disposición - Área parcial' ;
$lang['column_status'] = 'Estado' ;
$lang['label_banner'] = 'Bandera' ;
$lang['label_dimension'] = 'Dimensión (W x H)' ;
$lang['label_width'] = 'Anchura' ;
$lang['label_height'] = 'Altura' ;
$lang['label_status'] = 'Estado' ;
$lang['label_layout_partial'] = 'Disposición - Área parcial' ;
$lang['button_banners'] = 'Añadir Nuevo Banner' ;
$lang['help_layouts'] = 'Elegir un diseño para agregar uno o más de banner (s).' ;
$lang['alert_set_banners'] = 'Primero debe agregar el módulo de banderas a uno o más diseños' ;

/* End of file banners_lang.php */
/* Location: ./extensions/banners_lang/language/english/banners_lang.php */