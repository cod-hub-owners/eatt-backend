<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] = 'Diapositivas' ;
$lang['text_tab_general'] = 'General' ;
$lang['text_tab_slides'] = 'diapositivas' ;
$lang['label_display'] = 'Monitor' ;
$lang['label_dimension'] = 'Dimensión' ;
$lang['label_dimension_h'] = 'La dimensión de altura' ;
$lang['label_dimension_w'] = 'dimensión Ancho' ;
$lang['label_effect'] = 'efectos' ;
$lang['label_speed'] = 'Velocidad de transición' ;
$lang['label_slide_name'] = 'Nombre de diapositivas' ;
$lang['label_slide_image'] = 'imagen de diapositiva' ;
$lang['label_slide_caption'] = 'Leyenda de diapositivas' ;
$lang['help_dimension'] = '(W x H)' ;

/* End of file slideshow.php */
/* Location: ./extensions/slideshow/language/english/slideshow_lang.php */