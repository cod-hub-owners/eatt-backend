<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] = 'Boletin informativo' ;
$lang['text_tab_general'] = 'General' ;
$lang['text_subscribe'] = 'Suscríbete a nuestro boletín' ;
$lang['label_status'] = 'Estado:' ;
$lang['label_email'] = 'Email' ;
$lang['alert_success_subscribed'] = 'Gracias' ;
$lang['alert_success_existing'] = 'Gracias' ;

/* End of file newsletter_lang.php */
/* Location: ./extensions/newsletter/language/english/newsletter_lang.php */