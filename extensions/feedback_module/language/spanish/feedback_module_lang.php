<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] = 'Realimentación' ;
$lang['text_tab_general'] = 'General' ;
$lang['text_sandbox'] = 'Salvadera' ;
$lang['text_go_live'] = 'Ir a vivir' ;
$lang['text_sale'] = 'REBAJA' ;
$lang['text_authorization'] = 'AUTORIZACIÓN' ;
$lang['label_title'] = 'Título' ;
$lang['label_account_sid'] = 'accout SId' ;
$lang['label_api_version'] = 'Versión API' ;
$lang['label_auth_token'] = 'auth Token' ;
$lang['label_api_mode'] = 'Modo' ;
$lang['label_api_action'] = 'Acción pago' ;
$lang['label_account_number'] = 'Número de cuenta' ;
$lang['label_status'] = 'Estado' ;
$lang['help_account_number'] = 'Dé a su cuenta de la API De Número Móvil' ;

/* End of file paypal_express_lang.php */
/* Location: ./extensions/cod/language/english/paypal_express_lang.php */