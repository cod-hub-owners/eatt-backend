<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] 		    = 'Feedback';
$lang['text_tab_general'] 	    = 'General';
$lang['text_sandbox'] 	        = 'Sandbox';
$lang['text_go_live'] 	        = 'Go Live';
$lang['text_sale'] 	            = 'SALE';
$lang['text_authorization']     = 'AUTHORIZATION';

$lang['label_title'] 	        = 'Title';
$lang['label_account_sid'] 	    = 'Accout SId';
$lang['label_api_version'] 	    = 'API Version';
$lang['label_auth_token']    	= 'Auth Token';
$lang['label_api_mode'] 	    = 'Mode';
$lang['label_api_action'] 	    = 'Payment Action';
$lang['label_account_number'] 	= 'Account Number';
$lang['label_status'] 	        = 'Status';

$lang['text_tab_general'] 	    = 'General';

$lang['help_account_number'] 		= 'Give Your API Account From Mobile Number';

/* End of file paypal_express_lang.php */
/* Location: ./extensions/cod/language/english/paypal_express_lang.php */