<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] = 'Reserva' ;
$lang['text_tab_general'] = 'General' ;
$lang['text_heading'] = 'Encontrar una mesa' ;
$lang['text_time_heading'] = 'Seleccione Hora' ;
$lang['text_reservation'] = 'Mi reserva' ;
$lang['text_heading_success'] = 'Reserva confirmada' ;
$lang['text_no_table'] = 'No hay mesas disponibles en el restaurante local.' ;
$lang['text_find_msg'] = 'Utilice el siguiente formulario para encontrar una mesa para la reserva' ;
$lang['text_time_msg'] = 'los intervalos de tiempo de reserva disponibles en% s para% s invitados' ;
$lang['text_no_time_slot'] = '<span class="text-danger">No reservation time slot available' ;
$lang['text_location_closed'] = 'Lo siento' ;
$lang['label_status'] = 'Estado' ;
$lang['label_location'] = 'Restaurante' ;
$lang['label_guest_num'] = 'Número de invitados' ;
$lang['label_table_nos'] = 'Número de tablas' ;
$lang['label_date'] = 'Fecha' ;
$lang['label_time'] = 'Hora' ;
$lang['label_select'] = '- Por favor seleccione -' ;
$lang['total_tables'] = 'Tablas totales' ;
$lang['button_find_table'] = 'Encuentra la tabla' ;
$lang['button_select_time'] = 'Seleccione Hora' ;
$lang['button_back'] = 'atrás' ;
$lang['button_reset'] = 'Reiniciar' ;
$lang['error_invalid_date'] = 'se pasa la fecha / hora seleccionada' ;
$lang['error_invalid_time'] = 'El tiempo debe ser entre el tiempo de apertura del restaurante!' ;
$lang['alert_reservation_disabled'] = '<p class="alert-danger">Table reservation has been disabled' ;
$lang['alert_no_table_available'] = '<p class="alert-danger">No tables available at the selected location.</p>' ;
$lang['alert_fully_booked'] = '<p class="alert-danger">We are fully booked for the selected date and time' ;

/* End of file reservation_module_lang.php */
/* Location: ./extensions/reservation_module/language/english/reservation_module_lang.php */