<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] = 'Pago contra entrega' ;
$lang['text_tab_general'] = 'General' ;
$lang['label_title'] = 'Título' ;
$lang['label_order_total'] = 'mínimo total' ;
$lang['label_order_status'] = 'Estado del pedido' ;
$lang['label_status'] = 'Estado' ;
$lang['label_priority'] = 'Prioridad' ;
$lang['alert_min_total'] = 'total del pedido es inferior al total de la orden de pago mínimo.' ;
$lang['help_order_total'] = 'La cantidad total de la orden debe llegar antes de que esta pasarela de pago se activa' ;
$lang['help_order_status'] = 'Por defecto el estado del pedido, cuando el bacalao es el método de pago' ;
$lang['alert_min_order_total'] = 'You need to spend %s or more to use this payment method</p>' ;

/* End of file cod_lang.php */
/* Location: ./extensions/cod/language/english/cod_lang.php */