<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] = 'Producto destacado Menú' ;
$lang['text_tab_general'] = 'General' ;
$lang['text_subscribe'] = 'Suscríbete a nuestro boletín' ;
$lang['text_featured_menus'] = 'Menú destacado' ;
$lang['column_menu_name'] = 'Nombre del menú' ;
$lang['column_menu_remove'] = 'Eliminar' ;
$lang['label_title'] = 'Título' ;
$lang['label_menus'] = 'menús' ;
$lang['label_limit'] = 'Límite' ;
$lang['label_items_per_row'] = 'Los productos que por fila' ;
$lang['label_dimension'] = 'Dimensión:' ;
$lang['label_dimension_w'] = 'dimensión Ancho' ;
$lang['label_dimension_h'] = 'La dimensión de altura' ;
$lang['label_email'] = 'Email' ;
$lang['help_dimension'] = '(Alto x ancho)' ;
$lang['help_items_per_row'] = 'El número de elementos a mostrar por fila' ;

/* End of file newsletter_lang.php */
/* Location: ./extensions/newsletter/language/english/newsletter_lang.php */