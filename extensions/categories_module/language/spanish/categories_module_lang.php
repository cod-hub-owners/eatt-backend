<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] = 'categorías' ;
$lang['text_tab_general'] = 'General' ;
$lang['text_categories'] = 'categorías' ;
$lang['text_view_categories'] = 'Ver categorías' ;
$lang['text_clear'] = 'claro' ;
$lang['text_show_all'] = 'Mostrar todo' ;
$lang['label_fixed_categories'] = 'Categorías fijos Box' ;
$lang['label_fixed_offset'] = 'Fija arriba Offset' ;
$lang['label_fixed_top_offset'] = 'Fija arriba Offset' ;

/* End of file categories_module_lang.php */
/* Location: ./extensions/categories_module/language/english/categories_module_lang.php */