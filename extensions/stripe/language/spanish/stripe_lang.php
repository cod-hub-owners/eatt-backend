<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] = 'Raya' ;
$lang['text_tab_general'] = 'General' ;
$lang['text_description'] = 'Pagar con tarjeta de crédito a través de la raya' ;
$lang['text_live'] = 'En Vivo' ;
$lang['text_test'] = 'Prueba' ;
$lang['text_cc_number'] = 'Número de tarjeta válida' ;
$lang['text_exp'] = 'MM / YY' ;
$lang['text_cc_cvc'] = 'CVC' ;
$lang['text_stripe_charge_description'] = '%s Charge for %s' ;
$lang['text_payment_status'] = 'Payment %s (%s)' ;
$lang['label_title'] = 'Título' ;
$lang['label_description'] = 'Descripción' ;
$lang['label_transaction_mode'] = 'Modo de transacción' ;
$lang['label_test_secret_key'] = 'Prueba de clave secreta' ;
$lang['label_test_publishable_key'] = 'Prueba publicable clave' ;
$lang['label_live_secret_key'] = 'Vivo clave secreta' ;
$lang['label_live_publishable_key'] = 'Clave en vivo publicable' ;
$lang['label_force_ssl'] = 'fuerza SSL' ;
$lang['label_order_total'] = 'Total del pedido' ;
$lang['label_order_status'] = 'Estado del pedido' ;
$lang['label_priority'] = 'Prioridad' ;
$lang['label_status'] = 'Estado' ;
$lang['label_card_number'] = 'NÚMERO DE TARJETA' ;
$lang['label_card_expiry'] = 'FECHA DE CADUCIDAD' ;
$lang['label_card_cvc'] = 'CV CÓDIGO' ;
$lang['help_order_total'] = 'La cantidad total de la orden debe llegar antes de que esta pasarela de pago se activa' ;
$lang['help_order_status'] = 'Por defecto, cuando el estado del pedido raya es el método de pago seleccionado' ;
$lang['alert_min_order_total'] = 'Usted tiene que invertir% s o más para utilizar este método de pago </ p>' ;
$lang['alert_error_contacting'] = '<p class="alert-danger">There was a problem while contacting the payment gateway. Please try again.</p>' ;

/* End of file stripe_lang.php */
/* Location: ./extensions/stripe/language/english/stripe_lang.php */