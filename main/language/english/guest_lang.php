<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] 			        = 'Guest Booking Details';
$lang['thank_you_reservation'] = 'Thank you For your reservations. Here is the details for your Reservation,';
$lang['name'] = 'Name';
$lang['email'] = 'Email';
$lang['phone'] = 'Phone';
$lang['reservation_id'] = 'Reservation ID';
$lang['reservation_date'] = 'Reservation Date';
$lang['reservation_time'] = 'Reservation Time';
$lang['guests'] = 'Guests';
$lang['booking_price'] = 'Booking Price';
$lang['table_booking_tax'] = 'Table Booking Tax';
$lang['order_price'] = 'Order Price';
$lang['total_amount'] = 'Total Amount';
$lang['cancel_reservation'] = 'Cancel Reservation';
$lang['status'] = 'Status';
$lang['canceled'] = 'Canceled';
$lang['current_time'] = 'Current Time';
$lang['cancel_charge'] = 'Cancellation Charge';
$lang['cancel_period'] = 'Cancellation Period';
$lang['amount_paid'] = 'Amount Paid';
$lang['amount_to_refund'] = 'Amount to be Refunded';
$lang['cancel_policy'] = 'Cancellation Policy';
$lang['kindly_check'] = 'Kindly Check Our';
$lang['for_detail'] = 'For Detail.';





/* End of file reserve_lang.php */
/* Location: ./main/language/english/main/reserve_lang.php */