<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] 			        = 'My Order';
$lang['button_go_back'] 				= 'Go Back';
$lang['label_price']                    = 'Booking Cost';

/* End of file cart_lang.php */
/* Location: ./main/language/english/main/cart_lang.php */