<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] 			        = 'Recent Orders';
$lang['text_my_account'] 		        = 'My Account';
$lang['text_view_heading'] 		        = 'My Order View';
$lang['text_order_menus'] 			    = 'Order Menus';
$lang['text_empty'] 			        = 'There are no order(s).';
$lang['text_delivery'] 			        = 'Delivery';
$lang['text_order'] 			        = 'Order';
$lang['text_reservation'] 		        = 'Reservation';
$lang['text_collection'] 		        = 'Pick-up';
$lang['text_reorder'] 			        = 'Re-order';
$lang['text_leave_review'] 		        = 'Leave review';
$lang['text_no_payment'] 		        = 'No Payment';

$lang['column_id'] 				        = 'ID';
$lang['column_status'] 			        = 'Status';
$lang['column_delivery'] 		        = 'Delivery';
$lang['column_payment'] 		        = 'Payment Type';
$lang['column_location'] 		        = 'Ordered From';
$lang['column_time'] 			        = 'Ordered Time';
$lang['column_date'] 			        = 'Ordered Time - Date';
$lang['column_date_added'] 			    = 'Ordered Date';
$lang['column_order'] 			        = 'Order Type';
$lang['column_items'] 			        = 'Total Items';
$lang['column_total'] 			        = 'Order Total';
$lang['column_menu_qty']				= 'Quantity';
$lang['column_menu_name'] 		        = 'Name/Options';
$lang['column_menu_price'] 		        = 'Price';
$lang['column_menu_subtotal'] 		    = 'Total';
$lang['table_price']					= 'Table Price';
$lang['button_order'] 			        = 'Place New Order';
$lang['button_reorder'] 		        = 'Re-Order';
$lang['button_back'] 			        = 'Back';
$lang['sub_total'] 						= 'Sub Total';
$lang['order_total'] 					= 'Order Total';
$lang['alert_reorder_success'] 			= '<p class="alert-success">You have successfully added the menus from order ID %s to your order.</p>';
 $lang['reward_amount']    ='Reward Amount';
 $lang['coupon_discount']    ='Coupon Discount';
 $lang['vat'] = 'VAT';
/* End of file orders_lang.php */
/* Location: ./main/language/english/main/orders_lang.php */