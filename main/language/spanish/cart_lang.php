<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'Mi pedido' ;
$lang['button_go_back'] = 'Regresa' ;
$lang['label_price'] = 'Coste de reserva' ;

/* End of file cart_lang.php */
/* Location: ./main/language/english/main/cart_lang.php */