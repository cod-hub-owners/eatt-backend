<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'Mi cuenta' ;
$lang['text_my_details'] = 'Mis detalles' ;
$lang['text_default_address'] = 'Mi defecto Dirección' ;
$lang['text_cart'] = 'Mi carrito de compras' ;
$lang['text_orders'] = 'Mis ordenes' ;
$lang['text_reservations'] = 'Mis Reservas' ;
$lang['text_inbox'] = 'Mi bandeja de entrada' ;
$lang['text_edit'] = 'EDITAR' ;
$lang['text_checkout'] = 'CHEQUEAR AHORA' ;
$lang['text_change_password'] = 'Cambia la contraseña' ;
$lang['text_no_default_address'] = 'Usted no tiene una dirección por defecto' ;
$lang['text_no_cart_items'] = 'No hay menús añadido a la cesta.' ;
$lang['text_no_orders'] = 'No hay órdenes disponibles para mostrar.' ;
$lang['text_no_reservations'] = 'No hay reservas disponibles para mostrar.' ;
$lang['text_no_inbox'] = 'No hay mensajes disponibles para mostrar' ;
$lang['label_first_name'] = 'Nombre de pila:' ;
$lang['label_last_name'] = 'Apellido:' ;
$lang['label_email'] = 'Dirección de correo electrónico:' ;
$lang['label_password'] = 'Contraseña:' ;
$lang['label_telephone'] = 'Teléfono:' ;
$lang['label_s_question'] = 'Pregunta de Seguridad:' ;
$lang['label_s_answer'] = 'Respuesta de seguridad:' ;
$lang['label_reward_points'] = 'Puntos de recompensa:' ;
$lang['column_cart_items'] = 'Articulos totales' ;
$lang['column_cart_total'] = 'Cantidad total' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['column_date'] = 'Fecha y hora' ;
$lang['column_status'] = 'Estado' ;
$lang['column_subject'] = 'Tema' ;
$lang['Canceled'] = 'Cancelado' ;
$lang['Confirmed'] = 'Confirmado' ;

/* End of file account_lang.php */
/* Location: ./main/language/english/main/account_lang.php */