<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'órdenes recientes' ;
$lang['text_my_account'] = 'Mi cuenta' ;
$lang['text_view_heading'] = 'Pedidos Ver' ;
$lang['text_order_menus'] = 'Solicitar menús' ;
$lang['text_empty'] = 'No hay pedido (s).' ;
$lang['text_delivery'] = 'Entrega' ;
$lang['text_reservation'] = 'Reserva' ;
$lang['text_collection'] = 'Recoger' ;
$lang['text_reorder'] = 'Reordenar' ;
$lang['text_leave_review'] = 'Deje opinión' ;
$lang['text_no_payment'] = 'Sin pago' ;
$lang['column_id'] = 'CARNÉ DE IDENTIDAD' ;
$lang['column_status'] = 'Estado' ;
$lang['column_delivery'] = 'Entrega' ;
$lang['column_payment'] = 'Pago' ;
$lang['column_location'] = 'Ubicación' ;
$lang['column_date'] = 'Listo Tiempo - Fecha' ;
$lang['column_date_added'] = 'Fecha Agregada' ;
$lang['column_order'] = 'Tipo de orden' ;
$lang['column_items'] = 'Articulos totales' ;
$lang['column_total'] = 'Total del pedido' ;
$lang['column_menu_qty'] = 'Cantidad' ;
$lang['column_menu_name'] = 'Nombre / Opciones' ;
$lang['column_menu_price'] = 'Precio' ;
$lang['column_menu_subtotal'] = 'Total' ;
$lang['table_price'] = 'Tabla de precios' ;
$lang['button_order'] = 'Lugar Nuevo Orden' ;
$lang['button_reorder'] = 'Reordenar' ;
$lang['button_back'] = 'atrás' ;
$lang['sub_total'] = 'Total parcial' ;
$lang['order_total'] = 'Total del pedido' ;
$lang['alert_reorder_success'] = '<p class="alert-success">Ha agregado correctamente los menús desde el ID de pedido %s a su pedido.</p>' ;
$lang['reward_amount'] = 'Monto recompensa' ;
$lang['vat'] = 'IVA' ;
/* End of file orders_lang.php */
/* Location: ./main/language/english/main/orders_lang.php */