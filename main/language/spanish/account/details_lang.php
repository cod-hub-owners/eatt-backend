<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'Mis detalles' ;
$lang['text_my_account'] = 'Mi cuenta' ;
$lang['text_details'] = 'Editar sus detalles' ;
$lang['text_password_heading'] = 'Cambia la contraseña' ;
$lang['text_select'] = 'Por favor seleccione' ;
$lang['button_subscribe'] = 'Suscribir' ;
$lang['button_back'] = 'atrás' ;
$lang['button_save'] = 'Guardar detalles' ;
$lang['label_first_name'] = 'Nombre de pila' ;
$lang['label_last_name'] = 'Apellido' ;
$lang['label_email'] = 'Dirección de correo electrónico' ;
$lang['label_password'] = 'Nueva contraseña' ;
$lang['label_password_confirm'] = 'Confirmar nueva contraseña' ;
$lang['label_old_password'] = 'Contraseña anterior' ;
$lang['label_telephone'] = 'Teléfono' ;
$lang['label_s_question'] = 'Pregunta de Seguridad' ;
$lang['label_s_answer'] = 'Respuesta de seguridad' ;
$lang['label_newsletter'] = 'Mantenerme al día con las ofertas por correo electrónico.' ;
$lang['error_password'] = 'El% s que ha introducido no coincide.' ;
$lang['alert_updated_success'] = '<p class="alert-success">Detalles actualizados con éxito.</p>' ;
$lang['profile_image'] = 'Imagen de perfil' ;
$lang['best_dimension'] = 'mejor Dimensión' ;

/* End of file details_lang.php */
/* Location: ./main/language/english/main/details_lang.php */