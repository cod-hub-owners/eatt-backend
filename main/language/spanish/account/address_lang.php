<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'Directorio' ;
$lang['text_my_account'] = 'Mi cuenta' ;
$lang['text_edit_heading'] = 'Libreta de direcciones Editar' ;
$lang['text_no_address'] = 'Usted no tiene ninguna dirección almacenada (s)' ;
$lang['text_edit'] = 'EDITAR' ;
$lang['text_delete'] = 'ELIMINAR' ;
$lang['button_back'] = 'atrás' ;
$lang['button_add'] = 'Agregar nueva dirección' ;
$lang['button_update'] = 'Actualizar dirección' ;
$lang['label_address_1'] = 'Dirección 1' ;
$lang['label_address_2'] = 'Dirección 2' ;
$lang['label_city'] = 'Ciudad' ;
$lang['label_state'] = 'Estado' ;
$lang['label_postcode'] = 'Código postal' ;
$lang['label_country'] = 'País' ;
$lang['alert_updated_success'] = '<p class="alert-success">Dirección agregada / actualizada con éxito.</p>' ;
$lang['alert_deleted_success'] = '<p class="alert-success">Dirección eliminada con éxito.</p>' ;

/* End of file address_lang.php */
/* Location: ./main/language/english/main/address_lang.php */