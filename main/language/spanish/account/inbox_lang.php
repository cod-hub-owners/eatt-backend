<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'Mi bandeja de entrada' ;
$lang['text_my_account'] = 'Mi cuenta' ;
$lang['text_view_heading'] = 'Mi bandeja de entrada de mensajes' ;
$lang['text_empty'] = 'No hay mensaje (s) en su bandeja de entrada.' ;
$lang['text_view'] = 'Ver' ;
$lang['column_date'] = 'Fecha' ;
$lang['column_time'] = 'Hora' ;
$lang['column_subject'] = 'Tema' ;
$lang['column_text'] = 'Texto' ;
$lang['column_action'] = 'Comportamiento)' ;
$lang['button_back'] = 'atrás' ;
$lang['button_delete'] = 'Borrar mensaje' ;
$lang['alert_unknown_error'] = 'Lo sentimos, se ha producido un error.' ;
$lang['alert_deleted_success'] = 'Mensaje eliminado con éxito.' ;

/* End of file inbox_lang.php */
/* Location: ./main/language/english/main/inbox_lang.php */