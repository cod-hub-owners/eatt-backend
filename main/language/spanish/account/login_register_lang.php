<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'Iniciar sesión' ;
$lang['text_register_heading'] = 'Registrarse' ;
$lang['text_logout_heading'] = 'Desconectado' ;
$lang['text_login'] = 'Iniciar sesión' ;
$lang['text_register'] = 'Registrarse' ;
$lang['text_forgot'] = '¿Se te olvidó tu contraseña?' ;
$lang['text_required'] = '*** Todos los Campos obligatorios.' ;
$lang['text_login_register'] = '¿Ya registrado? <a href="%s"> sesión </a>' ;
$lang['text_subject'] = '¡Cuenta creada!' ;
$lang['text_success_message'] = 'Tu cuenta ha sido creada.' ;
$lang['text_signature'] = 'Saludos, <br />% s' ;
$lang['label_first_name'] = 'Nombre de pila' ;
$lang['label_last_name'] = 'Apellido' ;
$lang['label_email'] = 'Dirección de correo electrónico' ;
$lang['label_password'] = 'Contraseña' ;
$lang['label_password_confirm'] = 'Contraseña confirmada' ;
$lang['label_telephone'] = 'Teléfono' ;
$lang['label_s_question'] = 'Pregunta de Seguridad' ;
$lang['label_s_answer'] = 'Respuesta de seguridad' ;
$lang['label_newsletter'] = 'Mantenerme al día con las ofertas por correo electrónico.' ;
$lang['label_terms'] = 'By clicking <strong class="label label-primary">Register</strong>, you agree to the <a href="%s" data-toggle="modal" data-target="#terms-modal">Terms and Conditions</a> set out by this site, including our Cookie Use.' ;
$lang['label_captcha'] = 'Escribe el código mostrado' ;
$lang['label_i_agree'] = 'Estoy de acuerdo' ;
$lang['label_subscribe'] = 'Suscribir' ;
$lang['button_terms_agree'] = 'Estoy de acuerdo' ;
$lang['button_subscribe'] = 'Suscribir' ;
$lang['button_login'] = 'Iniciar sesión' ;
$lang['button_register'] = 'Registrarse' ;
$lang['error_email_exist'] = 'La dirección de correo electrónico ya tiene una cuenta, por favor conectarse' ;
$lang['error_captcha'] = 'Las cartas que ha entrado no coincide con la imagen.' ;
$lang['alert_logout_success'] = '<p class="alert-success">Has cerrado sesión correctamente.</p>' ;
$lang['alert_invalid_login'] = '<p class="alert-danger">ID de correo electrónico o contraseña no válidos!</p>' ;
$lang['alert_account_created'] = '<p class="alert-success">Cuenta creada con éxito, inicie sesión a continuación!</p>' ;
$lang['label_already_have_account'] =  'Ya tengo una cuenta? ';

/* End of file login_register_lang.php */
/* Location: ./main/language/english/main/login_register_lang.php */