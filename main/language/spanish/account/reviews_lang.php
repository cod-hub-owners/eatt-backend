<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'Comentarios recientes' ;
$lang['text_my_account'] = 'Mi cuenta' ;
$lang['text_empty'] = 'No hay se añaden comentario (s).' ;
$lang['text_view'] = 'Ver' ;
$lang['text_write_review'] = 'Escribir un comentario' ;
$lang['text_view_review'] = 'Ver Comentario' ;
$lang['column_sale_type'] = 'Tipo de venta' ;
$lang['column_sale_id'] = 'Identificación venta' ;
$lang['column_restaurant'] = 'Restaurante' ;
$lang['column_rating'] = 'Clasificación' ;
$lang['column_author'] = 'Autor' ;
$lang['column_date'] = 'Fecha' ;
$lang['column_action'] = 'Comportamiento)' ;
$lang['button_back'] = 'atrás' ;
$lang['button_review'] = 'Enviar Comentario' ;
$lang['label_restaurant'] = 'Restaurante' ;
$lang['label_customer_name'] = 'Nombre del cliente' ;
$lang['label_quality'] = 'Calidad' ;
$lang['label_delivery'] = 'Entrega' ;
$lang['label_service'] = 'Servicio' ;
$lang['label_review'] = 'revisión' ;
$lang['label_date'] = 'Fecha de revisión' ;
$lang['alert_review_success'] = 'Comentario enviado correctamente, será visible una vez aprobado.' ;
$lang['alert_review_error'] = 'Ha ocurrido un error. Por favor intente otra vez.' ;
$lang['alert_review_duplicate'] = 'Lo siento. Ya sea que ya ha dejado una reseña para este fin, o se ha producido un error.' ;
$lang['alert_review_disabled'] = 'Los comentarios ha sido desactivado, por favor, administrador del sitio de contacto.' ;
$lang['alert_review_status_history'] = 'Opinión no se puede añadir porque no se ha completado la orden / reserva' ;
$lang['order'] = 'Orden' ;
$lang['reservation'] = 'Reserva' ;
$lang['quality'] = 'Calidad' ;
$lang['service'] = 'Servicio' ;
/* End of file reviews_lang.php */
/* Location: ./main/language/english/main/reviews_lang.php */