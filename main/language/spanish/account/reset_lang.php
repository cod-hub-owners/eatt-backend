<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'Restablecimiento de contraseña de cuenta' ;
$lang['text_summary'] = 'dirección de correo electrónico que utiliza para acceder a su cuenta Te enviaremos un correo electrónico con una nueva contraseña.' ;
$lang['label_email'] = 'Dirección de correo electrónico:' ;
$lang['label_s_question'] = 'Pregunta de Seguridad:' ;
$lang['label_s_answer'] = 'Respuesta de seguridad:' ;
$lang['button_continue'] = 'Seguir' ;
$lang['button_login'] = 'Iniciar sesión' ;
$lang['button_reset'] = 'Reiniciar' ;
$lang['alert_reset_success'] = '<p class="alert-success">Restablecimiento de contraseña exitosamente, por favor revise su correo electrónico para su nueva contraseña.</p>' ;
$lang['alert_reset_error'] = '<p class="alert-error">El restablecimiento de la contraseña no se realizó correctamente, no se encontró el correo electrónico o se ingresaron detalles incorrectos.</p>' ;
$lang['alert_no_email_match'] = 'No hay ninguna dirección de correo electrónico coincidente' ;
$lang['alert_no_s_answer_match'] = 'Respuesta de seguridad no coincide' ;
$lang['alert_no_s_question_match'] = 'La pregunta de seguridad no coincide' ;
/* End of file reset_lang.php */
/* Location: ./main/language/english/main/account/reset_lang.php */