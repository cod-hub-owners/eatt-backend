<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['activity_self'] = 'Tú' ;
$lang['activity_custom'] = '{customer} <b>{action}</b> {context} <a href="{link}"><b>{item}</b></a>.' ;
$lang['activity_logged_in'] = '<a href="{link}">{customer}</a> <b>logged</b> in.' ;
$lang['activity_logged_out'] = '<a href="{link}">{customer}</a> <b>logged</b> out.' ;
$lang['activity_registered_account'] = '<a href="{link}">{customer}</a> <b>registered</b> an <b>account.</b>' ;
$lang['activity_updated_account'] = '<a href="{link}">{customer}</a> <b>updated</b> their <b>account details.</b>' ;
$lang['activity_changed_password'] = '<a href="{link}">{customer}</a> <b>changed</b> their <b>account password.</b>' ;
$lang['activity_created_order'] = '{customer} <b>created</b> a new order <a href="{link}"><b>#{order_id}.</b></a>' ;
$lang['activity_reserved_table'] = '<a href="{customer_link}">{customer}</a> made a new <b>reservation</b> <a href="{link}"><b>#{reservation_id}.</b></a>' ;
$lang['activity_feedback_table'] = '<a href="{$link}">{customer}</a> made a new <b>{feedback_type}</b> <a href="{feedback}">Feedback.</a>' ;


/* End of file activities_lang.php */
/* Location: ./main/language/english/activities_lang.php */