<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'Detalles de los clientes Reserva' ;
$lang['thank_you_reservation'] = 'Gracias por sus reservas. Aquí es los detalles de su reserva,' ;
$lang['name'] = 'Nombre' ;
$lang['email'] = 'Email' ;
$lang['phone'] = 'Teléfono' ;
$lang['reservation_id'] = 'ID de reserva' ;
$lang['reservation_date'] = 'Fecha de la reserva' ;
$lang['reservation_time'] = 'Hora de la reserva' ;
$lang['guests'] = 'Los huéspedes' ;
$lang['booking_price'] = 'Precio de reserva' ;
$lang['table_booking_tax'] = 'Tabla de impuestos de Reservas' ;
$lang['order_price'] = 'Precio del pedido' ;
$lang['total_amount'] = 'Cantidad total' ;
$lang['cancel_reservation'] = 'Cancelar la reserva' ;
$lang['status'] = 'Estado' ;
$lang['canceled'] = 'Cancelado' ;
$lang['current_time'] = 'Tiempo actual' ;
$lang['cancel_charge'] = 'Cargo de cancelación' ;
$lang['cancel_period'] = 'Período de cancelación' ;
$lang['amount_paid'] = 'Cantidad pagada' ;
$lang['amount_to_refund'] = 'Cantidad a devolver' ;
$lang['cancel_policy'] = 'Política de cancelación' ;
$lang['kindly_check'] = 'Nuestro cheque bondadoso' ;
$lang['for_detail'] = 'Para el detalle.' ;





/* End of file reserve_lang.php */
/* Location: ./main/language/english/main/reserve_lang.php */