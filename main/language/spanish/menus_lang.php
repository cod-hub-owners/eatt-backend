<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'Menú' ;
$lang['column_price'] = 'Precio' ;
$lang['text_filter'] = 'Nota: Filtro del menú mediante la selección de una categoría a la izquierda' ;
$lang['text_empty'] = 'No hay menús en esta categoría.' ;
$lang['text_no_category'] = 'No hay categoría para mostrar.' ;
$lang['text_no_match'] = 'No se encontraron artículos que coincidan con los filtros seleccionados.' ;
$lang['text_hungry'] = 'Hambriento ?' ;
$lang['text_category'] = 'Listas de la categoría' ;
$lang['text_specials'] = 'Ofertas especiales' ;
$lang['text_end_days'] = 'Ends %s in %s days' ;
$lang['text_end_today'] = 'termina hoy' ;
$lang['text_mealtime'] = '%s only (%s - %s)' ;
$lang['column_id'] = '%s only (%s - %s)' ;
$lang['column_photo'] = 'Foto' ;
$lang['column_menu'] = 'Menú' ;
$lang['column_action'] = 'Comportamiento)' ;
$lang['button_add'] = 'Añadir al carrito' ;
$lang['button_review'] = 'Dejar un comentario' ;
$lang['add_to_cart'] = 'AÑADIR AL CARRITO' ;

/* End of file menus_lang.php */
/* Location: ./main/language/english/main/menus_lang.php */