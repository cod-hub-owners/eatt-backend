<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'Reservar una mesa' ;
$lang['text_success_heading'] = 'Confirmación de reservación' ;
$lang['text_login'] = 'Already have an account? <a href="%s">Login Here</a>' ;
$lang['text_logout'] = 'Welcome back <b>%s</b>, Not You? <a href="%s">Logout</a>' ;
$lang['label_first_name'] = 'Nombre de pila' ;
$lang['label_last_name'] = 'Apellido' ;
$lang['label_email'] = 'Dirección de correo electrónico' ;
$lang['label_confirm_email'] = 'Confirmar el correo' ;
$lang['label_telephone'] = 'Teléfono' ;
$lang['label_comment'] = 'Solicitudes especiales' ;
$lang['label_captcha'] = 'Escribe el código mostrado' ;
$lang['button_find_again'] = 'Encuentra mesa de nuevo' ;
$lang['button_reservation'] = 'Hacer reservacion' ;
$lang['error_captcha'] = 'Las cartas que ha entrado no coincide con la imagen.' ;
$lang['alert_no_table'] = '<p class="alert-danger">Please check table availability using the Find A Table form.</p>' ;
$lang['alert_reservation_disabled'] = '<p class="alert-danger">Table reservation has been disabled, please contact site administrator.</p>' ;
$lang['text_subject'] = 'Table Reserved - %s!' ;
$lang['text_greetings'] = 'Thank You %s,' ;
$lang['text_success'] = 'Your reservation at %s has been booked for %s on %s at %s.<br />Thanks for reserving with us online!' ;
$lang['text_signature'] = 'Regards, <br />%s <br /><br />We hope to see you again soon.' ;
$lang['book_your_table'] = 'Reserve su mesa' ;
$lang['guest'] = 'Los huéspedes' ;
$lang['date'] = 'Fecha' ;
$lang['time'] = 'Hora' ;
$lang['total_tables'] = 'Tablas totales' ;
$lang['location'] = 'Restaurante' ;
$lang['book_now'] = 'Reservar ahora' ;
$lang['restaurant'] = 'Restaurante' ;
$lang['label_price'] = 'Coste de reserva' ;
$lang['return'] = '<Volver a la búsqueda' ;



/* End of file reserve_lang.php */
/* Location: ./main/language/english/main/reserve_lang.php */