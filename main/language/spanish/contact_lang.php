<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['text_heading'] = 'Contacto' ;
$lang['text_summary'] = 'No dude en enviar un mensaje' ;
$lang['text_find_us'] = 'Encontrarnos en Mapa' ;
$lang['text_select_subject'] = 'seleccione un tema' ;
$lang['text_contact_us'] = 'Contáctenos' ;
$lang['label_subject'] = 'Su mensaje Asunto:' ;
$lang['label_full_name'] = 'Nombre completo:' ;
$lang['label_email'] = 'Dirección de correo electrónico:' ;
$lang['label_telephone'] = 'Teléfono:' ;
$lang['label_comment'] = 'Comentario' ;
$lang['label_captcha'] = 'Escribe el código mostrado' ;
$lang['button_send'] = 'ENVIAR' ;
$lang['error_captcha'] = 'Las cartas que ha entrado no coincide con la imagen.' ;
$lang['alert_contact_sent'] = '<p class="alert-success">Message Sent successfully, we will get back to you shortly!</p>' ;
$lang['contact_details'] = 'Detalles de contacto' ;
"$lang['contact_information'] = '<b>
 <b>Telephone No</b> : +911234567<br>
 <b>Email</b> : hello@eatt.com' ;"




/* End of file contact_lang.php */
/* Location: ./main/language/english/main/contact_lang.php */