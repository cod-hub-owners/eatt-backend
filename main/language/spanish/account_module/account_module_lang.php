<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] = 'Cuenta' ;
$lang['text_tab_general'] = 'General' ;
$lang['text_heading'] = 'Cuenta' ;
$lang['text_account'] = 'Mi cuenta' ;
$lang['text_edit_details'] = 'Editar Mi detalles' ;
$lang['text_address'] = 'Directorio' ;
$lang['text_orders'] = 'órdenes recientes' ;
$lang['text_reservations'] = 'Reservas recientes' ;
$lang['text_reviews'] = 'Comentarios recientes' ;
$lang['text_inbox'] = 'Mi bandeja de entrada <span class="badge">%s</span>' ;
$lang['text_logout'] = 'Cerrar sesión' ;
$lang['label_heading'] = 'Bóveda:' ;

/* End of file account_lang.php */
/* Location: ./extensions/account_module/language/english/account_module_lang.php */