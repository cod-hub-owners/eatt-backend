<?php echo get_header(); ?>
<style type="text/less">
@media screen and (max-width: 414px) {
    footer {
        margin-top: 0rem;
    }
}
#wrapper {
  
  font-size: 1.5rem;
  text-align: center;
  box-sizing: border-box;
  color: #333;
  }
  #wrapper #dialog {
    border: solid 1px #ccc;
    margin: 10px auto;
    padding: 20px 30px;
    display: inline-block;
    box-shadow: 0 0 4px #ccc;
    background-color: #FAF8F8;
    overflow: hidden;
    position: relative;
    max-width: 450px;
    }
    #wrapper #dialog h3 {
      margin: 0 0 10px;
      padding: 0;
      line-height: 1.25;
    }
    
    #wrapper #dialog span {
      font-size: 90%;
    }
    
     #wrapper #dialog #form {
      max-width: 240px;
      margin: 25px auto 0;
      }
    #wrapper #dialog #form input {
        margin: 0 5px !important;
        text-align: center !important;
        line-height: 80px !important;
        font-size: 50px !important;
        border: solid 1px #ccc !important;
        box-shadow: 0 0 5px #ccc inset !important;
        outline: none !important;
        width: 20% !important;
        transition: all .2s ease-in-out !important;
        border-radius: 3px !important;
        }
        #wrapper #dialog #form input:focus {
          border-color: purple !important;
          box-shadow: 0 0 5px purple inset !important;
        }
        
        #wrapper #dialog #form input selection {
          background: transparent;
        }
      
      
    #wrapper #dialog #form  button {
        margin:  30px 0 50px;
        width: 100%;
        padding: 6px;
        background-color: #B85FC6;
        border: none;
        text-transform: uppercase;
      }
    
    
    #wrapper #dialog button .close {
      
        border: solid 2px;
        border-radius: 30px;
        line-height: 19px;
        font-size: 120%;
        width: 22px;
        position: absolute;
        right: 5px;
        top: 5px;
      }           
   
    
    #wrapper #dialog div {
      position: relative;
      z-index: 1;
    }

</style>
<div id="page-content" style="background-color: #d6d6d6 !important;">
	<div class="container">
		

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 text-center top-20">

				<div id="login-form" class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12  content-wrap">
				
				<div class="heading-section top-20" >
						<h2 style="color: #66C2A5;"><?php echo lang('text_register'); ?></h2>
						<!--<span class="under-heading"></span>-->
					<input type="hidden" name="check_otp" id="check_otp" value="">
				</div>
			
			<div class="col-xs-12 col-sm-12 col-md-12 center-block">
				
				<div id="register-form" class="content-wrap col-md-12 col-sm-12 col-xs-12 center-block">
					<form method="POST" accept-charset="utf-8" action="<?php echo current_url(); ?>" role="form"  id="reg_form" class="reg_form">
						<div class="row">

							<div class="col-xs-12 col-sm-6 col-md-6">
								<div class="form-group">
									<input type="text" id="first-name" class="form-control input-md" value="<?php echo set_value('first_name'); ?>" name="first_name" placeholder="<?php echo lang('label_first_name'); ?>" autofocus="" maxlength="16"  onkeypress="return (event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" >
									<?php echo form_error('first_name', '<span class="text-danger" id="text-danger1">', '</span>'); ?>
									<span id="fnamechecker"  style="display: none;color: #a94442;">Enter First Name</span>
								</div>
							</div>
							<div class="col-xs-12  col-sm-6 col-md-6 ">
								<div class="form-group">
									<input type="text" id="last-name" class="form-control input-md" value="<?php echo set_value('last_name'); ?>" name="last_name" placeholder="<?php echo lang('label_last_name'); ?>" maxlength="16"  onkeypress="return (event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" >
									<span id="lnamechecker"  style="display: none;color: #a94442;">Enter Last Name</span>
									<?php echo form_error('last_name', '<span class="text-danger" id="text-danger2">', '</span>'); ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<input type="text" id="email" class="form-control input-md" value="<?php echo set_value('email'); ?>" name="email" placeholder="<?php echo lang('label_email'); ?>" onkeyup="AjaxLookup()"  autocomplete="off">
							<?php echo form_error('email', '<span class="text-danger" id="text-danger3">', '</span>'); ?>
							<span id="emailchecker"></span><span id="emailchecker1"  style="display: none;color: #a94442;">Enter Email</span>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div class="form-group">
									<input type="password" id="password" class="form-control input-md" value="" name="password" placeholder="<?php echo lang('label_password'); ?>" maxlength="16">
									<?php echo form_error('password', '<span class="text-danger" id="text-danger4">', '</span>'); ?>
									<span id="pwdchecker"  style="display: none;color: #a94442;">Enter Password</span>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div class="form-group">
									<input type="password" id="password-confirm" class="form-control input-md" name="password_confirm" value="" placeholder="<?php echo lang('label_password_confirm'); ?>" maxlength="16">
									<?php echo form_error('password_confirm', '<span class="text-danger" id="text-danger5">', '</span>'); ?>
									<span id="cpwdchecker"  style="display: none;color: #a94442;">Enter Confirm Password</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4">
							<div class="form-group">
							<select class="class-select2" name="country_code" id="country_code">

								<?php
								 foreach($phone_code as $pcode){?>

                                <option data-countryCode="<?php echo $pcode->code;?>" value="<?php echo $pcode->dial_code;?>"<?php if($pcode->dial_code == '+34' && $telephone[0] == '') {echo ' selected';}?>><?php echo $pcode->code.' ('.$pcode->dial_code.')';?></option>
                            	<?php }?>

							</select>
							</div>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8">
							<div class="form-group">
							<input type="text" id="telephone" class="form-control input-md" value="<?php echo set_value('telephone'); ?>" name="telephone" placeholder="<?php echo lang('label_telephone'); ?>" maxlength="20<?php //echo $digits; ?>" onkeyup="AjaxLookupph()" autocomplete="off" >
							<?php echo form_error('telephone', '<span class="text-danger" id="text-danger6">', '</span>'); ?>
							<span id="phchecker"></span><span id="phchecker1" style="display: none;color: #a94442;">Enter Phone Number</span>
							</div>
							
						</div>
						</div>
						<div class="form-group">
							<?php $val = set_value('security_question'); ?>
							
							<select name="security_question" id="security-question" class="class-select2" placeholder="<?php echo lang('label_s_question'); ?>">
							<?php foreach ($questions as $question) { ?>
								<option <?php echo $val == $question['id'] ? 'selected' : ''?> value="<?php echo $question['id']; ?>"><?php echo $question['text']; ?></option>
							<?php } ?>
							</select>
							<?php echo form_error('security_question', '<span class="text-danger" id="text-danger7">', '</span>'); ?>
						</div>
						<div class="form-group">
							<input type="text" id="security-answer" class="form-control input-md" name="security_answer" value="<?php echo set_value('security_answer'); ?>" placeholder="<?php echo lang('label_s_answer'); ?>">
							<?php echo form_error('security_answer', '<span class="text-danger" id="text-danger8">', '</span>'); ?>
							<span id="security_answer" style="display: none;color: #a94442;">Enter Security Answer</span>
						</div>
						<!-- <div class="form-group">
							<div class="input-group" style="width: 100%">
         		 				<span><?php //echo $captcha['image']; ?></span>
								<input type="hidden" name="captcha_word" class="form-control" value="<?php //echo $captcha['word']; ?>" />
								<input type="text" name="captcha" class="form-control" placeholder="<?php //echo lang('label_captcha'); ?>" />
							</div>
							<?php //echo form_error('captcha', '<span class="text-danger" id="text-danger">', '</span>'); ?>
						</div> -->
						<!--<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<span class="button-checkbox">
									<button id="newsletter" type="button" class="btn" data-color="info" tabindex="7">&nbsp;&nbsp<?php //echo lang('button_subscribe'); ?></button>
			                        <input type="checkbox" name="newsletter" class="hidden" value="1" <?php //echo set_checkbox('newsletter', '1'); ?>>
								</span>
								 <?php //echo lang('label_newsletter'); ?>
							</div>
							<?php //echo form_error('newsletter', '<span class="text-danger" id="text-danger">', '</span>'); ?>
						</div>
						<br />-->

						<?php if ($registration_terms) {?>
							<div class="row">
								<div class="col-xs-8 col-sm-9 col-md-9">
									<span class="button-checkbox">
										<button id="terms-condition" type="button" class="btn" data-color="info" tabindex="7">&nbsp;&nbsp;<?php echo lang('button_terms_agree'); ?></button>
				                        <input type="checkbox" name="terms_condition" class="hidden" value="1" <?php echo set_checkbox('terms_condition', '1'); ?>>
									</span>
									<?php echo sprintf(lang('label_terms'), $registration_terms); ?>
								</div>
								<?php echo form_error('terms_condition', '<span class="text-danger" id="text-danger9">', '</span>'); ?>
							</div>
							<div class="modal fade" id="terms-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
						<br />
						<br />

						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<button type="button" id="send_otp" onclick="sendotp('normal')" class="btn btn-primary btn-block btn-lg" ><?php echo lang('button_register'); ?></button>
							</div><br><br>
							<div class="col-xs-12 col-md-12 col-sm-12">
								<a href="<?php echo $login_url; ?>" style="text-align: right;float: right;margin-top: 30px;"><b><?php echo lang('label_already_have_account'); ?><?php echo lang('button_login'); ?></b></a>
							</div>
						</div>
					</form>
				</div>
				<div id="wrapper" style="display: none;">
						  <div id="dialog">
						  <div class="row">
						    <button class="close" onclick="back_register()">
						    Back</button>
						    </div>
						    <h3>Please enter the 4-digit verification code we sent via SMS:</h3>
						    <span>(we want to make sure it's you before we contact our movers)</span>
						    <div id="form">
						      <input type="text" name="otp" id="otp" class="form-control input-md" maxLength="4" size="1" min="0" max="9" pattern="[0-9]{1}" />
						      <span id="otpchecker"></span><br>
						      <button class="btn btn-primary btn-embossed" onclick="checkotp()">Verify</button>
						    </div>
						    
						    <div>
						      Didn't receive the code?<br />
						      <a onclick="sendotp('again')" style="cursor:pointer;" >Send code again</a><br />
						    </div>
						     
						  </div>
						</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#telephone').keypress(function(event){

       if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
          document.getElementById("telephone").style.display = 'block';
           event.preventDefault(); //stop character from entering input
       }

   });

$('form').submit(function(){
  $(this).find(':submit').attr('disabled','disabled');
});
	function checkotp() {

		var enter_otp = $('#otp').val();
		var check_otp = $('#check_otp').val();
		if(enter_otp != '')
		{
			if(enter_otp != check_otp)
			{
				var result = '<span style="color:red">Enter Valid OTP</span>';
		        	 $("#otpchecker").html(result);
		        $("#otpchecker").html(result);
			}
			else
			{
				$('form.reg_form').trigger('submit');
			}
		}
		else
		{
			var result = '<span style="color:red">Enter Valid OTP</span>';
		        	 $("#otpchecker").html(result);
		        $("#otpchecker").html(result);
		}
	}

	function sendotp(type) {
	//$('#send_otp').click(function (e) {	
		
		if(checkvalidation() === true)
		{

		//$('form.reg_form').trigger('submit');
		//$("#reg_form").submit(function(e) {
		/*var pnNum = $('#telephone').val();
        var cc = $('#country_code').val();
        var phone = cc + '' + pnNum;
        var email = $('#email').val();
        */var form = $('#reg_form');
       	
    	$.ajax({
                 url:"account/register/sendverificationcode",
                 data:form.serialize(),
                 type:"POST",
                 success:function(result) {
                 	document.getElementById('check_otp').value = result;
                 	if(type == 'normal')
                 	{
                 		$("#register-form").hide();
                    	$("#wrapper").show();
                	}
                	else
                	{
                		alert('Code Resend Successfully');
                	}
                	
                }
                 });
        //}
    	}
    //});
	}
	function back_register()
	{
		$("#register-form").show();
        $("#wrapper").hide();
	}
	function checkvalidation()
	{

		var first_name = $('#first-name').val();
		var last_name = $('#last-name').val();
		var email = $('#email').val();
		var password = $('#password').val();
		var password_confirm = $('#password-confirm').val();
		var telephone = $('#telephone').val();
		var security_answer = $('#security-answer').val();

		if(!first_name)
		{
			$('#fnamechecker').css("display", "block");
			
		}
		if(!last_name)
		{
			$('#lnamechecker').css("display", "block");
			
		}
		if(!email)
		{
			$('#emailchecker1').css("display", "block");
			
		}
		if(!password)
		{
			$('#pwdchecker').css("display", "block");
			
		}
		if(!password_confirm)
		{
			$('#cpwdchecker').css("display", "block");
			
		}
		if(!telephone)
		{
			$('#phchecker1').css("display", "block");
			
		}
		if(!security_answer)
		{
			$('#security_answer').css("display", "block");
			
		}
		if(security_answer && telephone && password_confirm && password && email && last_name && first_name)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
     function AjaxLookup() {
            var emAddr = $('#email').val();
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;   
            var eml= emailReg.test( emAddr );
            //console.log(eml);
           if(!eml) {
           	$("#emailchecker").show();
           	document.getElementById("emailchecker1").style.display = 'none';
	             var result = '<span style="color:red">Enter Valid Email</span>';
	        	 $("#emailchecker").html(result);
	        	}
	        else {
	        	$.ajax({
	                     url:"account/check_reg",
	                     data:{email:emAddr},
	                     type:"POST",
	                     success:function(result) {
	                     	
	                         $("#emailchecker").html(result);
	                    }
	                 });
	        	
	        }
	         if(emAddr.length==0){
            	var result = '<span style="color:red">Enter Email</span>';
            	$("#emailchecker").hide();
            	 document.getElementById("emailchecker1").style.display = 'block';
	        	// $("#emailchecker1").html(result);
            }
	    }

	    function AjaxLookupph() {
            var pnNum = $('#telephone').val();
            var cc = $('#country_code').val();
            var pn = cc + '-' + pnNum;

            
           if(!pn) {

           	
	             var result = '<span style="color:red">Enter Valid phone number</span>';
	        	 $("#phchecker").html(result);
	        	}
	        else {
	        	$.ajax({
	                     url:"account/check_ph",
	                     data:{phone:pn},
	                     type:"POST",
	                     success:function(result) {
	                     	console.log(result);
	                     	if(result == '<span style="color:red">Phone Number Already Exists</span>'){
	                     		 $(':input[type="submit"]').prop('disabled', true);
	                     	}
	                     	else{
	                     		$(':input[type="submit"]').prop('disabled', false);
	                        }
	                         $("#phchecker").html(result);
	                     	
	                     	
	                    }
	                 });
	        	
	        }
	         if(pnNum==''){
            	var result = '<span style="color:red">Enter Valid phone number</span>';
	        	 $("#phchecker").hide();
	        	  document.getElementById("phchecker1").style.display = 'block';
            }	else{
            	$("#phchecker").show();
           	document.getElementById("phchecker1").style.display = 'none';
            }
	    }
$("#first-name").keypress(function(){
	$('#text-danger1').css("display", "none");
	$('#fnamechecker').css("display", "none");
});
$("#last-name").keypress(function(){
	$('#text-danger2').css("display", "none");
	$('#lnamechecker').css("display", "none");
});
$("#email").keypress(function(){
	$('#text-danger3').css("display", "none");
	$('#emailchecker1').css("display", "none");
});
$("#password").keypress(function(){
	$('#text-danger4').css("display", "none");
	$('#pwdchecker').css("display", "none");
});
$("#password-confirm").keypress(function(){
	$('#text-danger5').css("display", "none");
	$('#cpwdchecker').css("display", "none");
});
$("#telephone").keypress(function(){
	$('#text-danger6').css("display", "none");
	$('#phchecker1').css("display", "none");
});
$("#security-question").keypress(function(){
	$('#text-danger7').css("display", "none");
	
});
$("#security-answer").keypress(function(){
	$('#text-danger8').css("display", "none");
	$('#security_answer').css("display", "none");
});

$("#terms-condition").keypress(function(){
	$('#text-danger9').css("display", "none");
});
           
    
 </script>
<?php echo get_footer(); ?>