
        <footer>
            <div id="footer">
                <div class="container">
                    <div class="row row-bottom-padded-md">
                    
                        <div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
                            <h3><?php echo lang('spotneat'); ?></h3>
                            <ul>
                                <li><a href="<?php echo site_url('about-us'); ?>"><?php echo lang('about'); ?></a></li>
                                <li><a href="<?php echo site_url('#'); ?>"><?php echo lang('blog'); ?></a></li>
                                <!-- <li><a href="<?php echo site_url('#'); ?>"><?php echo lang('faq'); ?></a></li> -->
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
                            <h3><?php echo lang('legal'); ?></h3>
                            <ul>
                                <li><a href="<?php echo site_url('#'); ?>"><?php echo lang('how_it_works'); ?></a></li>
                                <li><a href="<?php echo site_url('#'); ?>"><?php echo lang('privacy_policy'); ?></a></li>
                                
                           
                            <?php /* if (get_theme_options('hide_admin_link') !== '1') { ?>
                                <li><a target="_blank" href="<?php echo admin_url(); ?>"><?php echo lang('menu_admin'); ?></a></li>
                            <?php } */ ?>
                                
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
                            <h3><?php echo lang('contact'); ?></h3>
                            <ul>
                                <li><a href="<?php echo site_url('#'); ?>"><?php echo lang('help_center'); ?></a></li>
                                <li><a href="<?php echo site_url('contact'); ?>"><?php echo lang('contact_us'); ?></a></li>
                               
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
                            <h3><?php echo lang('text_follow_us'); ?></h3>
                            <p class="fh5co-social-icons">
                                 <?php $social_icons = get_theme_options('social'); ?>
                    <?php if (!empty($social_icons) AND array_filter($social_icons)) { ?>
                           
                                <?php if (!empty($social_icons['facebook'])) { ?>
                                    <a class="icon-facebook" target="_blank" href="<?php echo $social_icons['facebook']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['twitter'])) { ?>
                                    <a class="icon-twitter" target="_blank" href="<?php echo $social_icons['twitter']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['google'])) { ?>
                                    <a class="icon-google-plus" target="_blank" href="<?php echo $social_icons['google']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['youtube'])) { ?>
                                    <a class="icon-youtube" target="_blank" href="<?php echo $social_icons['youtube']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['vimeo'])) { ?>
                                    <a class="icon-vimeo" target="_blank" href="<?php echo $social_icons['vimeo']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['linkedin'])) { ?>
                                    <a class="icon-linkedin" target="_blank" href="<?php echo $social_icons['linkedin']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['pinterest'])) { ?>
                                    <a class="icon-pinterest" target="_blank" href="<?php echo $social_icons['pinterest']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['tumblr'])) { ?>
                                    <a class="icon-tumblr" target="_blank" href="<?php echo $social_icons['tumblr']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['flickr'])) { ?>
                                    <a class="icon-flickr" target="_blank" href="<?php echo $social_icons['flickr']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['instagram'])) { ?>
                                    <a class="icon-instagram" target="_blank" href="<?php echo $social_icons['instagram']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['dribbble'])) { ?>
                                    <a class="icon-dribbble" target="_blank" href="<?php echo $social_icons['dribbble']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['foursquare'])) { ?>
                                    <a class="icon-foursquare" target="_blank" href="<?php echo $social_icons['foursquare']; ?>"></a>
                                <?php } ?>
                            
                    <?php } ?>
                                
                            </p>
                        </div>
                    
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <p> <?php echo lang('rights');?></p>
                            <?php //echo sprintf(lang('site_copyright'), date('Y'), config_item('site_name'), lang('spotneat_system_name')) . lang('spotneat_system_powered'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="clearfix"></div>

    </div>
    <!-- END fh5co-page -->

    </div>
    <!-- END fh5co-wrapper -->
    
   
<script type="text/javascript">
        
        $('.carousel[data-type="multi"] .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
        next = $(this).siblings(':first');
    }
    
    next.children(':first-child').clone().appendTo($(this));
  }
});


jQuery(document).ready(function(){
    // This button will increment the value
    $('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });

    $("#hide1").click(function(){
        $("#filters").slideToggle();
    });
    $("#hide2").click(function(){
        $("#cuisine").slideToggle();
    });
    $("#hide3").click(function(){
        $("#est_type").slideToggle();
    });
    $("#hide4").click(function(){
        $("#trans").slideToggle();
    });



});


function change_lang(theForm){

     // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
       
        var formData = {
            'lang'              : $('input[name=lang]').val(),
        };

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : "<?php echo site_url().'home/change_lang';?>", // the url where we want to POST
            data        : formData, // our data object
        })
            // using the done promise callback
            .done(function(data) {

                // log data to the console so we can see
                console.log(data); 
                window.location.reload();
                // here we will handle errors and validation messages
            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
   

}

</script>


<?php $custom_script = get_theme_options('custom_script'); ?>
<?php if (!empty($custom_script['footer'])) { echo '<script type="text/javascript">'.$custom_script['footer'].'</script>'; }; ?>

</body>
</html>